SamplerState sampleState;

Texture2D diffuseTexture : register(t0);
Texture2D specularTexture : register(t1);
Texture2D ambientOcclusionTexture : register(t2);
Texture2D emissiveTexture : register(t3);
Texture2D metalnessTexture : register(t4);
Texture2D roughnessTexture : register(t5);
Texture2D opacityTexture : register(t6);
Texture2D displacementTexture : register(t7);
Texture2D heightMap : register(t8);
Texture2D normalMap : register(t9);
Texture2D lightMap : register(t10);

cbuffer LightBuffer
{
    float3 lightDirection;
    float4 diffuseColor;
    float4 ambientColor;
    float padding;
};

struct Input
{
    float4 position : POSITION;
    float4 positionCS : SV_POSITION;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 biNormal : BINORMAL;
    float2 textureUV : TEXCOORD0;
    float3 cameraPosition : CAMERA_POSITION;
};

float4 GetDiffuse(float2 aTextureUV)
{
    float4 color;
    color.xyz = diffuseTexture.Sample(sampleState, aTextureUV).xyz;
    color.w = 1.0f;
    return color;
}

float4 main(Input input) : SV_TARGET
{ 
    float4 textureColor; 
	float3 lightDir;
	float lightIntensity;
    float4 outputColor = float4(0.0, 0.0, 0.0, 0.0);

    textureColor = GetDiffuse(input.textureUV);

	lightDir = -lightDirection;
	lightIntensity = saturate(dot(input.normal, lightDir));

	if(lightIntensity > 0.0f)
    {
        outputColor += (diffuseColor * lightIntensity);
    }

    outputColor = saturate(outputColor);
	outputColor = outputColor * textureColor;

    return outputColor;
}