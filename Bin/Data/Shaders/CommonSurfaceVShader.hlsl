cbuffer MatrixBuffer
{
	matrix modelMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
    float3 cameraPosition;
};

struct Input
{
    float4 position : SV_POSITION;
	float4 normal : NORMAL;
    float4 tangent : TANGENT;
    float4 biNormal : BINORMAL;
    float2 textureUV : TEXCOORD0;
};

struct Output
{
    float4 position : POSITION;
    float4 positionCS : SV_POSITION;
	float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 biNormal : BINORMAL;
    float2 textureUV : TEXCOORD0;
    float3 cameraPosition : CAMERA_POSITION;
};

Output main(in Input input)
{
    Output output;
    float3x3 modelTransform = (float3x3) modelMatrix;
	input.position.w = 1.0f;

    output.position = mul(input.position, modelMatrix);
    output.position = mul(output.position, mul(viewMatrix, projectionMatrix));
    output.positionCS = output.position;

    output.normal = mul((float3) input.normal, modelTransform);
    output.biNormal = mul((float3) input.biNormal, modelTransform);
    output.tangent = mul((float3) input.tangent, modelTransform);

    output.textureUV = input.textureUV;

    output.cameraPosition = cameraPosition;

    return output;
}