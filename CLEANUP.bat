@echo off
:Ask
echo Would you like to delete all temp-files from the solution directory?(Y/N)
set INPUT=
set /P INPUT=Type input: %=%
If /I "%INPUT%"=="y" goto yes 
If /I "%INPUT%"=="n" goto no
cls
echo Incorrect input & goto Ask
:yes
@RD /S /Q %cd%\TEMP
del /S /Q %cd%\*.ilk
del /S /Q %cd%\*.db
del /S /Q %cd%\*.pdb
pause
:no
quit