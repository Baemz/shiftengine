# README #

### Information ###

* The Shift-Engine is an experimental project that is used to extend my knowledge of low-level programming, game-engine structures and patterns, rendering techniques and much more. I've decided to work with this project in my spare time alongside my gameprogramming-education at The Game Assembly. 
* Current version is 0.8.
* Version 1.0 will feature DirectX 11 support, Vulkan support, nVidia PhysX, audio powered by wWise and more...

### v0.8 Feature Notes ###
* DirectX 11 rendering-support is complete.
* DirectX 11 PBR rendering is ~85% complete.
* Initial DirectX 12 support is complete.
* Initial Vulkan support is complete.
* Initial nVidia PhysX support is complete.
* FBX-model loading is complete.
* Windows-message based input.
* Xbox controller support.

### Contact ###

* Hampus Siversson: hampussiversson@live.com