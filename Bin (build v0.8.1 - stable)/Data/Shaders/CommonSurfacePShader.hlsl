
#define PI 3.1415926535897932384626433832795f
#define GAMMA 2.2f

SamplerState sampleState;

Texture2D diffuseTexture : register(t0);
Texture2D specularTexture : register(t1);
Texture2D ambientOcclusionTexture : register(t2);
Texture2D emissiveTexture : register(t3);
Texture2D metalnessTexture : register(t4);
Texture2D roughnessTexture : register(t5);
Texture2D opacityTexture : register(t6);
Texture2D displacementTexture : register(t7);
Texture2D heightMap : register(t8);
Texture2D normalMap : register(t9);
Texture2D lightMap : register(t10);


cbuffer LightBuffer
{
	float3 lightDirection;
	float4 diffuseColor;
    float4 ambientColor;
	float padding;
};

struct Input
{
    float4 position : POSITION;
    float4 positionCS : SV_POSITION;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 biNormal : BINORMAL;
    float2 textureUV : TEXCOORD0;
    float3 cameraPosition : CAMERA_POSITION;
};

struct Attributes
{
    float3 position;
    float2 textureUV;
    float3 normal;
    float3 biNormal;
    float3 tangent;
};

struct Material
{
    float4 diffuse;
    float3 metallic;
    float roughness;
    float3 normal;
};

float4 GammaCorrectTexture(Texture2D tex, SamplerState s, float2 uv)
{
    float4 sample = tex.Sample(s, uv);
    return float4(pow(sample.rgb, GAMMA), sample.a);
}

float3 GammaCorrectTextureRGB(Texture2D tex, SamplerState s, float2 uv)
{
    float4 samp = tex.Sample(s, uv);
    return float3(pow(samp.rgb, GAMMA));
}

float4 GetDiffuse(float2 aTextureUV)
{
    return GammaCorrectTexture(diffuseTexture, sampleState, aTextureUV);
}

float3 GetMetallic(float2 aTextureUV)
{
    return GammaCorrectTextureRGB(metalnessTexture, sampleState, aTextureUV);
}

float GetRoughness(float2 aTextureUV)
{
    return GammaCorrectTextureRGB(roughnessTexture, sampleState, aTextureUV).r;
}

float3 NormalMap(Attributes attributes)
{
    float3x3 toWorld = float3x3(attributes.biNormal, attributes.tangent, attributes.normal);
    float3 nMap = normalMap.Sample(sampleState, attributes.textureUV).rgb * 2.0 - 1.0;
    nMap = mul(nMap.rgb, toWorld);
    nMap = normalize(nMap);
    return nMap;
}

float Fresnel(float f0, float fd90, float view)
{
    return f0 + (fd90 - f0) * pow(max(1.0f - view, 0.1f), 5.0f);
}

float Diffuse(Attributes attributes, float3 lightDir, Material material, float3 eye)
{
    float3 halfVector = normalize(lightDir + eye);

    float NdotL = saturate(dot(attributes.normal, lightDir));
    float LdotH = saturate(dot(lightDir, halfVector));
    float NdotV = saturate(dot(attributes.normal, eye));

    float energyBias = lerp(0.0f, 0.5f, material.roughness);
    float energyFactor = lerp(1.0f, 1.0f / 1.51f, material.roughness);
    float fd90 = energyBias + 2.0f * (LdotH * LdotH) * material.roughness;
    float f0 = 1.0f;

    float lightScatter = Fresnel(f0, fd90, NdotL);
    float viewScatter = Fresnel(f0, fd90, NdotV);

    return lightScatter * viewScatter * energyFactor;
}

float3 Metallic(Attributes attributes, float3 lightDir, Material material, float3 eye)
{
    float3 h = normalize(lightDir + eye);
    float NdotH = saturate(dot(attributes.normal, h));

    float rough2 = max(material.roughness * material.roughness, 2.0e-3f); // capped so spec highlights don't disappear
    float rough4 = rough2 * rough2;

    float d = (NdotH * rough4 - NdotH) * NdotH + 1.0f;
    float D = rough4 / (PI * (d * d));

	// Fresnel
    float3 reflectivity = material.metallic;
    float fresnel = 1.0;
    float NdotL = saturate(dot(attributes.normal, lightDir));
    float LdotH = saturate(dot(lightDir, h));
    float NdotV = saturate(dot(attributes.normal, eye));
    float3 F = reflectivity + (fresnel - fresnel * reflectivity) * exp2((-5.55473f * LdotH - 6.98316f) * LdotH);

	// geometric / visibility
    float k = rough2 * 0.5f;
    float G_SmithL = NdotL * (1.0f - k) + k;
    float G_SmithV = NdotV * (1.0f - k) + k;
    float G = 0.25f / (G_SmithL * G_SmithV);

    return G * D * F;
}

float3 IBL(Attributes attributes, float3 lightDir, Material material, float3 eye)
{
	// Note: Currently this function assumes a cube texture resolution of 1024x1024
    //float NdotV = max(dot(attributes.normal, eye), 0.0f);
    //
    //float3 reflectionVector = normalize(reflect(-eye, attributes.normal));
    //float smoothness = 1.0f - material.roughness;
    //float mipLevel = (1.0f - smoothness * smoothness) * 10.0f;
    //float4 cs = u_EnvironmentMap.SampleLevel(u_EnvironmentMapSampler, reflectionVector, mipLevel);
    //float3 result = pow(cs.xyz, GAMMA); //* RadianceIBLIntegration(NdotV, material.roughness, material.specular);
    //
    //float3 diffuseDominantDirection = attributes.normal;
    //float diffuseLowMip = 9.6;
    //float3 diffuseImageLighting = u_EnvironmentMap.SampleLevel(u_EnvironmentMapSampler, diffuseDominantDirection, diffuseLowMip).rgb;
    //diffuseImageLighting = pow(diffuseImageLighting, GAMMA);
    //
    //return result + diffuseImageLighting * material.diffuse.rgb;
    return float3(0.0, 0.0, 0.0);
}

float4 main(in Input input) : SV_TARGET
{
    Attributes attributes;
    attributes.position = (float3)input.position;
    attributes.textureUV = input.textureUV;
    attributes.normal = normalize(input.normal);
    attributes.biNormal = normalize(input.biNormal);
    attributes.tangent = normalize(input.tangent);
    
    int globalUsingNormalMap = 0;
    if (globalUsingNormalMap > 0)
    {
        attributes.normal = NormalMap(attributes);
    }

    float3 eye = normalize(input.cameraPosition - attributes.position);

    Material material;
    material.diffuse = GetDiffuse(attributes.textureUV);
    material.metallic = GetMetallic(attributes.textureUV);
    material.roughness = GetRoughness(attributes.textureUV);

    float3 lightDir = -lightDirection;

    float4 diffuse = float4(0, 0, 0, 0);
    float3 metallic = float3(0, 0, 0);

    // Lights
    float NdotL = saturate(dot(attributes.normal, lightDir));
    diffuse += NdotL * Diffuse(attributes, lightDir, material, eye) * diffuseColor * 6.0; // NEEDS (* INTENSITY) ASWELL
    metallic += NdotL * Metallic(attributes, lightDir, material, eye) * diffuseColor.xyz * 2.0; // NEEDS (* INTENSITY) ASWELL

    float3 finalColor = (material.diffuse.rgb * diffuse.rgb) + metallic;
	/*float4 textureColor; 
	float3 lightDir;
	float lightIntensity;

    textureColor = GetDiffuseColor(input.textureUV);

	lightDir = -lightDirection;
	lightIntensity = saturate(dot(input.normal, lightDir));

	if(lightIntensity > 0.0f)
    {
        outputColor += (diffuseColor * lightIntensity);
    }

    outputColor = saturate(outputColor);
	outputColor = outputColor * textureColor;*/

    return float4(finalColor, material.diffuse.a);
}