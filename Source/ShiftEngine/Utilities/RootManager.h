#pragma once

class CFBXLoaderA;
namespace Shift3D
{
	class InputManager;
	class CTextureManager;
	class CModelManager;
	class CRootManager
	{
	public:
		static void Create();
		static void Destroy();

		static InputManager* GetInputManager() { return GetInstance()->myInputManager; }
		static CFBXLoaderA* GetFBXLoaderA() { return GetInstance()->myFBXLoaderA; }
		static CTextureManager* GetTextureManager() { return GetInstance()->myTextureManager; }
		static CModelManager* GetModelManager() { return GetInstance()->myModelManager; }
	private:
		CRootManager();
		~CRootManager();

		static CRootManager* GetInstance();
		static CRootManager* ourInstance;

		InputManager* myInputManager;
		CFBXLoaderA* myFBXLoaderA;
		CTextureManager* myTextureManager;
		CModelManager* myModelManager;
	};
}