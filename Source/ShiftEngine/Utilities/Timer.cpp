#include "stdafx.h"
#include "Timer.h"

using namespace Shift3D;

//Timer* Timer::ourInstance = nullptr;

Timer::Timer()
{
	myDeltaTime = 0;
	myTotalTime = 0;
	myFPS = 0;
	myPreviousTime = clock.now();
	myStartTime = clock.now();
	myFrameStartTime = clock.now();
	myFrameEndTime = clock.now();
}


Timer::~Timer()
{
	
}

/*
bool Timer::Create()
{
	assert(ourInstance == nullptr && "Instance already created!");
	ourInstance = new Timer();
	if (ourInstance == nullptr)
	{
		return false;
	}
	return true;
}*/

/*
bool Timer::Destroy()
{
	delete ourInstance;
	ourInstance = nullptr;
	return false;
}*/

/*
Timer * Timer::GetInstance()
{
	return ourInstance;
}*/

void Timer::Update()
{
	std::chrono::high_resolution_clock::time_point pointOne = clock.now();

	std::chrono::duration<float> deltaTime = std::chrono::duration_cast<std::chrono::duration<float>>(pointOne - myPreviousTime);
	std::chrono::duration<double> totalTime = std::chrono::duration_cast<std::chrono::duration<double>>(pointOne - myStartTime);

	myDeltaTime = deltaTime.count();
	myTotalTime = totalTime.count();

	myFPS = static_cast<unsigned int>(1 / myDeltaTime);
	myPreviousTime = pointOne;
}

unsigned int Shift3D::Timer::GetFPS() const
{
	return myFPS;
}

float Timer::GetDeltaTime() const
{
	if (myDeltaTime > 0.5)
	{
		return 0.5f;
	}
	else
	{
		return myDeltaTime;
	}
}

double Timer::GetTotalTime() const
{
	return myTotalTime;
}
