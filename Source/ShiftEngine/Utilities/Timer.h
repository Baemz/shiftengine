#pragma once
#include <chrono>
namespace Shift3D
{
	class Timer
	{
	public:

		/*static bool Create();
		static bool Destroy();
		static Timer* GetInstance();*/

		Timer();
		~Timer();
		Timer(const Timer& aTimer) = delete;
		Timer& operator=(const Timer& aTimer) = delete;

		void Update();
		unsigned int GetFPS() const;

		float GetDeltaTime() const;
		double GetTotalTime() const;

	private:
		//static Timer* ourInstance;
		float myDeltaTime;
		double myTotalTime;
		unsigned int myFPS;
		std::chrono::time_point<std::chrono::high_resolution_clock> myStartTime;
		std::chrono::time_point<std::chrono::high_resolution_clock> myPreviousTime;


		std::chrono::time_point<std::chrono::high_resolution_clock> myFrameStartTime;
		std::chrono::time_point<std::chrono::high_resolution_clock> myFrameEndTime;

		std::chrono::high_resolution_clock clock;
	};
}

