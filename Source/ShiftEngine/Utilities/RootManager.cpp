#include "stdafx.h"
#include "RootManager.h"
#include "Input/InputManager.h"
#include "Texture/TextureManager.h"
#include "Model/ModelManager.h"
#include "FBX/FBXLoader.h"
#include "ThirdParty/FBX/FBXLoaderA.h"
#include <cassert>

using namespace Shift3D;

CRootManager* CRootManager::ourInstance = nullptr;

void CRootManager::Create()
{
	if (ourInstance)
	{
		ERROR_PRINT("%s","Error, MainSingleton instance already initialised!")
		return;
	}
	else
	{
		ourInstance = new CRootManager();
	}
}

void CRootManager::Destroy()
{
	SAFE_DELETE(ourInstance);
	ourInstance = nullptr;
}

CRootManager* Shift3D::CRootManager::GetInstance()
{
	return ourInstance;
}

CRootManager::CRootManager()
{
	myInputManager = new InputManager();
	myFBXLoaderA = new CFBXLoaderA();
	myTextureManager = new CTextureManager();
	myModelManager = new CModelManager();
}

CRootManager::~CRootManager()
{
	SAFE_DELETE(myInputManager);
	SAFE_DELETE(myFBXLoaderA);
	SAFE_DELETE(myTextureManager);
	SAFE_DELETE(myModelManager);
}
