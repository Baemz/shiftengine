#pragma once
#include <string>

namespace Shift3D { namespace Utils {

	inline std::wstring ConvertStringToWString(const std::string& aString)
	{
		std::wstring ws(aString.begin(), aString.end());
		return ws;
	}

	inline wchar_t* ConvertCharArrayToLPCWSTR(const char* charArray)
	{
		wchar_t* wString = new wchar_t[4096];
		MultiByteToWideChar(CP_ACP, 0, charArray, -1, wString, 4096);
		return wString;
	}

	inline bool FileExists(const char* aFileName) {
		struct stat buffer;
		return (stat(aFileName, &buffer) == 0);
	}

	inline std::string GetFilename(const std::string& aPath)
	{
		return aPath.substr(aPath.find_last_of("/\\") + 1);
	}

} }