#pragma once
#include "../Engine.h"
#include <vector>

namespace Shift3D
{

#ifndef _RETAIL
#define SETCONSOLECOLOR(color) Shift3D::CEngine::GetInstance()->GetErrorHandler().>SetConsoleColor(color);
#define ERROR_PRINT(aFormat, ...) Shift3D::CEngine::GetInstance()->GetErrorHandler().ErrorPrint(__FILE__, __LINE__, aFormat, ##__VA_ARGS__);
#define INFO_PRINT(aFormat, ...) Shift3D::CEngine::GetInstance()->GetErrorHandler().InfoPrint(aFormat, ##__VA_ARGS__);
#define INFO_TIP(aFormat, ...) Shift3D::CEngine::GetInstance()->GetErrorHandler().InfoTip(aFormat, ##__VA_ARGS__);
#else
#define SETCONSOLECOLOR(color);
#define ERROR_PRINT(aFormat, ...);
#define INFO_PRINT(aFormat, ...);
#define INFO_TIP(aFormat, ...);
#endif
	class CErrorHandler
	{
	public:
		typedef std::function<void(std::string)> callback_function_log;
		typedef std::function<void(std::string)> callback_function_error;
		CErrorHandler();
		~CErrorHandler(void);
		void AddLogListener(callback_function_log aFunctionToCall, callback_function_error aFunctionToCallOnError);
		void Destroy();
		void ErrorPrint(const char* aFile, int aline, const char* aFormat, ...);
		void InfoPrint(const char* aFormat, ...);
		void InfoTip(const char* aFormat, ...);
		void SetConsoleColor(int aColor);
		unsigned int GetErrorsReported() const { return myErrorsReported; }
	private:
		std::vector<callback_function_log> myLogFunctions;
		std::vector<callback_function_error> myErrorFunctions;
		unsigned int myErrorsReported;

	};

}