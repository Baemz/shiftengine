#pragma once
namespace Shift3D
{
	enum EWindowSetting
	{
		EWindowSetting_Overlapped,
		EWindowSetting_Borderless,
	};

	enum class EGraphicsAPI
	{
		eDirectX11,
		eDirectX12,
		eOpenGL,
		eVulkan,
		None
	};
	struct SWindowConfig
	{
		unsigned short myWindowWidth;
		unsigned short myWindowHeight;

		unsigned short myRenderWidth;
		unsigned short myRenderHeight;

		unsigned short myTargetHeight;
		unsigned short myTargetWidth;

		bool myEnableVSync;
		bool myStartInFullScreen;

		EWindowSetting myWindowSetting;
	};
}