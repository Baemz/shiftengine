#pragma once

namespace Shift3D
{
	class CDirectionalLight
	{
	public:
		CDirectionalLight();
		~CDirectionalLight();

		void SetDirection(const Vector3f& aDirection);
		void SetColor(const Vector4f& aColor);
		void RotateAroundX(const float aAmount);
		void RotateAroundY(const float aAmount);
		void RotateAroundZ(const float aAmount);

		const Vector3f& GetDirection() const;
		const Vector4f& GetColor() const;

	private:
		Vector3f myDirection;
		Matrix33f myRotation;
		Vector4f myColor;
	};
}
