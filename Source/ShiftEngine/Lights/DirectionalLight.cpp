#include "stdafx.h"
#include "DirectionalLight.h"

using namespace Shift3D;

CDirectionalLight::CDirectionalLight()
{
}


CDirectionalLight::~CDirectionalLight()
{
}

void Shift3D::CDirectionalLight::SetDirection(const Vector3f& aDirection)
{
	myDirection = aDirection;
	myRotation = myRotation.MakeRotationMatrix(aDirection, { 0.f, 1.f, 0.f });
}

void Shift3D::CDirectionalLight::SetColor(const Vector4f& aColor)
{
	myColor = aColor;
}

void Shift3D::CDirectionalLight::RotateAroundX(const float aAmount)
{
	myRotation = myRotation.CreateRotateAroundX(aAmount);
	myDirection = myDirection * myRotation;
}

void Shift3D::CDirectionalLight::RotateAroundY(const float aAmount)
{
	myRotation = myRotation.CreateRotateAroundY(aAmount); 
	myDirection = myDirection * myRotation;
}

void Shift3D::CDirectionalLight::RotateAroundZ(const float aAmount)
{
	myRotation = myRotation.CreateRotateAroundZ(aAmount);
	myDirection = myDirection * myRotation;
}

const Vector3f& Shift3D::CDirectionalLight::GetDirection() const
{
	return myDirection;
}

const Vector4f& Shift3D::CDirectionalLight::GetColor() const
{
	return myColor;
}
