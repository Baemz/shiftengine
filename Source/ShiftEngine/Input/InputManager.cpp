#include "stdafx.h"
#include "InputManager.h"
#include <string>

using namespace Shift3D;

InputManager::InputManager()
{
	for (unsigned int i = 0; i < myCurrentFrameKeyStates.size(); i++)
	{
		myCurrentFrameKeyStates[i] = false;
	}
	for (unsigned int i = 0; i < myPreviousFrameKeyStates.size(); i++)
	{
		myPreviousFrameKeyStates[i] = false;
	}
	for (unsigned int i = 0; i < myCurrentFrameMouseButtonStates.size(); i++)
	{
		myCurrentFrameMouseButtonStates[i] = false;
	}
	for (unsigned int i = 0; i < myPreviousFrameMouseButtonStates.size(); i++)
	{
		myPreviousFrameMouseButtonStates[i] = false;
	}
	GetCursorPos(&myCurrentMousePosition);
	myMouseMovementSinceLastFrame = { 0, 0 };
	myScrollMovementSinceLastFrame = 0;
	myCurrentScrollMovement = 0;
	myScrollPosition = 0;
}


InputManager::~InputManager()
{
}

void InputManager::Update()
{
	myPreviousFrameKeyStates = myCurrentFrameKeyStates;
	myPreviousFrameMouseButtonStates = myCurrentFrameMouseButtonStates;

	if (myCurrentScrollMovement == 0)
	{
		myScrollMovementSinceLastFrame = 0;
	} 
	else
	{
		myScrollMovementSinceLastFrame = ((myCurrentScrollMovement)-(myScrollMovementSinceLastFrame));
		myScrollPosition += myScrollMovementSinceLastFrame;
	}

	if (abs(myCurrentMousePosition.x - (myPreviousMousePosition.x)) > 0)
	{
		myMouseMovementSinceLastFrame.x = (myCurrentMousePosition.x - (myPreviousMousePosition.x));
	}
	else
	{
		myMouseMovementSinceLastFrame.x = 0;
	}

	if (abs(myCurrentMousePosition.y - (myPreviousMousePosition.y)) > 0)
	{
		myMouseMovementSinceLastFrame.y = (myCurrentMousePosition.y - (myPreviousMousePosition.y));
	}
	else
	{
		myMouseMovementSinceLastFrame.y = 0;
	}
	myPreviousMousePosition = myCurrentMousePosition;
	myCurrentScrollMovement = 0;
}

void InputManager::CursorToScreen(HWND aHWND)
{
	GetCursorPos(&myCurrentMousePosition);
	ScreenToClient(aHWND, &myCurrentMousePosition);
}

void Shift3D::InputManager::SetKeyIsDown(const uint8_t aKey)
{
	myCurrentFrameKeyStates[aKey] = true;
}

void Shift3D::InputManager::SetKeyIsUp(const uint8_t aKey)
{
	myCurrentFrameKeyStates[aKey] = false;
}

bool Shift3D::InputManager::WasKeyJustPressed(const Keys& aKey) const
{
	uint8_t key = static_cast<uint8_t>(aKey);
	return (myCurrentFrameKeyStates[key] == true && myPreviousFrameKeyStates[key] == false);
}

bool Shift3D::InputManager::IsKeyDown(const Keys& aKey) const
{
	uint8_t key = static_cast<uint8_t>(aKey);
	return myCurrentFrameKeyStates[key];
}

bool Shift3D::InputManager::WasKeyJustReleased(const Keys& aKey) const
{
	uint8_t key = static_cast<uint8_t>(aKey);
	return (myCurrentFrameKeyStates[key] == false && myPreviousFrameKeyStates[key] == true);
}

bool Shift3D::InputManager::IsKeyUp(const Keys& aKey) const
{
	uint8_t key = static_cast<uint8_t>(aKey);
	return (myCurrentFrameKeyStates[key] == false);
}

int InputManager::GetMousePositionX() const
{
	return myCurrentMousePosition.x;
}

int InputManager::GetMousePositionY() const
{
	return myCurrentMousePosition.y;
}

void InputManager::SetMousePosition(const int aPositionX, const int aPositionY)
{
	POINT oldMousePosition = myCurrentMousePosition;
	if (SetCursorPos(aPositionX, aPositionY) == false)
	{
		SetCursorPos(oldMousePosition.x, oldMousePosition.y);
	}
}

int InputManager::GetMouseXMovementSinceLastFrame() const
{
	return myMouseMovementSinceLastFrame.x;
}

int InputManager::GetMouseYMovementSinceLastFrame() const
{
	return myMouseMovementSinceLastFrame.y;
}

void InputManager::SetLeftMouseButtonDown()
{
	myCurrentFrameMouseButtonStates[0] = true;
}

void InputManager::SetLeftMouseButtonUp()
{
	myCurrentFrameMouseButtonStates[0] = false;
}

void InputManager::SetRightMouseButtonDown()
{
	myCurrentFrameMouseButtonStates[1] = true;
}

void InputManager::SetRightMouseButtonUp()
{
	myCurrentFrameMouseButtonStates[1] = false;
}

void InputManager::SetMiddleMouseButtonDown()
{
	myCurrentFrameMouseButtonStates[2] = true;
}

void InputManager::SetMiddleMouseButtonUp()
{
	myCurrentFrameMouseButtonStates[2] = false;
}

void InputManager::SetScrollMovement(const long aScrollMovement)
{
	myCurrentScrollMovement = aScrollMovement / WHEEL_DELTA;
}

long InputManager::GetScrollMovementSinceLastFrame() const
{
	return myScrollMovementSinceLastFrame;
}

long InputManager::GetScrollPosition() const
{
	return myScrollPosition;
}

bool InputManager::IsMouseButtonDown(const MButtons& aButton) const
{
	unsigned button = static_cast<unsigned int>(aButton);
	if (button >= 0 && button <= myCurrentFrameMouseButtonStates.size())
	{
		return myCurrentFrameMouseButtonStates[button];
	} 
	else
	{
		return false;
	}
}

bool InputManager::WasMouseButtonJustPressed(const MButtons& aButton) const
{
	unsigned button = static_cast<unsigned int>(aButton);
	if (button >= 0 && button <= myCurrentFrameMouseButtonStates.size())
	{
		return (myCurrentFrameMouseButtonStates[button] == true && myPreviousFrameMouseButtonStates[button] == false);
	}
	else
	{
		return false;
	}
}

bool InputManager::WasMouseButtonJustReleased(const MButtons& aButton) const
{
	unsigned button = static_cast<unsigned int>(aButton);
	if (button >= 0 && button <= myCurrentFrameMouseButtonStates.size())
	{
		return (myCurrentFrameMouseButtonStates[button] == false && myPreviousFrameMouseButtonStates[button] == true);
	}
	else
	{
		return false;
	}
}

void InputManager::HandleInput(HWND aHWND, UINT message, WPARAM wParam, LPARAM /*lParam*/)
{
	CursorToScreen(aHWND);
	switch (message)
	{
	case WM_MOUSEMOVE:
	{
		//Called when the cursor moves.
		break;
	}
	case WM_RBUTTONDOWN:
	{
		this->SetRightMouseButtonDown();
		break;
	}
	case WM_RBUTTONUP:
	{
		this->SetRightMouseButtonUp();
		break;
	}
	case WM_LBUTTONDOWN:
	{
		this->SetLeftMouseButtonDown();
		break;
	}
	case WM_LBUTTONUP:
	{
		this->SetLeftMouseButtonUp();
		break;
	}
	case WM_MBUTTONDOWN:
	{
		this->SetMiddleMouseButtonDown();
		break;
	}
	case WM_MBUTTONUP:
	{
		this->SetMiddleMouseButtonUp();
		break;
	}
	case WM_MOUSEWHEEL:
	{
		this->SetScrollMovement(GET_WHEEL_DELTA_WPARAM(wParam));
		break;
	}
	case WM_KEYDOWN:
	{
		if (wParam < 256)
		{
			this->SetKeyIsDown(static_cast<uint8_t>(wParam));
		}
		break;
	}
	case WM_KEYUP:
	{
		if (wParam < 256)
		{
			this->SetKeyIsUp(static_cast<uint8_t>(wParam));
		}
		break;
	}
	default:
		break;
	}

}

