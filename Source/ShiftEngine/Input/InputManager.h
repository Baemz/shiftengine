#pragma once
#include <array>
#include <windows.h>
#include "Keys.h"

namespace Shift3D
{
	class InputManager
	{
	public:
		InputManager();
		~InputManager();

		// You MUST run Update() AFTER all of your desired input.
		void Update();


		void SetKeyIsDown(const uint8_t aKey);
		void SetKeyIsUp(const uint8_t aKey);

		bool WasKeyJustPressed(const Keys& aKey) const;
		bool IsKeyDown(const Keys& aKey) const;
		
		bool WasKeyJustReleased(const Keys& aKey) const;
		bool IsKeyUp(const Keys& aKey) const;

		int GetMousePositionX() const;
		int GetMousePositionY() const;
		void SetMousePosition(const int aPositionX, const int aPositionY);

		int GetMouseXMovementSinceLastFrame() const;
		int GetMouseYMovementSinceLastFrame() const;

		void SetLeftMouseButtonDown();
		void SetLeftMouseButtonUp();

		void SetRightMouseButtonDown();
		void SetRightMouseButtonUp();

		void SetMiddleMouseButtonDown();
		void SetMiddleMouseButtonUp();

		void SetScrollMovement(const long aScrollMovement);
		long GetScrollMovementSinceLastFrame() const;
		long GetScrollPosition() const;

		bool IsMouseButtonDown(const MButtons& aButton) const;
		bool WasMouseButtonJustPressed(const MButtons& aButton) const;
		bool WasMouseButtonJustReleased(const MButtons& aButton) const;

		void HandleInput(HWND aHWND, UINT message, WPARAM wParam, LPARAM lParam);

	private: 
		void CursorToScreen(HWND aHWND);

		std::array<bool, 256> myPreviousFrameKeyStates;
		std::array<bool, 256> myCurrentFrameKeyStates;
		POINT myCurrentMousePosition;
		POINT myPreviousMousePosition;
		POINT myMouseMovementSinceLastFrame;
		std::array<bool, 3> myCurrentFrameMouseButtonStates;
		std::array<bool, 3> myPreviousFrameMouseButtonStates;
		long myCurrentScrollMovement;
		long myScrollPosition;
		long myScrollMovementSinceLastFrame;
	};
}

