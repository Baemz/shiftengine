#pragma once
#include "FBXVertexStructs.h"
#include "Math/Matrix44.h"
#include "assimp/scene.h"
#include <map>


struct BoneInfo
{
	FBXLoader::Matrix44f BoneOffset;
	FBXLoader::Matrix44f FinalTransformation;

	BoneInfo()
	{

	}
};

// One model can contain multiple meshes
class CLoaderMesh
{
	friend class CFBXLoaderA;
	friend class CLoaderModel;
private:
	CLoaderMesh() { myShaderType = 0; myVerticies = nullptr; myVertexBufferSize = 0; myVertexCount = 0; myModel = nullptr; myIndexes.reserve(1000); }
	~CLoaderMesh(){}
	std::vector<UINT> myIndexes;
	std::vector<CLoaderMesh*> myChildren;
	unsigned int myShaderType;
	unsigned int myVertexBufferSize;
	int myVertexCount;
	class CLoaderModel* myModel;
	char* myVerticies;
};

class CLoaderModel
{
	friend class CFBXLoaderA;
private:
	CLoaderModel(){ myIsLoaded = false; myAnimationDuration = 0.0f; }
	~CLoaderModel(){}
	void SetData(const char* aModelPath){ myModelPath = aModelPath; }
	CLoaderMesh* CreateMesh(){ CLoaderMesh *model = new CLoaderMesh(); myMeshes.push_back(model); model->myModel = this; return model; }

	bool myIsLoaded;
	const struct aiScene* myScene;
	FBXLoader::Matrix44f myGlobalInverseTransform;
	std::string myModelPath;
	std::vector<CLoaderMesh*> myMeshes;
	std::vector<std::string> myTextures;
	// Animation data
	float myAnimationDuration;
	unsigned int myNumBones;
	std::vector<BoneInfo> myBoneInfo;
	std::map<std::string, unsigned int> myBoneNameToIndex;
};


class CFBXLoaderA
{
public:
	CFBXLoaderA();
	~CFBXLoaderA();
	bool LoadModel(const char* aModelPath, std::vector<FBXLoader::SMeshData>& aDestination);

private:
	void* LoadModelInternal(CLoaderModel* someInput);
	int DetermineAndLoadVerticies(struct aiMesh* aMesh, CLoaderMesh* aLoaderMesh);
	void LoadMaterials(const struct aiScene *sc, CLoaderModel* aModel);
	void LoadTexture(int aType, std::vector<std::string>& someTextures, struct aiMaterial* aMaterial, std::string& aDirectoy);

};

