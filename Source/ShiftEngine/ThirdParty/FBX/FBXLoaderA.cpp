#include "stdafx.h"
#include "FBXLoaderA.h"

#include "assimp/cimport.h"
#include "assimp/postprocess.h"
#include "Engine.h"

#include <map>
#include <string>
#include <windows.h>
#include <Model/Model.h>
#include <memory.h>

// Change to your path


#define TEXTURE_SET_0 0

CFBXLoaderA::CFBXLoaderA()
{
}


CFBXLoaderA::~CFBXLoaderA() 
{
}

FBXLoader::Matrix44f ConvertToEngineMatrix33(const aiMatrix3x3& AssimpMatrix)
{

	FBXLoader::Matrix44f mat;
	mat.myMatrix[0][0] = AssimpMatrix.a1;	mat.myMatrix[0][1] = AssimpMatrix.a2;	mat.myMatrix[0][2] = AssimpMatrix.a3;	mat.myMatrix[0][3] = 0.0f;
	mat.myMatrix[1][0] = AssimpMatrix.b1;	mat.myMatrix[1][1] = AssimpMatrix.b2;	mat.myMatrix[1][2] = AssimpMatrix.b3;	mat.myMatrix[1][3] = 0.0f;
	mat.myMatrix[2][0] = AssimpMatrix.c1;	mat.myMatrix[2][1] = AssimpMatrix.c2;	mat.myMatrix[2][2] = AssimpMatrix.c3;	mat.myMatrix[2][3] = 0.0f;
	mat.myMatrix[3][0] = 0.0f;				mat.myMatrix[3][1] = 0.0f;				mat.myMatrix[3][2] = 0.0f;				mat.myMatrix[3][3] = 1.0f;
	return mat;
}

// constructor from Assimp matrix
FBXLoader::Matrix44f ConvertToEngineMatrix44(const aiMatrix4x4& AssimpMatrix)
{
	FBXLoader::Matrix44f mat;
	mat.myMatrix[0][0] = AssimpMatrix.a1; mat.myMatrix[0][1] = AssimpMatrix.a2; mat.myMatrix[0][2] = AssimpMatrix.a3; mat.myMatrix[0][3] = AssimpMatrix.a4;
	mat.myMatrix[1][0] = AssimpMatrix.b1; mat.myMatrix[1][1] = AssimpMatrix.b2; mat.myMatrix[1][2] = AssimpMatrix.b3; mat.myMatrix[1][3] = AssimpMatrix.b4;
	mat.myMatrix[2][0] = AssimpMatrix.c1; mat.myMatrix[2][1] = AssimpMatrix.c2; mat.myMatrix[2][2] = AssimpMatrix.c3; mat.myMatrix[2][3] = AssimpMatrix.c4;
	mat.myMatrix[3][0] = AssimpMatrix.d1; mat.myMatrix[3][1] = AssimpMatrix.d2; mat.myMatrix[3][2] = AssimpMatrix.d3; mat.myMatrix[3][3] = AssimpMatrix.d4;
	return mat;
}

Shift3D::Matrix44f ConvertToCUMatrix33(const FBXLoader::Matrix44f& FBXMatrix)
{
	Shift3D::Matrix44f mat;
	mat.m11 = FBXMatrix.myMatrix[0][0];	mat.m12 = FBXMatrix.myMatrix[0][1];	mat.m13 = FBXMatrix.myMatrix[0][2];	mat.m14 = 0.0f;
	mat.m21 = FBXMatrix.myMatrix[1][0];	mat.m22 = FBXMatrix.myMatrix[1][1];	mat.m23 = FBXMatrix.myMatrix[1][2];	mat.m24 = 0.0f;
	mat.m31 = FBXMatrix.myMatrix[2][0];	mat.m32 = FBXMatrix.myMatrix[2][1];	mat.m33 = FBXMatrix.myMatrix[2][2];	mat.m34 = 0.0f;
	mat.m41 = 0.0f;						mat.m42 = 0.0f;						mat.m43 = 0.0f;						mat.m44 = 1.0f;
	return mat;
}

Shift3D::Matrix44f ConvertToCUMatrix44(const FBXLoader::Matrix44f& FBXMatrix)
{
	Shift3D::Matrix44f mat;
	mat.m11 = FBXMatrix.myMatrix[0][0];	mat.m12 = FBXMatrix.myMatrix[0][1];	mat.m13 = FBXMatrix.myMatrix[0][2];	mat.m14 = FBXMatrix.myMatrix[0][3];
	mat.m21 = FBXMatrix.myMatrix[1][0];	mat.m22 = FBXMatrix.myMatrix[1][1];	mat.m23 = FBXMatrix.myMatrix[1][2];	mat.m24 = FBXMatrix.myMatrix[1][3];
	mat.m31 = FBXMatrix.myMatrix[2][0];	mat.m32 = FBXMatrix.myMatrix[2][1];	mat.m33 = FBXMatrix.myMatrix[2][2];	mat.m34 = FBXMatrix.myMatrix[2][3];
	mat.m41 = FBXMatrix.myMatrix[3][0];	mat.m42 = FBXMatrix.myMatrix[3][1];	mat.m43 = FBXMatrix.myMatrix[3][2];	mat.m44 = FBXMatrix.myMatrix[3][3];
	return mat;
}

int CFBXLoaderA::DetermineAndLoadVerticies(aiMesh* fbxMesh, CLoaderMesh* aLoaderMesh)
{
	unsigned int modelBluePrintType = 0;

	modelBluePrintType |= (fbxMesh->HasPositions() ? EModelBluePrint_Position : 0);
	modelBluePrintType |= (fbxMesh->HasTextureCoords(0) ? EModelBluePrint_UV : 0);
	modelBluePrintType |= (fbxMesh->HasNormals() ? EModelBluePrint_Normal : 0);
	modelBluePrintType |= (fbxMesh->HasTangentsAndBitangents() ? EModelBluePrint_BinormTan : 0);
	modelBluePrintType |= (fbxMesh->HasBones() ? EModelBluePrint_Bones : 0);

	int vertexBufferSize = 0;
	vertexBufferSize += (fbxMesh->HasPositions() ? sizeof(float) * 4 : 0);
	vertexBufferSize += (fbxMesh->HasTextureCoords(0) ? sizeof(float) * 2 : 0);
	vertexBufferSize += (fbxMesh->HasNormals() ? sizeof(float) * 4 : 0);
	vertexBufferSize += (fbxMesh->HasTangentsAndBitangents() ? sizeof(float) * 8 : 0);
	vertexBufferSize += (fbxMesh->HasBones() ? sizeof(float) * 8 : 0); // Better with an UINT, but this works

	aLoaderMesh->myShaderType = modelBluePrintType;
	aLoaderMesh->myVertexBufferSize = vertexBufferSize;

	aLoaderMesh->myVerticies = new char[vertexBufferSize * fbxMesh->mNumVertices];
	aLoaderMesh->myVertexCount = fbxMesh->mNumVertices;

	std::vector<VertexBoneData> collectedBoneData;
	if (fbxMesh->HasBones())
	{
		collectedBoneData.resize(fbxMesh->mNumVertices);

		unsigned int BoneIndex = 0;
		for (unsigned int i = 0; i < fbxMesh->mNumBones; i++) 
		{
			std::string BoneName(fbxMesh->mBones[i]->mName.data);
			if (aLoaderMesh->myModel->myBoneNameToIndex.find(BoneName) == aLoaderMesh->myModel->myBoneNameToIndex.end())
			{
				BoneIndex = aLoaderMesh->myModel->myNumBones;
				aLoaderMesh->myModel->myNumBones++;
				BoneInfo bi;
				aLoaderMesh->myModel->myBoneInfo.push_back(bi);


				FBXLoader::Matrix44f NodeTransformation = ConvertToEngineMatrix44(fbxMesh->mBones[i]->mOffsetMatrix);

				aLoaderMesh->myModel->myBoneInfo[BoneIndex].BoneOffset = NodeTransformation;
				aLoaderMesh->myModel->myBoneNameToIndex[BoneName] = BoneIndex;
			}
			else {
				BoneIndex = aLoaderMesh->myModel->myBoneNameToIndex[BoneName];
			}

			for (unsigned int j = 0; j < fbxMesh->mBones[i]->mNumWeights; j++) 
			{
				unsigned int VertexID = fbxMesh->mBones[i]->mWeights[j].mVertexId;
				float Weight = fbxMesh->mBones[i]->mWeights[j].mWeight;
				collectedBoneData[VertexID].AddBoneData(BoneIndex, Weight);
			}
		}
	}
	

	SVertexCollection vertexCollection;
	for (unsigned int i = 0; i < fbxMesh->mNumVertices; i++)
	{
		if (fbxMesh->HasPositions())
		{
			aiVector3D& mVertice = fbxMesh->mVertices[i];
			vertexCollection.PushVec4(FBXLoader::Vector4f(mVertice.x, mVertice.y, mVertice.z, 1));
		}
		if (fbxMesh->HasNormals())
		{
			aiVector3D& mNorm = fbxMesh->mNormals[i];
			vertexCollection.PushVec4(FBXLoader::Vector4f(mNorm.x, mNorm.y, mNorm.z, 1));
		}
		if (fbxMesh->HasTangentsAndBitangents())
		{
			aiVector3D& mTangent = fbxMesh->mTangents[i];
			aiVector3D& biTangent = fbxMesh->mBitangents[i];

			vertexCollection.PushVec4(FBXLoader::Vector4f(mTangent.x, mTangent.y, mTangent.z, 1));
			vertexCollection.PushVec4(FBXLoader::Vector4f(biTangent.x, biTangent.y, biTangent.z, 1));
		}
		if (fbxMesh->HasTextureCoords(TEXTURE_SET_0))		//HasTextureCoords(texture_coordinates_set)
		{
			vertexCollection.PushVec2(FBXLoader::Vector2f(fbxMesh->mTextureCoords[TEXTURE_SET_0][i].x, fbxMesh->mTextureCoords[TEXTURE_SET_0][i].y));
		}
		if (fbxMesh->HasBones())
		{
			VertexBoneData& boneData = collectedBoneData[i];

			// UINTS woudl be better
			aiVector3D bones;
			vertexCollection.PushVec4(FBXLoader::Vector4f((float)boneData.IDs[0], (float)boneData.IDs[1], (float)boneData.IDs[2], (float)boneData.IDs[3]));

			aiVector3D weights;
			vertexCollection.PushVec4(FBXLoader::Vector4f(boneData.Weights[0], boneData.Weights[1], boneData.Weights[2], boneData.Weights[3]));
		}
	}

	memcpy(aLoaderMesh->myVerticies, &vertexCollection.myData[0], vertexBufferSize * fbxMesh->mNumVertices);

	return vertexBufferSize;
}

bool CFBXLoaderA::LoadModel(const char* aModelPath, std::vector<FBXLoader::SMeshData>& aDestination)
{
	if (Shift3D::Utils::FileExists(aModelPath) == false)
	{
		return false;
	}

	CLoaderModel* newModel = new CLoaderModel();
	newModel->SetData(aModelPath);

	if (!LoadModelInternal(newModel))
	{
		delete newModel;
		newModel = nullptr;
	}

	for (size_t i = 0; i < newModel->myMeshes.size(); i++)
	{
		CLoaderMesh* mesh = newModel->myMeshes[i];
		FBXLoader::SMeshData data;
		data.myIndices = mesh->myIndexes;
		data.myVertices = mesh->myVerticies;
		data.myVertexBufferSize = mesh->myVertexBufferSize;
		data.myVertexCount = mesh->myVertexCount;
		data.myTextures = newModel->myTextures;
		aDestination.push_back(data);
	}

	delete newModel;
	newModel = nullptr;
	return true;
}

void* CFBXLoaderA::LoadModelInternal(CLoaderModel* someInput)
{
	CLoaderModel* model = someInput;
	const struct aiScene* scene = NULL;

	if (Shift3D::Utils::FileExists(model->myModelPath.c_str()) == false)
	{
		OutputDebugStringA("File not found");
		return nullptr;
	}

	scene = aiImportFile(model->myModelPath.c_str(), aiProcessPreset_TargetRealtime_MaxQuality | aiProcess_ConvertToLeftHanded);

	OutputDebugStringA(model->myModelPath.c_str());
	
	if (!scene)
	{
		OutputDebugStringA(aiGetErrorString());
		return nullptr;
	}

	model->myScene = scene;


	for (unsigned int n = 0; n < scene->mNumMeshes; ++n)
	{
		CLoaderMesh* mesh = model->CreateMesh();

		aiMesh* fbxMesh = scene->mMeshes[n];

		DetermineAndLoadVerticies(fbxMesh, mesh);

		for (unsigned int i = 0; i < fbxMesh->mNumFaces; i++)
		{
			for (uint j = 0; j < fbxMesh->mFaces[i].mNumIndices; j++)
			{
				mesh->myIndexes.push_back(fbxMesh->mFaces[i].mIndices[j]);
			}
		}
	}

	// Change to support multiple animations
	if (scene->mNumAnimations > 0)
	{
		model->myAnimationDuration = (float)scene->mAnimations[0]->mDuration;
	}
	
	LoadMaterials(scene, model);
	
	return model;

}


void CFBXLoaderA::LoadMaterials(const struct aiScene *sc, CLoaderModel* aModel)
{

	for (unsigned int m = 0; m < sc->mNumMaterials; m++)
	{
		LoadTexture(aiTextureType_DIFFUSE, aModel->myTextures, sc->mMaterials[m], aModel->myModelPath); // TEXTURE_DEFINITION_ALBEDO
		LoadTexture(aiTextureType_SPECULAR, aModel->myTextures, sc->mMaterials[m], aModel->myModelPath); // TEXTURE_DEFINITION_ROUGHNESS
		LoadTexture(aiTextureType_AMBIENT, aModel->myTextures, sc->mMaterials[m], aModel->myModelPath); // TEXTURE_DEFINITION_AMBIENTOCCLUSION
		LoadTexture(aiTextureType_EMISSIVE, aModel->myTextures, sc->mMaterials[m], aModel->myModelPath); // TEXTURE_DEFINITION_EMISSIVE
		LoadTexture(aiTextureType_HEIGHT, aModel->myTextures, sc->mMaterials[m], aModel->myModelPath);
		LoadTexture(aiTextureType_NORMALS, aModel->myTextures, sc->mMaterials[m], aModel->myModelPath); // TEXTURE_DEFINITION_NORMAL
		LoadTexture(aiTextureType_SHININESS, aModel->myTextures, sc->mMaterials[m], aModel->myModelPath);
		LoadTexture(aiTextureType_OPACITY, aModel->myTextures, sc->mMaterials[m], aModel->myModelPath);
		LoadTexture(aiTextureType_DISPLACEMENT, aModel->myTextures, sc->mMaterials[m], aModel->myModelPath);
		LoadTexture(aiTextureType_LIGHTMAP, aModel->myTextures, sc->mMaterials[m], aModel->myModelPath);
		LoadTexture(aiTextureType_REFLECTION, aModel->myTextures, sc->mMaterials[m], aModel->myModelPath); // TEXTURE_DEFINITION_METALNESS
	}
}

void CFBXLoaderA::LoadTexture(int aType, std::vector<std::string>& someTextures, aiMaterial* aMaterial, std::string& aDirectoy)
{
	int texIndex = 0;
	aiReturn texFound = AI_SUCCESS;

	aiString path;	// filename

	texFound = aMaterial->GetTexture((aiTextureType)aType, texIndex, &path);
	if (texFound == AI_FAILURE)
	{
		someTextures.push_back("");
		return;
	}

	std::string filePath = std::string(path.data);

	const size_t last_slash_idx = filePath.find_last_of("\\/");
	if (std::string::npos != last_slash_idx)
	{
		filePath.erase(0, last_slash_idx + 1);
	}

	const size_t last_directory_slash_idx = aDirectoy.find_last_of("\\/");
	if (std::string::npos != last_directory_slash_idx)
	{
		aDirectoy.erase(last_directory_slash_idx + 1, aDirectoy.size() - last_directory_slash_idx);
	}

	filePath = /*aDirectoy + */filePath;

	someTextures.push_back(filePath);
}