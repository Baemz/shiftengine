// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#ifndef WIN32_LEAN_AND_MEAN
	#define WIN32_LEAN_AND_MEAN
#endif          

// Windows Header Files:
#include <windows.h>

// C RunTime Header Files:
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

// STL
#include <algorithm>
#include <array>
#include <exception>
#include <map>
#include <set>
#include <string>
#include <vector>

// Shift Engine specific files and defines:
#pragma warning(disable : 4006)
#define SAFE_DELETE(aPtr) delete aPtr; aPtr = nullptr;
#define SAFE_RELEASE(aPtr) { if ( (aPtr) ) { (aPtr)->Release(); (aPtr) = 0; } }
#define MAX(aNum, aSecondNum) (aNum > aSecondNum) ? aNum : aSecondNum
#define MIN(aNum, aSecondNum) (aNum < aSecondNum) ? aNum : aSecondNum
#define PIf       3.14159265358979323846
#define DEGREES_TO_RADIANS static_cast<float>(PIf / 180.0f)


#include <Math/Vector.h>
#include <Math/Matrix.h>
#include <Utilities/ErrorHandler.h>
#include <Rendering/Render_Utils.h>
#include <Utilities/miscutil.h>
#include <Utilities/RootManager.h>