#pragma once
#include <cmath>

namespace Shift3D
{
	template <class T>
	class Vector2
	{
	public:
		T x;
		T y;

		Vector2<T>();
		Vector2<T>(const T& aX, const T& aY);
		Vector2<T>(const Vector2<T>& aVector) = default;
		Vector2<T>& operator=(const Vector2<T>& aVector2) = default;
		Vector2<T> operator-() const;
		~Vector2<T>() = default;

		T Length2() const;
		T Length() const;
		Vector2<T> GetNormalized() const;
		void Normalize();
		T Dot(const Vector2<T>& aVector) const;
	};


	template<class T> inline Vector2<T>::Vector2()
	{
		x = static_cast<T>(0);
		y = static_cast<T>(0);
	}

	template<class T> inline Vector2<T>::Vector2(const T& aX, const T& aY)
	{
		x = aX;
		y = aY;
	}

	template<class T> inline T Vector2<T>::Length2() const
	{
		return ((x*x) + (y*y));
	}

	template<class T> inline T Vector2<T>::Length() const
	{
		return sqrt((x*x) + (y*y));
	}

	template<class T> inline Vector2<T> Vector2<T>::GetNormalized() const
	{
		return Vector2<T>(x / this->Length(), y / this->Length());
	}

	template<class T> inline void Vector2<T>::Normalize()
	{
		T length = this->Length();
		x /= length;
		y /= length;
	}

	template<class T> inline T Vector2<T>::Dot(const Vector2<T>& aVector) const
	{
		return ((x*aVector.x) + (y*aVector.y));
	}

	template <class T> Vector2<T> Vector2<T>::operator-() const
	{
		return Vector2<T>(-x, -y);
	}

	//Returns the vector sum of aVector0 and aVector1
	template <class T> Vector2<T> operator+(const Vector2<T>& aVector0, const Vector2<T>& aVector1)
	{
		return Vector2<T>((aVector0.x + aVector1.x), (aVector0.y + aVector1.y));
	}

	//Returns the vector difference of aVector0 and aVector1
	template <class T> Vector2<T> operator-(const Vector2<T>& aVector0, const Vector2<T>& aVector1)
	{
		return Vector2<T>((aVector0.x - aVector1.x), (aVector0.y - aVector1.y));
	}

	//Returns the vector aVector multiplied by the scalar aScalar
	template <class T> Vector2<T> operator*(const Vector2<T>& aVector, const T& aScalar)
	{
		return Vector2<T>((aVector.x * aScalar), (aVector.y * aScalar));
	}

	//Returns the vector aVector multiplied by the scalar aScalar
	template <class T> Vector2<T> operator*(const T& aScalar, const Vector2<T>& aVector)
	{
		return Vector2<T>((aVector.x * aScalar), (aVector.y * aScalar));
	}

	//Returns the vector aVector divided by the scalar aScalar (equivalent to aVector multiplied by 1 / aScalar)
	template <class T> Vector2<T> operator/(const Vector2<T>& aVector, const T& aScalar)
	{
		T scale = (1 / aScalar);
		return Vector2<T>((aVector.x * scale), (aVector.y * scale));
	}

	//Equivalent to setting aVector0 to (aVector0 + aVector1)
	template <class T> void operator+=(Vector2<T>& aVector0, const Vector2<T>& aVector1)
	{
		aVector0.x = (aVector0.x + aVector1.x);
		aVector0.y = (aVector0.y + aVector1.y);
	}


	//Equivalent to setting aVector0 to (aVector0 - aVector1)
	template <class T> void operator-=(Vector2<T>& aVector0, const Vector2<T>& aVector1)
	{
		aVector0.x = (aVector0.x - aVector1.x);
		aVector0.y = (aVector0.y - aVector1.y);
	}


	//Equivalent to setting aVector to (aVector * aScalar)
	template <class T> void operator*=(Vector2<T>& aVector, const T& aScalar)
	{
		aVector.x = (aVector.x * aScalar);
		aVector.y = (aVector.y * aScalar);
	}


	//Equivalent to setting aVector to (aVector / aScalar)
	template <class T> void operator/=(Vector2<T>& aVector, const T& aScalar)
	{
		T scale = (1 / aScalar);
		aVector.x = (aVector.x * scale);
		aVector.y = (aVector.y * scale);
	}

	typedef Vector2<float> Vector2f;
	typedef Vector2<int> Vector2i;
	typedef Vector2<unsigned> Vector2ui;
	typedef Vector2<double> Vector2d;
}