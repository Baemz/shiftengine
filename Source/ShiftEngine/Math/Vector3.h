#pragma once
#include <cmath>
namespace Shift3D
{
	template <class T>
	class Vector4;

	template <class T>
	class Vector2;

	template <class T>
	class Vector3
	{
	public:
		T x;
		T y;
		T z;

		//Creates a null-vector
		Vector3<T> ();


		//Creates a vector (aX, aY, aZ)
		Vector3<T> (const T& aX, const T& aY, const T& aZ);


		//Copy constructor (compiler generated)
		Vector3<T> (const Vector3<T>& aVector) = default;

		explicit Vector3(const Vector4<T> &aVector4)
		{
			x = aVector4.x;
			y = aVector4.y;
			z = aVector4.z;
		}

		explicit Vector3(const Vector2<T> &aVector2)
		{
			x = aVector2.x;
			y = aVector2.y;
			z = 1.0f;
		}

		//Assignment operator (compiler generated)
		Vector3<T>& operator=(const Vector3<T>& aVector3) = default;


		//Destructor (compiler generated)
		~Vector3<T> () = default;


		//Returns the squared length of the vector
		T Length2() const;


		//Returns the length of the vector
		T Length() const;


		//Returns a normalized copy of this
		Vector3<T> GetNormalized() const;


		//Normalizes the vector
		void Normalize();


		//Returns the dot product of this and aVector
		T Dot(const Vector3<T>& aVector) const;


		//Returns the cross product of this and aVector
		Vector3<T> Cross(const Vector3<T>& aVector) const;

		static T Dot(const Vector3 &aLeft, const Vector3 &aRight);
		static Vector3 Cross(const Vector3 &aLeft, const Vector3 &aRight);
		static Vector3 Normalize(Vector3 aVector);
		static Vector3 HalfVector(const Vector3 &aLeft, const Vector3 &aRight);
		static T Manhattan(const Vector3<T> &aLeft, const Vector3<T> &aRight);
		static const Vector3
			Zero,
			UnitX,
			UnitY,
			UnitZ,
			One,
			Forward,
			Backward,
			Left,
			Right,
			Up,
			Down,
			Max;


		static float GetAngleFromCross(const Vector3<T> & aCrossProductFromNormalizedVectors)
		{
			float crossLength = aCrossProductFromNormalizedVectors.Length();
			if (crossLength > 1.f)
			{
				crossLength = 1.f;
			}
			else if (crossLength < 0.f)
			{
				crossLength = 0.f;
			}
			return asin(crossLength);
		}

		static Vector3 Lerp(const Vector3<T> & aLeft, const Vector3<T> & aRight, float anAmount)
		{
			if (anAmount >= 1.0f)
			{
				return aRight;
			}
			if (anAmount <= 0.0f)
			{
				return aLeft;
			}

			return Vector3<T>(aLeft.x + (aRight.x - aLeft.x) * anAmount, aLeft.y + (aRight.y - aLeft.y) * anAmount, aLeft.z + (aRight.z - aLeft.z) * anAmount);
		}

		static Vector3 Slerp(const Vector3<T> & aFrom, const Vector3<T> & aTo, float anAmount)
		{

			if (anAmount == 0) return aFrom;
			if (aFrom == aTo || anAmount == 1.0f) return aTo;

			float theta = acos(Vector3f::Dot(aFrom, aTo) / (aFrom.Length() * aTo.Length()));
			//float thetaDegree = theta * RadianToDegree;

			if (theta == 0) return aTo;

			float sinTheta = sin(theta);
			return (aFrom * (sin((1 - anAmount) * theta) / sinTheta)) + (aTo * (sin(anAmount * theta) / sinTheta));
		}
	};


	template<class T> inline Vector3<T>::Vector3()
	{
		x = static_cast<T>(0);
		y = static_cast<T>(0);
		z = static_cast<T>(0);
	}

	template<class T> inline Vector3<T>::Vector3(const T& aX, const T& aY, const T& aZ)
	{
		x = aX;
		y = aY;
		z = aZ;
	}

	template<class T> inline T Vector3<T>::Length2() const
	{
		return ((x*x) + (y*y) + (z*z));
	}

	template<class T> inline T Vector3<T>::Length() const
	{
		return sqrt((x*x) + (y*y) + (z*z));
	}

	template<class T> inline Vector3<T> Vector3<T>::GetNormalized() const
	{
		return Vector3<T>(x/this->Length(), y/this->Length(), z/this->Length());
	}

	template<class T> inline void Vector3<T>::Normalize()
	{
		T length = this->Length();
		x /= length;
		y /= length;
		z /= length;
	}

	template<class T> inline T Vector3<T>::Dot(const Vector3<T>& aVector) const
	{
		return ((x*aVector.x) + (y*aVector.y) + (z*aVector.z));
	}

	template<class T> inline Vector3<T> Vector3<T>::Cross(const Vector3<T>& aVector) const
	{
		return Vector3<T>(((y*aVector.z) - (z*aVector.y)), ((z*aVector.x) - (x*aVector.z)), ((x*aVector.y) - (y*aVector.x)));
	}

	template<class T>
	inline T Vector3<T>::Dot(const Vector3<T> & aLeft, const Vector3<T> & aRight)
	{
		return ((aLeft.x * aRight.x) + (aLeft.y * aRight.y) + (aLeft.z * aRight.z));
	}

	template<class T>
	inline Vector3<T> Vector3<T>::Cross(const Vector3<T> & aLeft, const Vector3<T> & aRight)
	{
		Vector3<T> tempVector;
		tempVector.x = aLeft.y * aRight.z - aLeft.z * aRight.y;
		tempVector.y = aLeft.z * aRight.x - aLeft.x * aRight.z;
		tempVector.z = aLeft.x * aRight.y - aLeft.y * aRight.x;

		return tempVector;
	}

	template<class T>
	inline Vector3<T> Vector3<T>::Normalize(Vector3<T> aVector)
	{
		/*float aLength = aVector.Length();
		if (aLength != 0.0f)
		{
			return aVector /= aLength;
		}*/

		return aVector;
	}

	template<class T>
	inline Vector3<T> Vector3<T>::HalfVector(const Vector3<T> & aLeft, const Vector3<T> & aRight)
	{
		return (aLeft + aRight).GetNormalized();
	}

	template<class T>
	inline T Vector3<T>::Manhattan(const Vector3<T> &anOriginPoint, const Vector3<T> &aDestinationPoint)
	{
		return abs(aDestinationPoint.x - anOriginPoint.x) + abs(aDestinationPoint.y - anOriginPoint.y) + abs(aDestinationPoint.z - anOriginPoint.z);
	}


	//Returns the vector sum of aVector0 and aVector1
	template <class T> Vector3<T> operator+(const Vector3<T>& aVector0, const Vector3<T>& aVector1) 
	{
		return Vector3<T>((aVector0.x + aVector1.x), (aVector0.y + aVector1.y), (aVector0.z + aVector1.z));
	}

	//Returns the vector difference of aVector0 and aVector1
	template <class T> Vector3<T> operator-(const Vector3<T>& aVector0, const Vector3<T>& aVector1) 
	{
		return Vector3<T>((aVector0.x - aVector1.x), (aVector0.y - aVector1.y), (aVector0.z - aVector1.z));
	}

	//Returns the negated vector
	template <class T> Vector3<T> operator-(const Vector3<T>& aVector)
	{
		return aVector * static_cast<T>(-1);
	}

	//Returns the vector aVector multiplied by the scalar aScalar
	template <class T> Vector3<T> operator*(const Vector3<T>& aVector, const T& aScalar) 
	{
		return Vector3<T>((aVector.x * aScalar), (aVector.y * aScalar), (aVector.z * aScalar));
	}

	//Returns the vector aVector multiplied by the scalar aScalar
	template <class T> Vector3<T> operator*(const T& aScalar, const Vector3<T>& aVector) 
	{
		return Vector3<T>((aVector.x * aScalar), (aVector.y * aScalar), (aVector.z * aScalar));
	}

	//Returns the vector multiplied by the another vector
	template <class T> Vector3<T> operator*(const Vector3<T>& aVector0, const Vector3<T>& aVector1)
	{
		return Vector3<T>((aVector0.x * aVector1.x), (aVector0.y * aVector1.y), (aVector0.z * aVector1.z));
	}

	//Returns the vector aVector divided by the scalar aScalar (equivalent to aVector multiplied by 1 / aScalar)
	template <class T> Vector3<T> operator/(const Vector3<T>& aVector, const T& aScalar) 
	{
		T scale = (1 / aScalar);
		return Vector3<T>((aVector.x * scale), (aVector.y * scale), (aVector.z * scale));
	}

	//Equivalent to setting aVector0 to (aVector0 + aVector1)
	template <class T> void operator+=(Vector3<T>& aVector0, const Vector3<T>& aVector1) 
	{
		aVector0.x = (aVector0.x + aVector1.x);
		aVector0.y = (aVector0.y + aVector1.y);
		aVector0.z = (aVector0.z + aVector1.z);
	}


	//Equivalent to setting aVector0 to (aVector0 - aVector1)
	template <class T> void operator-=(Vector3<T>& aVector0, const Vector3<T>& aVector1) 
	{
		aVector0.x = (aVector0.x - aVector1.x);
		aVector0.y = (aVector0.y - aVector1.y);
		aVector0.z = (aVector0.z - aVector1.z);
	}


	//Equivalent to setting aVector to (aVector * aScalar)
	template <class T> void operator*=(Vector3<T>& aVector, const T& aScalar) 
	{
		aVector.x = (aVector.x * aScalar);
		aVector.y = (aVector.y * aScalar);
		aVector.z = (aVector.z * aScalar);
	}

	template <class T> bool operator==(const Vector3<T>& aVec0, const Vector3<T>& aVec1)
	{
		if (aVec0.x == aVec1.x &&
			aVec0.y == aVec1.y &&
			aVec0.z == aVec1.z)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	template <class T> bool operator!=(const Vector3<T>& aVec0, const Vector3<T>& aVec1)
	{
		return !(aVec0 == aVec1);
	}


	//Equivalent to setting aVector to (aVector / aScalar)
	template <class T> void operator/=(Vector3<T>& aVector, const T& aScalar) 
	{
		T scale = (1 / aScalar);
		aVector.x = (aVector.x * scale);
		aVector.y = (aVector.y * scale);
		aVector.z = (aVector.z * scale);
	}

	typedef Vector3<char> Vector3c;
	typedef Vector3<int> Vector3i;
	typedef Vector3<unsigned int> Vector3ui;
	typedef Vector3<float> Vector3f;
	typedef Vector3<double> Vector3d;

	typedef Vector3<char> Point3c;
	typedef Vector3<int> Point3i;
	typedef Vector3<unsigned int> Point3ui;
	typedef Vector3<float> Point3f;
	typedef Vector3<double> Point3d;

	template<typename T> const Vector3<T> Vector3<T>::Zero(0, 0, 0);
	template<typename T> const Vector3<T> Vector3<T>::UnitX(1, 0, 0);
	template<typename T> const Vector3<T> Vector3<T>::UnitY(0, 1, 0);
	template<typename T> const Vector3<T> Vector3<T>::UnitZ(0, 0, 1);
	template<typename T> const Vector3<T> Vector3<T>::One(1, 1, 1);
	template<typename T> const Vector3<T> Vector3<T>::Forward(0, 0, 1);
	template<typename T> const Vector3<T> Vector3<T>::Backward(0, 0, -1);
	template<typename T> const Vector3<T> Vector3<T>::Left(-1, 0, 0);
	template<typename T> const Vector3<T> Vector3<T>::Right(1, 0, 0);
	template<typename T> const Vector3<T> Vector3<T>::Up(0, 1, 0);
	template<typename T> const Vector3<T> Vector3<T>::Down(0, -1, 0);
	template<typename T> const Vector3<T> Vector3<T>::Max(FLT_MAX, FLT_MAX, FLT_MAX);
}