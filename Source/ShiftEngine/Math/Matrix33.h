#pragma once
#include "Matrix44.h"
#include "Vector.h"
#include <string>

#define MATRIX33_SIZE 9

namespace Shift3D
{
	template <typename ObjectType>
	class Matrix33
	{
	public:
		ObjectType myMatrix[MATRIX33_SIZE];


		Matrix33();
		Matrix33(const Matrix33& aMatrix);
		Matrix33(const Matrix44<ObjectType>& aMatrix);
		Matrix33(const ObjectType a11, const ObjectType a12, const ObjectType a13,
			const ObjectType a21, const ObjectType a22, const ObjectType a23,
			const ObjectType a31, const ObjectType a32, const ObjectType a33);
		~Matrix33();

		Matrix33 operator+(const Matrix33& aMatrix) const;
		Matrix33& operator+=(const Matrix33& aMatrix);
		Matrix33 operator-(const Matrix33& aMatrix) const;
		Matrix33& operator-=(const Matrix33& aMatrix);
		Matrix33 operator*(const Matrix33& aMatrix) const;
		Matrix33& operator*=(const Matrix33& aMatrix);
		bool operator==(const Matrix33& aMatrix) const;
		Matrix33& operator=(const Matrix33& aMatrix);
		const ObjectType& operator[](const int aIndex) const;
		ObjectType& operator[](const int aIndex);

		Vector3<ObjectType> operator*(const Vector3<ObjectType>& aVector);

		static Matrix33 CreateRotateAroundX(ObjectType aAngleInRadius);
		static Matrix33 CreateRotateAroundY(ObjectType aAngleInRadius);
		static Matrix33 CreateRotateAroundZ(ObjectType aAngleInRadius);

		static Matrix33 Transpose(const Matrix33& aMatrixToTranspose);
		

		Matrix33 MakeRotationMatrix(const Vector3<ObjectType>& direction, const Vector3<ObjectType>& up = Vector3<ObjectType>(1.0f, 0.0f, 0.0f));
	};
	
	template<typename ObjectType>
	inline Matrix33<ObjectType>::Matrix33()
	{
		for (int i = 0; i < MATRIX33_SIZE; i++)
		{
			if (i % 4 == 0)
			{
				myMatrix[i] = static_cast<ObjectType>(1);
			}
			else
			{
				myMatrix[i] = static_cast<ObjectType>(0);
			}
		}
	}

	template<typename ObjectType>
	inline Matrix33<ObjectType>::Matrix33(const Matrix33& aMatrix)
	{
		for (int i = 0; i < MATRIX33_SIZE; i++)
		{
			myMatrix[i] = aMatrix.myMatrix[i];
		}
	}

	template<typename ObjectType>
	inline Matrix33<ObjectType>::Matrix33(const Matrix44<ObjectType>& aMatrix)
	{
		for (int i = 0; i < (MATRIX33_SIZE) + 1; i++)
		{
			if(i <= 2)
			{
				myMatrix[i] = aMatrix.myMatrix[i];
			}
			else if (i <= 5)
			{
				myMatrix[i] = aMatrix.myMatrix[i + 1];
			}
			else if(i <= 8)
			{
				myMatrix[i] = aMatrix.myMatrix[i + 2];
			}
		}
	}

	template<typename ObjectType>
	inline Matrix33<ObjectType>::Matrix33(const ObjectType a11, const ObjectType a12, const ObjectType a13, const ObjectType a21, const ObjectType a22, const ObjectType a23, const ObjectType a31, const ObjectType a32, const ObjectType a33)
	{
		myMatrix[0] = a11;
		myMatrix[1] = a12;
		myMatrix[2] = a13;

		myMatrix[3] = a21;
		myMatrix[4] = a22;
		myMatrix[5] = a23;

		myMatrix[6] = a31;
		myMatrix[7] = a32;
		myMatrix[8] = a33;
	}

	template<typename ObjectType>
	inline Matrix33<ObjectType>::~Matrix33()
	{
	}

	template<typename ObjectType>
	inline Matrix33<ObjectType> Matrix33<ObjectType>::operator+(const Matrix33<ObjectType>& aMatrix) const
	{
		Shift3D::Matrix33<ObjectType> newMatrix;
		for (int i = 0; i < MATRIX33_SIZE; i++)
		{
			newMatrix.myMatrix[i] = (myMatrix[i] + aMatrix.myMatrix[i]);
		}

		return newMatrix;
	}

	template<typename ObjectType>
	inline Matrix33<ObjectType>& Matrix33<ObjectType>::operator+=(const Matrix33<ObjectType>& aMatrix) 
	{
		for (int i = 0; i < MATRIX33_SIZE; i++)
		{
			myMatrix[i] = (myMatrix[i] + aMatrix.myMatrix[i]);
		}
		return *(this);
	}

	template<typename ObjectType>
	inline Matrix33<ObjectType> Matrix33<ObjectType>::operator-(const Matrix33<ObjectType>& aMatrix) const
	{
		Shift3D::Matrix33<ObjectType> newMatrix;
		for (int i = 0; i < MATRIX33_SIZE; i++)
		{
			newMatrix.myMatrix[i] = (myMatrix[i] - aMatrix.myMatrix[i]);
		}

		return newMatrix;
	}

	template<typename ObjectType>
	inline Matrix33<ObjectType>& Matrix33<ObjectType>::operator-=(const Matrix33<ObjectType>& aMatrix) 
	{
		for (int i = 0; i < MATRIX33_SIZE; i++)
		{
			myMatrix[i] = (myMatrix[i] - aMatrix.myMatrix[i]);
		}
		return *(this);
	}

	template<typename ObjectType>
	inline Matrix33<ObjectType> Matrix33<ObjectType>::operator*(const Matrix33<ObjectType>& aMatrix) const
	{
		Matrix33<ObjectType> newMatrix;

		newMatrix[0] = (myMatrix[0] * aMatrix.myMatrix[0]) + (myMatrix[1] * aMatrix.myMatrix[3]) + (myMatrix[2] * aMatrix.myMatrix[6]);
		newMatrix[1] = (myMatrix[0] * aMatrix.myMatrix[1]) + (myMatrix[1] * aMatrix.myMatrix[4]) + (myMatrix[2] * aMatrix.myMatrix[7]);
		newMatrix[2] = (myMatrix[0] * aMatrix.myMatrix[2]) + (myMatrix[1] * aMatrix.myMatrix[5]) + (myMatrix[2] * aMatrix.myMatrix[8]);

		newMatrix[3] = (myMatrix[3] * aMatrix.myMatrix[0]) + (myMatrix[4] * aMatrix.myMatrix[3]) + (myMatrix[5] * aMatrix.myMatrix[6]);
		newMatrix[4] = (myMatrix[3] * aMatrix.myMatrix[1]) + (myMatrix[4] * aMatrix.myMatrix[4]) + (myMatrix[5] * aMatrix.myMatrix[7]);
		newMatrix[5] = (myMatrix[3] * aMatrix.myMatrix[2]) + (myMatrix[4] * aMatrix.myMatrix[5]) + (myMatrix[5] * aMatrix.myMatrix[8]);

		newMatrix[6] = (myMatrix[6] * aMatrix.myMatrix[0]) + (myMatrix[7] * aMatrix.myMatrix[3]) + (myMatrix[8] * aMatrix.myMatrix[6]);
		newMatrix[7] = (myMatrix[6] * aMatrix.myMatrix[1]) + (myMatrix[7] * aMatrix.myMatrix[4]) + (myMatrix[8] * aMatrix.myMatrix[7]);
		newMatrix[8] = (myMatrix[6] * aMatrix.myMatrix[2]) + (myMatrix[7] * aMatrix.myMatrix[5]) + (myMatrix[8] * aMatrix.myMatrix[8]);

		return newMatrix;
	}

	template<typename ObjectType>
	inline Matrix33<ObjectType>& Matrix33<ObjectType>::operator*=(const Matrix33<ObjectType>& aMatrix)
	{
		ObjectType tempMatrix[9];
		tempMatrix[0] = myMatrix[0]; tempMatrix[1] = myMatrix[1]; tempMatrix[2] = myMatrix[2];
		tempMatrix[3] = myMatrix[3]; tempMatrix[4] = myMatrix[4]; tempMatrix[5] = myMatrix[5];
		tempMatrix[6] = myMatrix[6]; tempMatrix[7] = myMatrix[7]; tempMatrix[8] = myMatrix[8];

		myMatrix[0] = (tempMatrix[0] * aMatrix[0]) + (tempMatrix[1] * aMatrix[3]) + (tempMatrix[2] * aMatrix[6]);
		myMatrix[1] = (tempMatrix[0] * aMatrix[1]) + (tempMatrix[1] * aMatrix[4]) + (tempMatrix[2] * aMatrix[7]);
		myMatrix[2] = (tempMatrix[0] * aMatrix[2]) + (tempMatrix[1] * aMatrix[5]) + (tempMatrix[2] * aMatrix[8]);

		myMatrix[3] = (tempMatrix[3] * aMatrix[0]) + (tempMatrix[4] * aMatrix[3]) + (tempMatrix[5] * aMatrix[6]);
		myMatrix[4] = (tempMatrix[3] * aMatrix[1]) + (tempMatrix[4] * aMatrix[4]) + (tempMatrix[5] * aMatrix[7]);
		myMatrix[5] = (tempMatrix[3] * aMatrix[2]) + (tempMatrix[4] * aMatrix[5]) + (tempMatrix[5] * aMatrix[8]);

		myMatrix[6] = (tempMatrix[6] * aMatrix[0]) + (tempMatrix[7] * aMatrix[3]) + (tempMatrix[8] * aMatrix[6]);
		myMatrix[7] = (tempMatrix[6] * aMatrix[1]) + (tempMatrix[7] * aMatrix[4]) + (tempMatrix[8] * aMatrix[7]);
		myMatrix[8] = (tempMatrix[6] * aMatrix[2]) + (tempMatrix[7] * aMatrix[5]) + (tempMatrix[8] * aMatrix[8]);
		return *this;
	}

	template<typename ObjectType>
	inline bool Matrix33<ObjectType>::operator==(const Matrix33<ObjectType>& aMatrix) const
	{
		for (int i = 0; i < MATRIX33_SIZE; i++)
		{
			if (myMatrix[i] != aMatrix.myMatrix[i])
			{
				return false;
			}
		}

		return true;
	}

	template<typename ObjectType>
	inline Matrix33<ObjectType>& Matrix33<ObjectType>::operator=(const Matrix33<ObjectType>& aMatrix)
	{
		for (int i = 0; i < MATRIX33_SIZE; i++)
		{
			myMatrix[i] = aMatrix.myMatrix[i];
		}
		return *(this);
	}

	template<typename ObjectType>
	inline const ObjectType& Matrix33<ObjectType>::operator[](const int aIndex) const
	{
		assert(aIndex >= 0 && aIndex < 9 && "Index out of bounds");
		return myMatrix[aIndex];
	}

	template<typename ObjectType>
	inline ObjectType& Matrix33<ObjectType>::operator[](const int aIndex)
	{
		assert(aIndex >= 0 && aIndex < 9 && "Index out of bounds");
		return myMatrix[aIndex];
	}

	template<typename ObjectType>
	inline Vector3<ObjectType> Matrix33<ObjectType>::operator*(const Vector3<ObjectType>& aVector3)
	{
		Vector3<ObjectType> newVector;

		Vector3<ObjectType> column1 = { myMatrix[0], myMatrix[3], myMatrix[6] };
		Vector3<ObjectType> column2 = { myMatrix[1], myMatrix[4], myMatrix[7] };
		Vector3<ObjectType> column3 = { myMatrix[2], myMatrix[5], myMatrix[8] };

		newVector.x = aVector3.x * column1.x + aVector3.y * column1.y + aVector3.z * column1.z;
		newVector.y = aVector3.x * column2.x + aVector3.y * column2.y + aVector3.z * column2.z;
		newVector.z = aVector3.x * column3.x + aVector3.y * column3.y + aVector3.z * column3.z;

		return newVector;
	}

	template<class ObjectType>
	inline Vector3<ObjectType> operator*(const Vector3<ObjectType>& aVector3, const Matrix33<ObjectType>& aMatrix33)
	{
		Vector3<ObjectType> newVector;

		Vector3<ObjectType> column1 = { aMatrix33.myMatrix[0], aMatrix33.myMatrix[3], aMatrix33.myMatrix[6] };
		Vector3<ObjectType> column2 = { aMatrix33.myMatrix[1], aMatrix33.myMatrix[4], aMatrix33.myMatrix[7] };
		Vector3<ObjectType> column3 = { aMatrix33.myMatrix[2], aMatrix33.myMatrix[5], aMatrix33.myMatrix[8] };

		newVector.x = aVector3.x * column1.x + aVector3.y * column1.y + aVector3.z * column1.z;
		newVector.y = aVector3.x * column2.x + aVector3.y * column2.y + aVector3.z * column2.z;
		newVector.z = aVector3.x * column3.x + aVector3.y * column3.y + aVector3.z * column3.z;

		return newVector;
	}

	template<typename ObjectType>
	inline Matrix33<ObjectType> Matrix33<ObjectType>::CreateRotateAroundX(ObjectType aAngleInRadius)
	{
		ObjectType cosinus = static_cast<ObjectType>(std::cos(static_cast<double>(aAngleInRadius)));
		ObjectType sinus = static_cast<ObjectType>(std::sin(static_cast<double>(aAngleInRadius)));
		Matrix33<ObjectType> rotationMatrix;
		rotationMatrix.myMatrix[0] = 1;
		rotationMatrix.myMatrix[1] = 0;
		rotationMatrix.myMatrix[2] = 0;
		rotationMatrix.myMatrix[3] = 0;
		rotationMatrix.myMatrix[4] = cosinus;
		rotationMatrix.myMatrix[5] = sinus;
		rotationMatrix.myMatrix[6] = 0;
		rotationMatrix.myMatrix[7] = -sinus;
		rotationMatrix.myMatrix[8] = cosinus;

		return rotationMatrix;
	}

	template<typename ObjectType>
	inline Matrix33<ObjectType> Matrix33<ObjectType>::CreateRotateAroundY(ObjectType aAngleInRadius)
	{
		ObjectType cosinus = static_cast<ObjectType>(std::cos(static_cast<double>(aAngleInRadius)));
		ObjectType sinus = static_cast<ObjectType>(std::sin(static_cast<double>(aAngleInRadius)));

		Matrix33<ObjectType> rotationMatrix;
		rotationMatrix.myMatrix[0] = cosinus;
		rotationMatrix.myMatrix[1] = 0;
		rotationMatrix.myMatrix[2] = -sinus;
		rotationMatrix.myMatrix[3] = 0;
		rotationMatrix.myMatrix[4] = 1;
		rotationMatrix.myMatrix[5] = 0;
		rotationMatrix.myMatrix[6] = sinus;
		rotationMatrix.myMatrix[7] = 0;
		rotationMatrix.myMatrix[8] = cosinus;

		return rotationMatrix;
	}

	template<typename ObjectType>
	inline Matrix33<ObjectType> Matrix33<ObjectType>::CreateRotateAroundZ(ObjectType aAngleInRadius)
	{
		ObjectType cosinus = static_cast<ObjectType>(std::cos(static_cast<double>(aAngleInRadius)));
		ObjectType sinus = static_cast<ObjectType>(std::sin(static_cast<double>(aAngleInRadius)));

		Matrix33<ObjectType> rotationMatrix;
		rotationMatrix.myMatrix[0] = cosinus;
		rotationMatrix.myMatrix[1] = sinus;
		rotationMatrix.myMatrix[2] = 0;

		rotationMatrix.myMatrix[3] = -sinus;
		rotationMatrix.myMatrix[4] = cosinus;
		rotationMatrix.myMatrix[5] = 0;

		rotationMatrix.myMatrix[6] = 0;
		rotationMatrix.myMatrix[7] = 0;
		rotationMatrix.myMatrix[8] = 1;

		return rotationMatrix;
	}

	template<typename ObjectType>
	inline Matrix33<ObjectType> Matrix33<ObjectType>::Transpose(const Matrix33& aMatrixToTranspose)
	{
		Shift3D::Matrix33<ObjectType> transposedMatrix;

		transposedMatrix.myMatrix[0] = aMatrixToTranspose.myMatrix[0];
		transposedMatrix.myMatrix[1] = aMatrixToTranspose.myMatrix[3];
		transposedMatrix.myMatrix[2] = aMatrixToTranspose.myMatrix[6];
		transposedMatrix.myMatrix[3] = aMatrixToTranspose.myMatrix[1];
		transposedMatrix.myMatrix[4] = aMatrixToTranspose.myMatrix[4];
		transposedMatrix.myMatrix[5] = aMatrixToTranspose.myMatrix[7];
		transposedMatrix.myMatrix[6] = aMatrixToTranspose.myMatrix[2];
		transposedMatrix.myMatrix[7] = aMatrixToTranspose.myMatrix[5];
		transposedMatrix.myMatrix[8] = aMatrixToTranspose.myMatrix[8];

		return transposedMatrix;
	}
	template<typename ObjectType>
	inline Matrix33<ObjectType> Matrix33<ObjectType>::MakeRotationMatrix(const Vector3<ObjectType>& direction, const Vector3<ObjectType>& up)
	{

		Matrix33 matrix;
		Vector3<ObjectType> xaxis = up.Cross(direction);
		xaxis.Normalize();

		Vector3<ObjectType> yaxis = direction.Cross(xaxis);
		yaxis.Normalize();

		matrix[0] = xaxis.x;
		matrix[1] = yaxis.x;
		matrix[2] = direction.x;

		matrix[3] = xaxis.y;
		matrix[4] = yaxis.y;
		matrix[5] = direction.y;

		matrix[6] = xaxis.z;
		matrix[7] = yaxis.z;
		matrix[8] = direction.z;

		return matrix;
	}

	typedef Matrix33<float> Matrix33f;
}