#pragma once
#include "Vector.h"
#include <assert.h>

#define MATRIX44_SIZE 16

namespace Shift3D
{
	template <typename ObjectType>
	class Matrix44
	{
	public:
		// For handling "nameless union/struct"-warning.
		#pragma warning(disable : 4201)
		union
		{
			struct
			{
				ObjectType	m11, m12, m13, m14,
					m21, m22, m23, m24,
					m31, m32, m33, m34,
					m41, m42, m43, m44;
			};
			ObjectType myMatrix[MATRIX44_SIZE];
		};
		

		Matrix44();
		Matrix44(const Matrix44& aMatrix);
		Matrix44(const ObjectType a11, const ObjectType a12, const ObjectType a13, const ObjectType a14,
			const ObjectType a21, const ObjectType a22, const ObjectType a23, const ObjectType a24,
			const ObjectType a31, const ObjectType a32, const ObjectType a33, const ObjectType a34,
			const ObjectType a41, const ObjectType a42, const ObjectType a43, const ObjectType a44);
		~Matrix44();

		Matrix44 operator+(const Matrix44& aMatrix) const;
		Matrix44& operator+=(const Matrix44& aMatrix);
		Matrix44 operator-(const Matrix44& aMatrix) const;
		Matrix44& operator-=(const Matrix44& aMatrix);
		Matrix44 operator*(const Matrix44& aMatrix) const;
		Matrix44& operator*=(const Matrix44& aMatrix);
		bool operator==(const Matrix44& aMatrix) const;
		Matrix44& operator=(const Matrix44& aMatrix);
		ObjectType& operator[](const int aIndex);
		const ObjectType& operator[](const int aIndex) const;

		void SetTranslation(const Vector4<ObjectType>& aPoint);

		Vector3<ObjectType> GetPosition() const;
		void SetPosition(const Vector3<ObjectType>& aPosition);
		Vector4<ObjectType> GetTranslation() const;
		Vector3<ObjectType> GetScale() const;


		const Vector3<ObjectType> GetRotationVector() const
		{
			Vector3<ObjectType> temp;
			temp.x = atan2(myMatrix[9], myMatrix[10]);
			temp.y = atan2(-myMatrix[8], sqrt((myMatrix[9] * myMatrix[9]) + (myMatrix[10] * myMatrix[10])));
			temp.z = atan2(myMatrix[4], myMatrix[0]);

			return temp;
		}

		Matrix44<ObjectType> GetFastInverse() const;

		Vector4<ObjectType> operator*(const Vector4<ObjectType>& aVector);

		Matrix44<ObjectType> CreateProjectionMatrix(ObjectType aNearZ, ObjectType aFarZ, ObjectType aWidth, ObjectType aHeight, ObjectType aFovAngle, bool aIsLeftHanded = true)
		{
			Matrix44<ObjectType> temp;
			ObjectType aspectRatio = aWidth / aHeight; 
			if (aspectRatio == static_cast<ObjectType>(0) || aFovAngle == static_cast<ObjectType>(0))
			{
				assert(false && "Invalid input");
				return Matrix44f::Identity;
			}

			float yScale = 1.0f / tan(aFovAngle / 2.0f);
			float xScale = yScale / aspectRatio; 
			float nearFarDiff = 0.f; 
			float rightOrLeft = 0.f;
			if (aIsLeftHanded)
			{
				rightOrLeft = 1.0f;
				nearFarDiff = aFarZ - aNearZ;
			}
			else
			{
				rightOrLeft = -1.0f;
				nearFarDiff = aNearZ - aFarZ;
			}
			
			temp.myMatrix[0] = xScale;
			temp.myMatrix[1] = 0.0f;
			temp.myMatrix[2] = 0.0f;
			temp.myMatrix[3] = 0.0f;

			temp.myMatrix[4] = 0.0f;
			temp.myMatrix[5] = yScale;
			temp.myMatrix[6] = 0.0f;
			temp.myMatrix[7] = 0.0f;

			temp.myMatrix[8] = 0.0f;
			temp.myMatrix[9] = 0.0f;
			temp.myMatrix[10] = aFarZ / nearFarDiff;
			temp.myMatrix[11] = rightOrLeft;

			temp.myMatrix[12] = 0.0f;
			temp.myMatrix[13] = 0.0f;
			temp.myMatrix[14] = -(aFarZ / nearFarDiff) * aNearZ;
			temp.myMatrix[15] = 0.0f;

			return temp;
		}

		static Matrix44 CreateRotateAroundX(ObjectType aAngleInRadius);
		static Matrix44 CreateRotateAroundY(ObjectType aAngleInRadius);
		static Matrix44 CreateRotateAroundZ(ObjectType aAngleInRadius);
		static Matrix44<ObjectType> CreateScale(const ObjectType x, const ObjectType y, const ObjectType z)
		{
			return Matrix44<ObjectType>(
				x, 0, 0, 0,
				0, y, 0, 0,
				0, 0, z, 0,
				0, 0, 0, 1
			);
		}

		
		static Matrix44 Transpose(const Matrix44& aMatrixToTranspose);
		static Matrix44 CreateTranslation(const Vector4<ObjectType>& aPoint);

		static const Matrix44 Identity, Zero;

	};
	
	template<typename ObjectType>
	inline Matrix44<ObjectType>::Matrix44()
	{
		for (int i = 0; i < MATRIX44_SIZE; i++)
		{
			if (i % 5 == 0)
			{
				myMatrix[i] = static_cast<ObjectType>(1);
			}
			else
			{
				myMatrix[i] = static_cast<ObjectType>(0);
			}
		}
	}
	
	template<typename ObjectType>
	inline Matrix44<ObjectType>::Matrix44(const Matrix44 & aMatrix)
	{
		for (int i = 0; i < MATRIX44_SIZE; i++)
		{
			myMatrix[i] = aMatrix.myMatrix[i];
		}
	}

	template<typename ObjectType>
	inline Matrix44<ObjectType>::Matrix44(const ObjectType a11, const ObjectType a12, const ObjectType a13, const ObjectType a14,
		const ObjectType a21, const ObjectType a22, const ObjectType a23, const ObjectType a24,
		const ObjectType a31, const ObjectType a32, const ObjectType a33, const ObjectType a34,
		const ObjectType a41, const ObjectType a42, const ObjectType a43, const ObjectType a44)
	{
		myMatrix[0] = a11;
		myMatrix[1] = a12;
		myMatrix[2] = a13;
		myMatrix[3] = a14;

		myMatrix[4] = a21;
		myMatrix[5] = a22;
		myMatrix[6] = a23;
		myMatrix[7] = a24;

		myMatrix[8] = a31;
		myMatrix[9] = a32;
		myMatrix[10] = a33;
		myMatrix[11] = a34;

		myMatrix[12] = a41;
		myMatrix[13] = a42;
		myMatrix[14] = a43;
		myMatrix[15] = a44;
	}
	
	template<typename ObjectType>
	inline Matrix44<ObjectType>::~Matrix44()
	{
	}

	template<typename ObjectType>
	inline Matrix44<ObjectType> Matrix44<ObjectType>::operator+(const Matrix44 & aMatrix) const
	{
		Shift3D::Matrix44<ObjectType> newMatrix;
		for (int i = 0; i < MATRIX44_SIZE; i++)
		{
			newMatrix.myMatrix[i] = (myMatrix[i] + aMatrix.myMatrix[i]);
		}

		return newMatrix;
	}

	template<typename ObjectType>
	inline Matrix44<ObjectType>& Matrix44<ObjectType>::operator+=(const Matrix44 & aMatrix)
	{
		for (int i = 0; i < MATRIX44_SIZE; i++)
		{
			myMatrix[i] = (myMatrix[i] + aMatrix.myMatrix[i]);
		}

		return *(this);
	}

	template<typename ObjectType>
	inline Matrix44<ObjectType> Matrix44<ObjectType>::operator-(const Matrix44 & aMatrix) const
	{
		Shift3D::Matrix44<ObjectType> newMatrix;
		for (int i = 0; i < MATRIX44_SIZE; i++)
		{
			newMatrix.myMatrix[i] = (myMatrix[i] - aMatrix.myMatrix[i]);
		}

		return newMatrix;
	}

	template<typename ObjectType>
	inline Matrix44<ObjectType>& Matrix44<ObjectType>::operator-=(const Matrix44 & aMatrix)
	{
		for (int i = 0; i < MATRIX44_SIZE; i++)
		{
			myMatrix[i] = (myMatrix[i] - aMatrix.myMatrix[i]);
		}

		return *(this);
	}
	
	template<typename ObjectType>
	inline Matrix44<ObjectType> Matrix44<ObjectType>::CreateRotateAroundX(ObjectType aAngleInRadius)
	{
		ObjectType cosinus = static_cast<ObjectType>(std::cos(static_cast<double>(aAngleInRadius)));
		ObjectType sinus = static_cast<ObjectType>(std::sin(static_cast<double>(aAngleInRadius)));

		Matrix44<ObjectType> rotationMatrix;
		rotationMatrix.myMatrix[0] = 1;
		rotationMatrix.myMatrix[1] = 0;
		rotationMatrix.myMatrix[2] = 0;
		rotationMatrix.myMatrix[3] = 0;
		rotationMatrix.myMatrix[4] = 0;
		rotationMatrix.myMatrix[5] = cosinus;
		rotationMatrix.myMatrix[6] = sinus;
		rotationMatrix.myMatrix[7] = 0;
		rotationMatrix.myMatrix[8] = 0;
		rotationMatrix.myMatrix[9] = -sinus;
		rotationMatrix.myMatrix[10] = cosinus;
		rotationMatrix.myMatrix[11] = 0;
		rotationMatrix.myMatrix[12] = 0;
		rotationMatrix.myMatrix[13] = 0;
		rotationMatrix.myMatrix[14] = 0;
		rotationMatrix.myMatrix[15] = 1;

		return rotationMatrix;
	}
	
	template<typename ObjectType>
	inline Matrix44<ObjectType> Matrix44<ObjectType>::CreateRotateAroundY(ObjectType aAngleInRadius)
	{
		ObjectType cosinus = static_cast<ObjectType>(std::cos(static_cast<double>(aAngleInRadius)));
		ObjectType sinus = static_cast<ObjectType>(std::sin(static_cast<double>(aAngleInRadius)));

		Matrix44<ObjectType> rotationMatrix;
		rotationMatrix.myMatrix[0] = cosinus;
		rotationMatrix.myMatrix[1] = 0;
		rotationMatrix.myMatrix[2] = -sinus;
		rotationMatrix.myMatrix[3] = 0;
		rotationMatrix.myMatrix[4] = 0;
		rotationMatrix.myMatrix[5] = 1;
		rotationMatrix.myMatrix[6] = 0;
		rotationMatrix.myMatrix[7] = 0;
		rotationMatrix.myMatrix[8] = sinus;
		rotationMatrix.myMatrix[9] = 0;
		rotationMatrix.myMatrix[10] = cosinus;
		rotationMatrix.myMatrix[11] = 0;
		rotationMatrix.myMatrix[12] = 0;
		rotationMatrix.myMatrix[13] = 0;
		rotationMatrix.myMatrix[14] = 0;
		rotationMatrix.myMatrix[15] = 1;

		return rotationMatrix;
	}
	
	template<typename ObjectType>
	inline Matrix44<ObjectType> Matrix44<ObjectType>::CreateRotateAroundZ(ObjectType aAngleInRadius)
	{
		ObjectType cosinus = static_cast<ObjectType>(std::cos(static_cast<double>(aAngleInRadius)));
		ObjectType sinus = static_cast<ObjectType>(std::sin(static_cast<double>(aAngleInRadius)));

		Matrix44<ObjectType> rotationMatrix;
		rotationMatrix.myMatrix[0] = cosinus;
		rotationMatrix.myMatrix[1] = sinus;
		rotationMatrix.myMatrix[2] = 0;
		rotationMatrix.myMatrix[3] = 0;
		rotationMatrix.myMatrix[4] = -sinus;
		rotationMatrix.myMatrix[5] = cosinus;
		rotationMatrix.myMatrix[6] = 0;
		rotationMatrix.myMatrix[7] = 0;
		rotationMatrix.myMatrix[8] = 0;
		rotationMatrix.myMatrix[9] = 0;
		rotationMatrix.myMatrix[10] = 1;
		rotationMatrix.myMatrix[11] = 0;
		rotationMatrix.myMatrix[12] = 0;
		rotationMatrix.myMatrix[13] = 0;
		rotationMatrix.myMatrix[14] = 0;
		rotationMatrix.myMatrix[15] = 1;

		return rotationMatrix;
	}
	
	template<typename ObjectType>
	inline Matrix44<ObjectType> Matrix44<ObjectType>::Transpose(const Matrix44 & aMatrixToTranspose)
	{
		Shift3D::Matrix44<ObjectType> transposedMatrix;

		transposedMatrix.myMatrix[0] = aMatrixToTranspose.myMatrix[0];
		transposedMatrix.myMatrix[1] = aMatrixToTranspose.myMatrix[4];
		transposedMatrix.myMatrix[2] = aMatrixToTranspose.myMatrix[8];
		transposedMatrix.myMatrix[3] = aMatrixToTranspose.myMatrix[12];
		transposedMatrix.myMatrix[4] = aMatrixToTranspose.myMatrix[1];
		transposedMatrix.myMatrix[5] = aMatrixToTranspose.myMatrix[5];
		transposedMatrix.myMatrix[6] = aMatrixToTranspose.myMatrix[9];
		transposedMatrix.myMatrix[7] = aMatrixToTranspose.myMatrix[13];
		transposedMatrix.myMatrix[8] = aMatrixToTranspose.myMatrix[2];
		transposedMatrix.myMatrix[9] = aMatrixToTranspose.myMatrix[6];
		transposedMatrix.myMatrix[10] = aMatrixToTranspose.myMatrix[10];
		transposedMatrix.myMatrix[11] = aMatrixToTranspose.myMatrix[14];
		transposedMatrix.myMatrix[12] = aMatrixToTranspose.myMatrix[3];
		transposedMatrix.myMatrix[13] = aMatrixToTranspose.myMatrix[7];
		transposedMatrix.myMatrix[14] = aMatrixToTranspose.myMatrix[11];
		transposedMatrix.myMatrix[15] = aMatrixToTranspose.myMatrix[15];

		return transposedMatrix;
	}

	template<typename ObjectType>
	inline Matrix44<ObjectType> Matrix44<ObjectType>::operator*(const Matrix44 & aMatrix) const
	{
		Matrix44<ObjectType> newMatrix;

		newMatrix[0] = myMatrix[0] * aMatrix[0] + myMatrix[1] * aMatrix[4] + myMatrix[2] * aMatrix[8] + myMatrix[3] * aMatrix[12];
		newMatrix[1] = myMatrix[0] * aMatrix[1] + myMatrix[1] * aMatrix[5] + myMatrix[2] * aMatrix[9] + myMatrix[3] * aMatrix[13];
		newMatrix[2] = myMatrix[0] * aMatrix[2] + myMatrix[1] * aMatrix[6] + myMatrix[2] * aMatrix[10] + myMatrix[3] * aMatrix[14];
		newMatrix[3] = myMatrix[0] * aMatrix[3] + myMatrix[1] * aMatrix[7] + myMatrix[2] * aMatrix[11] + myMatrix[3] * aMatrix[15];

		newMatrix[4] = myMatrix[4] * aMatrix[0] + myMatrix[5] * aMatrix[4] + myMatrix[6] * aMatrix[8] + myMatrix[7] * aMatrix[12];
		newMatrix[5] = myMatrix[4] * aMatrix[1] + myMatrix[5] * aMatrix[5] + myMatrix[6] * aMatrix[9] + myMatrix[7] * aMatrix[13];
		newMatrix[6] = myMatrix[4] * aMatrix[2] + myMatrix[5] * aMatrix[6] + myMatrix[6] * aMatrix[10] + myMatrix[7] * aMatrix[14];
		newMatrix[7] = myMatrix[4] * aMatrix[3] + myMatrix[5] * aMatrix[7] + myMatrix[6] * aMatrix[11] + myMatrix[7] * aMatrix[15];

		newMatrix[8] = myMatrix[8] * aMatrix[0] + myMatrix[9] * aMatrix[4] + myMatrix[10] * aMatrix[8] + myMatrix[11] * aMatrix[12];
		newMatrix[9] = myMatrix[8] * aMatrix[1] + myMatrix[9] * aMatrix[5] + myMatrix[10] * aMatrix[9] + myMatrix[11] * aMatrix[13];
		newMatrix[10] = myMatrix[8] * aMatrix[2] + myMatrix[9] * aMatrix[6] + myMatrix[10] * aMatrix[10] + myMatrix[11] * aMatrix[14];
		newMatrix[11] = myMatrix[8] * aMatrix[3] + myMatrix[9] * aMatrix[7] + myMatrix[10] * aMatrix[11] + myMatrix[11] * aMatrix[15];

		newMatrix[12] = myMatrix[12] * aMatrix[0] + myMatrix[13] * aMatrix[4] + myMatrix[14] * aMatrix[8] + myMatrix[15] * aMatrix[12];
		newMatrix[13] = myMatrix[12] * aMatrix[1] + myMatrix[13] * aMatrix[5] + myMatrix[14] * aMatrix[9] + myMatrix[15] * aMatrix[13];
		newMatrix[14] = myMatrix[12] * aMatrix[2] + myMatrix[13] * aMatrix[6] + myMatrix[14] * aMatrix[10] + myMatrix[15] * aMatrix[14];
		newMatrix[15] = myMatrix[12] * aMatrix[3] + myMatrix[13] * aMatrix[7] + myMatrix[14] * aMatrix[11] + myMatrix[15] * aMatrix[15];

		return newMatrix;
	}

	template<typename ObjectType>
	inline Matrix44<ObjectType>& Matrix44<ObjectType>::operator*=(const Matrix44 & aMatrix)
	{
		ObjectType tempMatrix[16];
		tempMatrix[0] = myMatrix[0]; tempMatrix[1] = myMatrix[1]; tempMatrix[2] = myMatrix[2]; tempMatrix[3] = myMatrix[3];
		tempMatrix[4] = myMatrix[4]; tempMatrix[5] = myMatrix[5]; tempMatrix[6] = myMatrix[6]; tempMatrix[7] = myMatrix[7];
		tempMatrix[8] = myMatrix[8]; tempMatrix[9] = myMatrix[9]; tempMatrix[10] = myMatrix[10]; tempMatrix[11] = myMatrix[11];
		tempMatrix[12] = myMatrix[12]; tempMatrix[13] = myMatrix[13]; tempMatrix[14] = myMatrix[14]; tempMatrix[15] = myMatrix[15];

		myMatrix[0] = (tempMatrix[0] * aMatrix[0]) + (tempMatrix[1] * aMatrix[4]) + (tempMatrix[2] * aMatrix[8]) + (tempMatrix[3] * aMatrix[12]);
		myMatrix[1] = (tempMatrix[0] * aMatrix[1]) + (tempMatrix[1] * aMatrix[5]) + (tempMatrix[2] * aMatrix[9]) + (tempMatrix[3] * aMatrix[13]);
		myMatrix[2] = (tempMatrix[0] * aMatrix[2]) + (tempMatrix[1] * aMatrix[6]) + (tempMatrix[2] * aMatrix[10]) + (tempMatrix[3] * aMatrix[14]);
		myMatrix[3] = (tempMatrix[0] * aMatrix[3]) + (tempMatrix[1] * aMatrix[7]) + (tempMatrix[2] * aMatrix[11]) + (tempMatrix[3] * aMatrix[15]);

		myMatrix[4] = (tempMatrix[4] * aMatrix[0]) + (tempMatrix[5] * aMatrix[4]) + (tempMatrix[6] * aMatrix[8]) + (tempMatrix[7] * aMatrix[12]);
		myMatrix[5] = (tempMatrix[4] * aMatrix[1]) + (tempMatrix[5] * aMatrix[5]) + (tempMatrix[6] * aMatrix[9]) + (tempMatrix[7] * aMatrix[13]);
		myMatrix[6] = (tempMatrix[4] * aMatrix[2]) + (tempMatrix[5] * aMatrix[6]) + (tempMatrix[6] * aMatrix[10]) + (tempMatrix[7] * aMatrix[14]);
		myMatrix[7] = (tempMatrix[4] * aMatrix[3]) + (tempMatrix[5] * aMatrix[7]) + (tempMatrix[6] * aMatrix[11]) + (tempMatrix[7] * aMatrix[15]);

		myMatrix[8] = (tempMatrix[8] * aMatrix[0]) + (tempMatrix[9] * aMatrix[4]) + (tempMatrix[10] * aMatrix[8]) + (tempMatrix[11] * aMatrix[12]);
		myMatrix[9] = (tempMatrix[8] * aMatrix[1]) + (tempMatrix[9] * aMatrix[5]) + (tempMatrix[10] * aMatrix[9]) + (tempMatrix[11] * aMatrix[13]);
		myMatrix[10] = (tempMatrix[8] * aMatrix[2]) + (tempMatrix[9] * aMatrix[6]) + (tempMatrix[10] * aMatrix[10]) + (tempMatrix[11] * aMatrix[14]);
		myMatrix[11] = (tempMatrix[8] * aMatrix[3]) + (tempMatrix[9] * aMatrix[7]) + (tempMatrix[10] * aMatrix[11]) + (tempMatrix[11] * aMatrix[15]);

		myMatrix[12] = (tempMatrix[12] * aMatrix[0]) + (tempMatrix[13] * aMatrix[4]) + (tempMatrix[14] * aMatrix[8]) + (tempMatrix[15] * aMatrix[12]);
		myMatrix[13] = (tempMatrix[12] * aMatrix[1]) + (tempMatrix[13] * aMatrix[5]) + (tempMatrix[14] * aMatrix[9]) + (tempMatrix[15] * aMatrix[13]);
		myMatrix[14] = (tempMatrix[12] * aMatrix[2]) + (tempMatrix[13] * aMatrix[6]) + (tempMatrix[14] * aMatrix[10]) + (tempMatrix[15] * aMatrix[14]);
		myMatrix[15] = (tempMatrix[12] * aMatrix[3]) + (tempMatrix[13] * aMatrix[7]) + (tempMatrix[14] * aMatrix[11]) + (tempMatrix[15] * aMatrix[15]);
		return *this;
	}

	template<typename ObjectType>
	inline bool Matrix44<ObjectType>::operator==(const Matrix44 & aMatrix) const
	{
		for (int i = 0; i < MATRIX44_SIZE; i++)
		{
			if (myMatrix[i] != aMatrix.myMatrix[i])
			{
				return false;
			}
		}

		return true;
	}

	template<typename ObjectType>
	inline Matrix44<ObjectType>& Matrix44<ObjectType>::operator=(const Matrix44 & aMatrix)
	{
		for (int i = 0; i < MATRIX44_SIZE; i++)
		{
			myMatrix[i] = aMatrix.myMatrix[i];
		}
		return *(this);
	}

	template<typename ObjectType>
	inline ObjectType& Matrix44<ObjectType>::operator[](const int aIndex)
	{
		assert(aIndex >= 0 && aIndex < 16 && "Index out of bounds");
		return myMatrix[aIndex];
	}

	template<typename ObjectType>
	inline const ObjectType& Matrix44<ObjectType>::operator[](const int aIndex) const
	{
		assert(aIndex >= 0 && aIndex < 16 && "Index out of bounds");
		return myMatrix[aIndex];
	}

	template<typename ObjectType>
	inline Vector4<ObjectType> Matrix44<ObjectType>::operator*(const Vector4<ObjectType>& aVector4)
	{
		Vector4<ObjectType> newVector;

		Vector4<ObjectType> column1 = { myMatrix[0], myMatrix[4], myMatrix[8], myMatrix[12] };
		Vector4<ObjectType> column2 = { myMatrix[1], myMatrix[5], myMatrix[9], myMatrix[13] };
		Vector4<ObjectType> column3 = { myMatrix[2], myMatrix[6], myMatrix[10], myMatrix[14] };
		Vector4<ObjectType> column4 = { myMatrix[3], myMatrix[7], myMatrix[11], myMatrix[15] };

		newVector.x = aVector4.x * column1.x + aVector4.y * column1.y + aVector4.z * column1.z + aVector4.w * column1.w;
		newVector.y = aVector4.x * column2.x + aVector4.y * column2.y + aVector4.z * column2.z + aVector4.w * column2.w;
		newVector.z = aVector4.x * column3.x + aVector4.y * column3.y + aVector4.z * column3.z + aVector4.w * column3.w;
		newVector.w = aVector4.x * column4.x + aVector4.y * column4.y + aVector4.z * column4.z + aVector4.w * column4.w;

		return newVector;
	}

	template<class ObjectType>
	inline Vector4<ObjectType> operator*(const Vector4<ObjectType>& aVector4, const Matrix44<ObjectType>& aMatrix44)
	{
		Vector4<ObjectType> newVector;

		Vector4<ObjectType> column1 = { aMatrix44.myMatrix[0], aMatrix44.myMatrix[4], aMatrix44.myMatrix[8], aMatrix44.myMatrix[12] };
		Vector4<ObjectType> column2 = { aMatrix44.myMatrix[1], aMatrix44.myMatrix[5], aMatrix44.myMatrix[9], aMatrix44.myMatrix[13] };
		Vector4<ObjectType> column3 = { aMatrix44.myMatrix[2], aMatrix44.myMatrix[6], aMatrix44.myMatrix[10], aMatrix44.myMatrix[14] };
		Vector4<ObjectType> column4 = { aMatrix44.myMatrix[3], aMatrix44.myMatrix[7], aMatrix44.myMatrix[11], aMatrix44.myMatrix[15] };

		newVector.x = aVector4.x * column1.x + aVector4.y * column1.y + aVector4.z * column1.z + aVector4.w * column1.w;
		newVector.y = aVector4.x * column2.x + aVector4.y * column2.y + aVector4.z * column2.z + aVector4.w * column2.w;
		newVector.z = aVector4.x * column3.x + aVector4.y * column3.y + aVector4.z * column3.z + aVector4.w * column3.w;
		newVector.w = aVector4.x * column4.x + aVector4.y * column4.y + aVector4.z * column4.z + aVector4.w * column4.w;

		return newVector;
	}

	template<class ObjectType>
	inline Matrix44<ObjectType> Matrix44<ObjectType>::CreateTranslation(const Vector4<ObjectType>& aPoint)
	{
		return Matrix44<ObjectType>(
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			aPoint.x, aPoint.y, aPoint.z, aPoint.w
			);
	}

	template<class ObjectType>
	inline void Matrix44<ObjectType>::SetTranslation(const Vector4<ObjectType>& aPoint)
	{
		myMatrix[12] = aPoint.x;
		myMatrix[13] = aPoint.y;
		myMatrix[14] = aPoint.z;
		myMatrix[15] = aPoint.w;
	}


	template<class ObjectType>
	inline Vector3<ObjectType> Matrix44<ObjectType>::GetPosition() const
	{
		return Vector3<ObjectType>(myMatrix[12], myMatrix[13], myMatrix[14]);
	}

	template<typename ObjectType>
	inline void Matrix44<ObjectType>::SetPosition(const Vector3<ObjectType>& aPosition)
	{
		myMatrix[12] = aPosition.x;
		myMatrix[13] = aPosition.y;
		myMatrix[14] = aPosition.z;
	}

	template<class ObjectType>
	inline Vector4<ObjectType> Matrix44<ObjectType>::GetTranslation() const
	{
		return Vector4<ObjectType>(myMatrix[12], myMatrix[13], myMatrix[14], myMatrix[15]);
	}

	template<class ObjectType>
	inline Vector3<ObjectType> Matrix44<ObjectType>::GetScale() const
	{
		Vector3<ObjectType> temp;
		temp.x = myMatrix[0];
		temp.y = myMatrix[5];
		temp.z = myMatrix[10];

		return temp;
	}

	

	template<class ObjectType>
	inline Matrix44<ObjectType> Matrix44<ObjectType>::GetFastInverse() const
	{
		return Matrix44<ObjectType>(

			myMatrix[0], myMatrix[4], myMatrix[8], myMatrix[3],
			myMatrix[1], myMatrix[5], myMatrix[9], myMatrix[7],
			myMatrix[2], myMatrix[6], myMatrix[10], myMatrix[11],

			(-myMatrix[12] * myMatrix[0]) + (-myMatrix[13] * myMatrix[1]) + (-myMatrix[14] * myMatrix[2]),
			(-myMatrix[12] * myMatrix[4]) + (-myMatrix[13] * myMatrix[5]) + (-myMatrix[14] * myMatrix[6]),
			(-myMatrix[12] * myMatrix[8]) + (-myMatrix[13] * myMatrix[9]) + (-myMatrix[14] * myMatrix[10]),

			myMatrix[15]
			);
	}

	typedef Matrix44<float> Matrix44f; 

	template <typename ObjectType>
		const Matrix44<ObjectType> Matrix44<ObjectType>::Zero(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

	template <typename ObjectType>
	const Matrix44<ObjectType> Matrix44<ObjectType>::Identity;
}