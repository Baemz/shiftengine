#include "stdafx.h"
#include "Quaternion.h"

using namespace Shift3D;

Quaternion::Quaternion(float aRotationX, float aRotationY, float aRotationZ, float aRotationAmount, bool aNormalize) 
	: myRotation(aRotationX, aRotationY, aRotationZ), myRotationAmount(aRotationAmount)
{
	if (aNormalize == true)
	{
		Normalize();
	}
}

Quaternion::Quaternion(const Vector3f &aRotation, float aRotationAmount /*= 1.0f*/, bool aNormalize)
	:Quaternion(aRotation.x, aRotation.y, aRotation.z, aRotationAmount, aNormalize)
{

}

Quaternion::Quaternion(const Vector4f & aRotationAndAmount /*= Vector4f(0.0f, 0.0f, 0.0f, 1.0f)*/, bool aNormalize)
	:Quaternion(aRotationAndAmount.x, aRotationAndAmount.y, aRotationAndAmount.z, aRotationAndAmount.w, aNormalize)
{

}

Quaternion::Quaternion(Matrix44f pm)
{
	float wsqrdminus1 = pm[0] + pm[5] + pm[10];
	float xsqrdminus1 = pm[0] - pm[5] - pm[10];
	float ysqrdminus1 = pm[5] - pm[0] - pm[10];
	float zsqrdminus1 = pm[10] - pm[0] - pm[5];
	int biggestindex = 0;
	float biggest = wsqrdminus1;
	if (xsqrdminus1 > biggest) {
		biggest = xsqrdminus1;
		biggestindex = 1;
	}
	if (ysqrdminus1 > biggest) {
		biggest = ysqrdminus1;
		biggestindex = 2;
	}
	if (zsqrdminus1 > biggest) {
		biggest = zsqrdminus1;
		biggestindex = 3;
	}
	float biggestval = sqrtf(biggest + 1.0f)*.5f;
	float mult = .25f / biggestval;
	switch (biggestindex) {
	case 0:
		myRotationAmount = biggestval;
		myRotation.x = (pm[6] - pm[9]) * mult;
		myRotation.y = (pm[8] - pm[2]) * mult;
		myRotation.z = (pm[1] - pm[4]) * mult;
		break;
	case 1:
		myRotation.x = biggestval;
		myRotationAmount = (pm[6] - pm[9]) * mult;
		myRotation.y = (pm[1] + pm[4]) * mult;
		myRotation.z = (pm[8] + pm[2]) * mult;
		break;
	case 2:
		myRotation.y = biggestval;
		myRotationAmount = (pm[8] - pm[2]) * mult;
		myRotation.x = (pm[1] + pm[4]) * mult;
		myRotation.z = (pm[6] + pm[9]) * mult;
		break;
	case 3:
		myRotation.z = biggestval;
		myRotationAmount = (pm[2] - pm[4]) * mult;
		myRotation.x = (pm[8] + pm[2]) * mult;
		myRotation.y = (pm[6] + pm[9]) * mult;
		break;
	};
}

void Quaternion::RotateTowardsPoint(const Vector3f & aCurrentPosition, const Vector3f & aTargetPosition)
{
	bool angleIsPositive = false;
	const Vector3f CurrentDirection = GetForward().GetNormalized();
	const float sign = GetRight().GetNormalized().Dot(aTargetPosition - aCurrentPosition); //Sign is positive to the right, negative to the left
	Vector3f rotAxis;

	if (sign > 0.0001f) // right
	{
		rotAxis = Vector3f::Cross(CurrentDirection, aTargetPosition - aCurrentPosition);
		angleIsPositive = true;
	}
	else if (sign < 0.0001f) //left
	{
		rotAxis = Vector3f::Cross(aTargetPosition - aCurrentPosition, CurrentDirection);
		angleIsPositive = false;
	}
	else
	{
		return;
	}

	if (isnan(rotAxis.z) == true || isnan(rotAxis.y) == true || isnan(rotAxis.x) == true || rotAxis == Vector3f::Zero)
	{
		return;
	}

	float angle = Vector3f::GetAngleFromCross(rotAxis);

	if (isnan(angle) == true || angle == 0.f)
	{
		return;
	}
	  
	if (angleIsPositive == false)
	{
		angle *= -1.f;
	}

	RotateAlongAxis(rotAxis, angle);
}

void Quaternion::Normalize()
{
	float magnitude = sqrtf(powf(myRotation.x, 2) + powf(myRotation.y, 2) + powf(myRotation.z, 2) + powf(myRotationAmount, 2));

	myRotation.x /= magnitude;
	myRotation.y /= magnitude;
	myRotation.z /= magnitude;
	myRotationAmount /= magnitude;
}



void Quaternion::RotateAlongAxis(const Vector3f &aAxis, float aRotationAmount)
{
	Quaternion localRotation;
	localRotation.myRotation.x = aAxis.x * sinf(aRotationAmount / 2.f);
	localRotation.myRotation.y = aAxis.y * sinf(aRotationAmount / 2.f);
	localRotation.myRotation.z = aAxis.z * sinf(aRotationAmount / 2.f);
	localRotation.myRotationAmount = cosf(aRotationAmount / 2.f);

	*this = *this * localRotation;
}

void Quaternion::RotateWorld(const Vector3f& aRotationAmount)
{
	RotateWorldX(aRotationAmount.x);
	RotateWorldY(aRotationAmount.y);
	RotateWorldZ(aRotationAmount.z);
}

void Quaternion::RotateWorldX(float aRotationAmount)
{
	RotateAlongAxis(Vector3f(1.0f, 0.0f, 0.0f), aRotationAmount);
}

void Quaternion::RotateWorldY(float aRotationAmount)
{
	RotateAlongAxis(Vector3f(0.0f, 1.0f, 0.0f), aRotationAmount);
}

void Quaternion::RotateWorldZ(float aRotationAmount)
{
	RotateAlongAxis(Vector3f(0.0f, 0.0f, 1.0f), aRotationAmount);
}

Quaternion Quaternion::CreateFromAxisAngle(const Vector3f &aAxis, const float anAngle)
{
	float halfAngle = anAngle * .5f;
	float s = sinf(halfAngle);
	Quaternion q;
	q.myRotation.x = aAxis.x * s;
	q.myRotation.y = aAxis.y * s;
	q.myRotation.z = aAxis.z * s;
	q.myRotationAmount = cosf(halfAngle);
	return q;
}

void Quaternion::RotateLocal(const Vector3f& aRotationAmount)
{
	RotateLocalX(aRotationAmount.x);
	RotateLocalY(aRotationAmount.y);
	RotateLocalZ(aRotationAmount.z);
}

void Quaternion::RotateLocalX(float aRotationAmount)
{
	RotateAlongAxis(*this * GetRight(), aRotationAmount);
}

void Quaternion::RotateLocalY(float aRotationAmount)
{
	RotateAlongAxis(*this * GetUp(), aRotationAmount);
}

void Quaternion::RotateLocalZ(float aRotationAmount)
{
	RotateAlongAxis(*this * GetForward(), aRotationAmount);
}


Vector3f Quaternion::ToEuler()  const
{
	float sp = -2.0f * (myRotation.y * myRotation.z - myRotationAmount*myRotation.x);// Extract sinf(pitch)

	float pitch, roll, yaw;

	if (fabs(sp) > 0.9999f) {// Check for Gimbel lock, giving slight tolerance for numerical imprecision
		pitch = (3.141592f / 2.f) * sp;// Looking straight up or down
		yaw = atan2(-myRotation.x*myRotation.z + myRotationAmount*myRotation.y, 0.5f - myRotation.y*myRotation.y - myRotation.z*myRotation.z);// Compute heading, slam roll to zero
		roll = 0.0f;
	}
	else {// Compute angles.  We don't have to use the "safe" asin function because we already checked for range errors when checking for Gimbel lock
		pitch = asinf(sp);
		yaw = atan2(myRotation.x*myRotation.z + myRotationAmount * myRotation.y, 0.5f - myRotation.x*myRotation.x - myRotation.y*myRotation.y);
		roll = atan2(myRotation.x*myRotation.y + myRotationAmount*myRotation.z, 0.5f - myRotation.x*myRotation.x - myRotation.z*myRotation.z);
	}

	return Vector3f(pitch, roll, yaw);
}

Vector3f Quaternion::GetRight() const
{
	return *this * Vector3f(1.0f, 0.0f, 0.0f);
}

Vector3f Quaternion::GetLeft() const
{
	return -GetRight();
}

Vector3f Quaternion::operator*(const Vector3f& aRight) const
{
	Vector3f quaternionAsVector(myRotation.x, myRotation.y, myRotation.z);
	float scalar = myRotationAmount;

	Vector3f firstTest = quaternionAsVector * 2.0f * Vector3f::Dot(quaternionAsVector, aRight);
	Vector3f secondTest = aRight * (scalar*scalar - Vector3f::Dot(quaternionAsVector, quaternionAsVector));
	Vector3f thirdTest = Vector3f::Cross(quaternionAsVector, aRight) * scalar * 2.0f;

	return Vector3f(
		firstTest
		+ secondTest
		+ thirdTest);
}

Vector3f Quaternion::GetUp() const
{
	return *this * Vector3f(0.0f, 1.0f, 0.0f);
}

Vector3f Quaternion::GetDown() const
{
	return -GetUp();
}

Vector3f Quaternion::GetForward() const
{
	return *this * Vector3f(0.0f, 0.0f, 1.0f);
}

Vector3f Quaternion::GetBackward() const
{
	return -GetForward();
}

//	Roll = Z Rotation
float Quaternion::GetRoll() const
{
	return ToEuler().y;
	//return atan2f(2 * myRotation.y * myRotationAmount - 2 * myRotation.x * myRotation.z, 1 - 2 * myRotation.y * myRotation.y - 2 * myRotation.z * myRotation.z);
}

//	Pitch = X Rotation
float Quaternion::GetPitch() const
{
	return ToEuler().x;
}

//	Yaw = Y Rotation
float Quaternion::GetYaw() const
{
	return ToEuler().z;
}

Quaternion Quaternion::LookAt(const Vector3f &aStartPosition, const Vector3f &aEndPosition, const Vector3f &aUp)
{
  	Vector3f forwardVector = Vector3f::Normalize(aEndPosition - aStartPosition);

	float dot = Vector3f::Dot(Vector3f(0.0f, 0.0f, 1.0f), forwardVector);

	if (std::abs(dot - (-1.0f)) < 0.000001f)
	{
		return Quaternion(aUp, 3.1415926535897932f);
	}
	if (std::abs(dot - (1.0f)) < 0.000001f)
	{
		return Quaternion();
	}

	float rotAngle = (float)std::acosf(dot);
	Vector3f rotAxis = Vector3f::Cross(Vector3f(0.0f, 0.0f, 1.0f), forwardVector);
	rotAxis = Vector3f::Normalize(rotAxis);

	Quaternion quaternion;
	quaternion.RotateAlongAxis(rotAxis, rotAngle);
	return CreateFromAxisAngle(rotAxis, rotAngle);
}

bool Quaternion::operator==(const Quaternion& aRight) const
{
	return aRight.myRotation == myRotation && aRight.myRotationAmount == myRotationAmount;
}

bool Quaternion::operator!=(const Quaternion& aRight) const
{
	return !(aRight == *this);
}

Quaternion Quaternion::operator*(const Quaternion& aRight) const
{
	Quaternion returnValue;
	returnValue.myRotation.x = myRotationAmount * aRight.myRotation.x + myRotation.x * aRight.myRotationAmount + myRotation.y * aRight.myRotation.z - myRotation.z * aRight.myRotation.y;

	returnValue.myRotation.y = myRotationAmount * aRight.myRotation.y - myRotation.x * aRight.myRotation.z + myRotation.y * aRight.myRotationAmount + myRotation.z * aRight.myRotation.x;

	returnValue.myRotation.z = myRotationAmount * aRight.myRotation.z + myRotation.x * aRight.myRotation.y - myRotation.y * aRight.myRotation.x + myRotation.z * aRight.myRotationAmount;

	returnValue.myRotationAmount = myRotationAmount * aRight.myRotationAmount - myRotation.x * aRight.myRotation.x - myRotation.y * aRight.myRotation.y - myRotation.z * aRight.myRotation.z;

	return returnValue;
}

Quaternion& Quaternion::operator*=(const Quaternion& aRight)
{
	*this = *this * aRight;
	return *this;
}

Quaternion Quaternion::operator/(const float aScale) const
{
	return Quaternion(myRotation / aScale, myRotationAmount / aScale);
}

Quaternion Quaternion::operator*(const float aScale) const
{
	return Quaternion(myRotation * aScale, myRotationAmount * aScale, false);
}

Quaternion Quaternion::operator+(const Quaternion& aRight) const
{
	return Quaternion(myRotation + aRight.myRotation, myRotationAmount + aRight.myRotationAmount);
}

Quaternion Quaternion::operator-(const Quaternion& aRight) const
{
	return Quaternion(myRotation - aRight.myRotation, myRotationAmount - aRight.myRotationAmount);
}

Quaternion& Quaternion::operator*=(const float aScale)
{
	*this = *this * aScale;
	return *this;
}

Quaternion& Quaternion::operator+=(const Quaternion& aRight)
{
	*this = *this + aRight;
	return *this;
}

Quaternion& Quaternion::operator-=(const Quaternion& aRight)
{
	*this = *this - aRight;
	return *this;
}

Quaternion& Quaternion::operator/=(const float aScale)
{
	*this = *this / aScale;
	return *this;
}

Matrix44f Quaternion::GenerateMatrix() const
{
	float X = myRotation.x;
	float Y = myRotation.y;
	float Z = myRotation.z;
	float W = myRotationAmount;
	float xx = X * X;
	float xy = X * Y;
	float xz = X * Z;
	float xw = X * W;
	float yy = Y * Y;
	float yz = Y * Z;
	float yw = Y * W;
	float zz = Z * Z;
	float zw = Z * W;

	Matrix44f returnValue;
	returnValue[0] = 1 - 2 * (yy + zz);
	returnValue[1] = 2 * (xy - zw);
	returnValue[2] = 2 * (xz + yw);

	returnValue[4] = 2 * (xy + zw);
	returnValue[5] = 1 - 2 * (xx + zz);
	returnValue[6] = 2 * (yz - xw);

	returnValue[8] = 2 * (xz - yw);
	returnValue[9] = 2 * (yz + xw);
	returnValue[10] = 1 - 2 * (xx + yy);

	returnValue[3] = returnValue[7] = returnValue[11] = returnValue[12] = returnValue[13] = returnValue[14] = 0;
	returnValue[15] = 1;

	returnValue= Matrix44f::Transpose(returnValue);
	return returnValue;
}

Quaternion Quaternion::GetInverse(const Quaternion &aQuaternion)
{
	Quaternion temp;
	temp.myRotationAmount = aQuaternion.myRotationAmount;
	temp.myRotation = aQuaternion.myRotation * -1.f;

	return temp;
}

float Quaternion::Dot(const Quaternion &aFirstQuaternion, const Quaternion &aSecondQuaternion)
{
	return aFirstQuaternion.myRotation.Dot(aSecondQuaternion.myRotation) + aFirstQuaternion.myRotationAmount * aSecondQuaternion.myRotationAmount;
}

Quaternion Quaternion::Lerp(const Quaternion &aFirstQuaternion, const Quaternion &aSecondQuaternion, const float aProgress)
{
	return Quaternion(aFirstQuaternion * (1.f - aProgress) + aSecondQuaternion * aProgress);
}

Quaternion Quaternion::Slerp(const Quaternion &aFirstQuaternion, const Quaternion &aSecondQuaternion, const float aProgress)
{
 	Quaternion first = aFirstQuaternion;
	Quaternion second = aSecondQuaternion;
	float angle = Quaternion::Dot(first, second);
	Quaternion quaternion;
	// make sure we use the short rotation
	if (angle < 0.0f)
	{
		first = first * -1.0f;
		angle *= -1.0f;
	}

	if (angle <= (1 - 0.05f)) // spherical interpolation
	{
		const float theta = acosf(angle);
		const float invsintheta = 1.0f / (sinf(theta));
		const float scale = sinf(theta * (1.0f - aProgress)) * invsintheta;
		const float invscale = sinf(theta * aProgress) * invsintheta;
		Quaternion firstMul = first * scale;
		Quaternion secondMul = second * invscale;
		Quaternion res = firstMul + secondMul;
		return res;
	}
	return Lerp(first, second, aProgress);
		
}
