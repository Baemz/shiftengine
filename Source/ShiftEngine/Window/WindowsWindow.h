#pragma once
#include <windows.h>
#include <functional>

namespace Shift3D
{
	using callback_function_wndProc = std::function<LRESULT(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)>;

	struct EngineCreateParameters;
	class WindowsWindow
	{
	public:
		WindowsWindow();
		~WindowsWindow();
		bool Init(Vector2ui aWindowSize, HWND*& aHwnd, EngineCreateParameters* aSetting, HINSTANCE& aHInstanceToFill, callback_function_wndProc aWndPrcCallback);
		HWND GetWindowHandle() const { return myWindowHandle; }
		void SetResolution(Vector2ui aResolution);
		void Close();

	private:
		LRESULT LocWindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
		static LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
		HWND myWindowHandle;
		WNDCLASSEX myWindowClass;
		callback_function_wndProc myWndProcCallback;
		Vector2ui myResolution;
	};
}