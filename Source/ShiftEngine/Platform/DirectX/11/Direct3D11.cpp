#include "stdafx.h"
#include "Direct3D11.h"
#include <d3d11.h>
#include <dxgi1_4.h>
#include "Math/Vector2.h"
#include "Engine.h"

#pragma comment(lib,"d3d11.lib")
#pragma comment(lib,"dxgi.lib")

using namespace Shift3D;


#define NUM_SUPPORTED_FEATURE_LEVELS 4
const D3D_FEATURE_LEVEL supportedDXVersions[NUM_SUPPORTED_FEATURE_LEVELS] = {
	D3D_FEATURE_LEVEL_11_1,
	D3D_FEATURE_LEVEL_11_0,
	D3D_FEATURE_LEVEL_10_1,
	D3D_FEATURE_LEVEL_10_0
};


CDirect3D11::CDirect3D11()
	: CGraphicsInterface()
	, mySwapChain(nullptr)
	, myDevice(nullptr)
	, myContext(nullptr)
	, myRenderTargetView(nullptr)
	, myVideoCardMemory(0)
	, myDepthStencilView(nullptr)
	, mySamplerState(nullptr)
{
}

CDirect3D11::~CDirect3D11()
{
	mySwapChain->SetFullscreenState(FALSE, nullptr);
	myContext->ClearState();
	myContext->Flush();
	SAFE_RELEASE(mySwapChain);
	SAFE_RELEASE(myDevice);
	SAFE_RELEASE(myContext);
	SAFE_RELEASE(myRenderTargetView);
	SAFE_RELEASE(myDepthStencilBuffer);
	SAFE_RELEASE(myDepthStencilView);
	SAFE_RELEASE(myDepthStencilState);
	SAFE_RELEASE(myRasterizerState);
	SAFE_RELEASE(mySamplerState);
}

bool Shift3D::CDirect3D11::Init(const CEngine& aEngine, Vector2ui aWindowSize, bool aEnableVSync, bool aStartInFullScreen)
{
	INFO_PRINT("%s", "Initiating DirectX 11...");
	HRESULT result;
	myWindowSize = aWindowSize;
	myEnableVSync = aEnableVSync;
	ourInterface = this;
	ourGFXAPI = EGraphicsAPI::eDirectX11;

	DXGI_MODE_DESC bufferDesc;
	ZeroMemory(&bufferDesc, sizeof(DXGI_MODE_DESC));

	bufferDesc.Width = myWindowSize.x;
	bufferDesc.Height = myWindowSize.y;

	Vector2<int> numDenum;
	IDXGIAdapter* adapter = nullptr;
	if (CollectAdapters(myWindowSize, numDenum, adapter))
	{
		INFO_PRINT("%s%s", "	- VSYNC Compatible: Yes, Enabled: ", myEnableVSync ? "Yes" : "No");
	}

	bufferDesc.RefreshRate.Numerator = numDenum.x;
	bufferDesc.RefreshRate.Denominator = numDenum.y;
	bufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	bufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	bufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	ZeroMemory(&swapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));

	swapChainDesc.BufferDesc = bufferDesc;
	swapChainDesc.SampleDesc.Count = 4;
	swapChainDesc.SampleDesc.Quality = 0;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.BufferCount = 2;
	swapChainDesc.OutputWindow = aEngine.GetHWND();
	swapChainDesc.Windowed = !aStartInFullScreen;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;


	INFO_PRINT("%s", "	Creating Device & Swapchain.");
	result = D3D11CreateDeviceAndSwapChain(nullptr, 
		D3D_DRIVER_TYPE_HARDWARE, 
		nullptr, 
		D3D11_CREATE_DEVICE_DEBUG & D3D11_CREATE_DEVICE_DEBUGGABLE,
		supportedDXVersions,
		NUM_SUPPORTED_FEATURE_LEVELS,
		D3D11_SDK_VERSION, 
		&swapChainDesc, 
		&mySwapChain, 
		&myDevice, 
		nullptr,
		&myContext);
	if (FAILED(result))
	{
		ERROR_PRINT("%s", "Could not create device & swapchain, exiting...");
		return false;
	}

	INFO_PRINT("%s", "	Setting Backbuffer.");
	ID3D11Texture2D* backBuffer;
	result = mySwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&backBuffer);
	if (FAILED(result))
	{
		ERROR_PRINT("%s", "Could not create backbuffer, exiting...");
		return false;
	}

	INFO_PRINT("%s", "	Creating RenderTargetView.");
	result = myDevice->CreateRenderTargetView(backBuffer, nullptr, &myRenderTargetView);
	if (FAILED(result))
	{
		ERROR_PRINT("%s", "Could not create rendertargetview, exiting...");
		return false;
	}
	backBuffer->Release();

	CreateDepthStencil();
	CreateSamplerState();
	CreateViewport();

	myPostProcessRenderer.Init();

	INFO_PRINT("%s", "DirectX 11 successfully initiated.");
	return true;
}

void Shift3D::CDirect3D11::EndRenderFrame()
{
	myPostProcessRenderer.DoHDR();
	if (myEnableVSync)
	{
		mySwapChain->Present(1, 0);
	}
	else
	{
		mySwapChain->Present(0, 0);
	}
}

void Shift3D::CDirect3D11::BeginRenderFrame()
{
	float clearColor[] = { 0.150f, 0.150f, 0.150f, 1.f };
	myContext->ClearRenderTargetView(myRenderTargetView, clearColor);
	myContext->ClearDepthStencilView(myDepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
}

void Shift3D::CDirect3D11::CreateDepthStencil()
{
	HRESULT result;
	D3D11_TEXTURE2D_DESC depthTextureDesc;
	depthTextureDesc.Width = myWindowSize.x;
	depthTextureDesc.Height = myWindowSize.y;
	depthTextureDesc.MipLevels = 1;
	depthTextureDesc.ArraySize = 1;
	depthTextureDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthTextureDesc.SampleDesc.Count = 4;
	depthTextureDesc.SampleDesc.Quality = 0;
	depthTextureDesc.Usage = D3D11_USAGE_DEFAULT;
	depthTextureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthTextureDesc.CPUAccessFlags = NULL;
	depthTextureDesc.MiscFlags = NULL;

	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
	depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
	depthStencilViewDesc.Texture2D.MipSlice = 0;

	INFO_PRINT("%s", "	Creating DepthStencil.");
	result = myDevice->CreateTexture2D(&depthTextureDesc, nullptr, &myDepthStencilBuffer);
	result = myDevice->CreateDepthStencilView(myDepthStencilBuffer, &depthStencilViewDesc, &myDepthStencilView);
	myContext->OMSetRenderTargets(1, &myRenderTargetView, myDepthStencilView);

	CreateRenderStates();
	if (FAILED(result))
	{
		ERROR_PRINT("%s", "Could not create depth stencil, exiting...");
		return;
	}

}

void Shift3D::CDirect3D11::CreateSamplerState()
{
	D3D11_SAMPLER_DESC samplerDesc;
	samplerDesc.Filter = D3D11_FILTER_ANISOTROPIC;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 16;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	myDevice->CreateSamplerState(&samplerDesc, &mySamplerState);
	myContext->PSSetSamplers(0, 1, &mySamplerState);
}

void Shift3D::CDirect3D11::CreateViewport()
{
	D3D11_VIEWPORT viewport;
	ZeroMemory(&viewport, sizeof(D3D11_VIEWPORT));

	viewport.TopLeftX = 0;
	viewport.TopLeftY = 0;
	viewport.Width = static_cast<FLOAT>(myWindowSize.x);
	viewport.Height = static_cast<FLOAT>(myWindowSize.y);
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;

	myContext->RSSetViewports(1, &viewport);
}

void Shift3D::CDirect3D11::CreateRenderStates()
{
	D3D11_RASTERIZER_DESC rasterDesc;
	ZeroMemory(&rasterDesc, sizeof(rasterDesc));

	rasterDesc.AntialiasedLineEnable = true;
	rasterDesc.CullMode = D3D11_CULL_BACK;
	rasterDesc.DepthBias = 0;
	rasterDesc.DepthBiasClamp = 0.0f;
	rasterDesc.DepthClipEnable = true;
	rasterDesc.FillMode = D3D11_FILL_SOLID;
	rasterDesc.FrontCounterClockwise = false;
	rasterDesc.MultisampleEnable = true;
	rasterDesc.ScissorEnable = false;
	rasterDesc.SlopeScaledDepthBias = 0.0f;

	myDevice->CreateRasterizerState(&rasterDesc, &myRasterizerState);

	myContext->RSSetState(myRasterizerState);
}

bool Shift3D::CDirect3D11::CollectAdapters(Vector2ui aWindowSize, Vector2i& aNumDenumerator, IDXGIAdapter*& outAdapter)
{
	HRESULT result = S_OK;
	IDXGIFactory4* factory;

	DXGI_MODE_DESC* displayModeList = nullptr;
	unsigned numModes = 0;
	unsigned denominator = 0;
	unsigned numerator = 0;
	result = CreateDXGIFactory1(IID_PPV_ARGS(&factory));
	if (FAILED(result))
	{
		ERROR_PRINT("%s", "Could not create dxFactory, exiting...");
		return false;
	}

	IDXGIAdapter* usingAdapter = nullptr;
	int adapterIndex = 0;
	std::vector<DXGI_ADAPTER_DESC> myAdapterDescs;
	std::vector<IDXGIAdapter*> myAdapters;
	while (factory->EnumAdapters(adapterIndex, &usingAdapter) != DXGI_ERROR_NOT_FOUND)
	{
		DXGI_ADAPTER_DESC adapterDesc;
		usingAdapter->GetDesc(&adapterDesc);
		myAdapterDescs.push_back(adapterDesc);
		myAdapters.push_back(usingAdapter);
		++adapterIndex;
	}

	if (adapterIndex == 0)
	{
		return false;
	}

	INFO_PRINT("%s", "Video card(s) detected: ");
	for (DXGI_ADAPTER_DESC desc : myAdapterDescs)
	{
		int memory = static_cast<int>(desc.DedicatedVideoMemory / 1024 / 1024);
		INFO_PRINT("	- %ls%s%i%s", desc.Description, " Mem: ", memory, "Mb");
		memory;
	}

	INFO_PRINT("%s", "	Detecting best card...");

	DXGI_ADAPTER_DESC usingAdapterDesc = myAdapterDescs[0];
	usingAdapter = myAdapters[0];

	const std::wstring nvidia = L"NVIDIA";
	const std::wstring amd = L"AMD";

	int memory = static_cast<int>(usingAdapterDesc.DedicatedVideoMemory / 1024 / 1024);
	int mostMem = 0;

	for (unsigned int i = 0; i < myAdapterDescs.size(); i++)
	{
		DXGI_ADAPTER_DESC desc = myAdapterDescs[i];
		memory = static_cast<int>(desc.DedicatedVideoMemory / 1024 / 1024);
		std::wstring name = desc.Description;
		if (name.find(nvidia) != std::wstring::npos || name.find(amd) != std::wstring::npos)
		{
			if (memory > mostMem)
			{
				mostMem = memory;
				usingAdapterDesc = desc;
				usingAdapter = myAdapters[i];
			}
		}
	}
	INFO_PRINT("%s%ls%s%i%s", "	- Using graphic card: ", usingAdapterDesc.Description, " Dedicated Mem: ", mostMem, "Mb");

	IDXGIOutput* pOutput = nullptr;
	if (usingAdapter->EnumOutputs(0, &pOutput) != DXGI_ERROR_NOT_FOUND)
	{
		// Get the number of modes that fit the DXGI_FORMAT_R8G8B8A8_UNORM display format for the adapter output (monitor).
		result = pOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, NULL);
		if (!FAILED(result))
		{
			// Create a list to hold all the possible display modes for this monitor/video card combination.
			displayModeList = new DXGI_MODE_DESC[numModes];
			if (displayModeList)
			{
				// Now fill the display mode list structures.
				result = pOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, displayModeList);
				if (!FAILED(result))
				{
					// Now go through all the display modes and find the one that matches the screen width and height.
					// When a match is found store the numerator and denominator of the refresh rate for that monitor.
					for (unsigned int i = 0; i < numModes; i++)
					{
						if (displayModeList[i].Width == (unsigned int)aWindowSize.x)
						{
							if (displayModeList[i].Height == (unsigned int)aWindowSize.y)
							{
								numerator = displayModeList[i].RefreshRate.Numerator;
								denominator = displayModeList[i].RefreshRate.Denominator;
							}
						}
					}
				}
			}
		}
		// Release the adapter output.
		pOutput->Release();
		pOutput = 0;
	}

	result = usingAdapter->GetDesc(&usingAdapterDesc);
	if (FAILED(result))
	{
		return false;
	}

	myVideoCardMemory = (int)(usingAdapterDesc.DedicatedVideoMemory / 1024 / 1024);

	delete[] displayModeList;
	displayModeList = 0;

	factory->Release();
	factory = 0;

	if (myEnableVSync == true)
	{
		aNumDenumerator.x = numerator;
		aNumDenumerator.y = denominator;
	}
	else
	{
		aNumDenumerator.x = 0;
		aNumDenumerator.y = 1;
	}

	outAdapter = usingAdapter;
	return true;
}
