#pragma once
#include "Rendering/GraphicsInterface.h"
#include <Rendering/PostProcessRenderer.h>

struct IDXGISwapChain;
struct ID3D11Device;
struct ID3D11DeviceContext;
struct ID3D11RenderTargetView;
struct ID3D11SamplerState;
struct ID3D11Texture2D;
struct ID3D11DepthStencilState;
struct ID3D11DepthStencilView;
struct ID3D11RasterizerState;
struct IDXGIAdapter;

namespace Shift3D
{
	class CEngine;
	class CDirect3D11 : public CGraphicsInterface
	{
	public:
		CDirect3D11();
		~CDirect3D11();

		bool Init(const CEngine& aEngine, Vector2ui aWindowSize, bool aEnableVSync, bool aStartInFullScreen) override;
		void EndRenderFrame() override;
		void BeginRenderFrame() override;

		inline static ID3D11Device* GetDevice() { return GetD3D()->myDevice; }
		inline static ID3D11DeviceContext* GetDeviceContext() { return GetD3D()->myContext; }
	private:
		IDXGISwapChain* mySwapChain;
		ID3D11Device* myDevice;
		ID3D11DeviceContext* myContext;
		ID3D11RenderTargetView* myRenderTargetView;
		ID3D11DepthStencilView* myDepthStencilView;
		ID3D11DepthStencilState* myDepthStencilState;
		ID3D11Texture2D* myDepthStencilBuffer;
		ID3D11RasterizerState* myRasterizerState;
		ID3D11SamplerState* mySamplerState;
		CPostProcessRenderer myPostProcessRenderer;

		int myVideoCardMemory;

		void CreateDepthStencil();
		void CreateSamplerState();
		void CreateViewport();
		void CreateRenderStates();
		bool CollectAdapters(Vector2ui aWindowSize, Vector2i& aNumDenumerator, IDXGIAdapter*& outAdapter);
		
		inline static CDirect3D11* GetD3D() { return (CDirect3D11*)ourInterface; }
	};
}
