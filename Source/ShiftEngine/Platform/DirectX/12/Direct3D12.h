#pragma once
#include <d3d12.h>
#include <dxgi1_4.h>
#include <DirectXMath.h>
#include "Math/Vector2.h"
#include "Rendering/GraphicsInterface.h"


#define FRAME_BUFFER_COUNT 3


namespace Shift3D
{
	class CEngine;
	class CDirect3D12 : public CGraphicsInterface
	{
	public:
		CDirect3D12();
		~CDirect3D12();

		bool Init(const CEngine& aEngine, Vector2ui aWindowSize, bool aEnableVSync, bool aStartInFullScreen) override;
		void EndRenderFrame() override;
		void BeginRenderFrame() override;

	private:
		int myFrameIndex;
		int myRtvDescriptorSize;
		int myVideoCardMemory;

		// DX3D Memebers
		ID3D12Device* myDevice;
		IDXGISwapChain3* mySwapChain;
		ID3D12CommandQueue* myCommandQueue;
		ID3D12DescriptorHeap* myRtvDescriptorHeap;
		ID3D12Resource* myRenderTargets[FRAME_BUFFER_COUNT];
		ID3D12CommandAllocator* myCommandAllocator[FRAME_BUFFER_COUNT];
		ID3D12GraphicsCommandList* myCommandList;
		ID3D12Fence* myFence[FRAME_BUFFER_COUNT];
		HANDLE myFenceEvent;
		UINT64 myFenceValue[FRAME_BUFFER_COUNT];
		ID3D12PipelineState* myPipelineStateObject;
		ID3D12RootSignature* myRootSignature;
		D3D12_VIEWPORT myViewport;
		D3D12_RECT myScissorRect;
		ID3D12Resource* myVertexBuffer;
		D3D12_VERTEX_BUFFER_VIEW myVertexBufferView;

		// DX3D FUNCTIONS
		bool CollectAdapters(Vector2ui aWindowSize, Vector2i& aNumDenumerator, IDXGIAdapter*& outAdapter);
		bool CreateCmdQueue();
		bool CreateSwapChain(IDXGIFactory4* dxgiFactory, const CEngine& aEngine, DXGI_SAMPLE_DESC& sampleDesc, const bool aStartInFullscreen);
		bool CreateBackBuffers();
		bool CreateCmdAllocators();
		bool CreateCmdList();
		bool CreateFences();
		bool CreateRootSignature();
		void SetViewport(float aTopLeftX, float aTopLeftY, float aWidth, float aHeight, float aMinDepth = 0.f, float aMaxDepth = 1.f);
		void WaitForPreviousFrame();
		void UpdatePipeline();
	};

}