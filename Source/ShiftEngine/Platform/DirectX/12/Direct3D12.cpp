#include "stdafx.h"
#include "Direct3D12.h"
#include "d3dx12.h"
#include <dxgi1_4.h>
#include <D3Dcompiler.h>
#include "Engine.h"
#include <vector>
#pragma comment(lib,"d3d12.lib")
#pragma comment(lib,"dxgi.lib")

namespace Shift3D
{
	CDirect3D12::CDirect3D12()
		: CGraphicsInterface()
		, myPipelineStateObject(nullptr)
	{
	}


	CDirect3D12::~CDirect3D12()
	{
		// wait for the gpu to finish all frames
		for (int i = 0; i < FRAME_BUFFER_COUNT; ++i)
		{
			myFrameIndex = i;
			WaitForPreviousFrame();
		}

		// get swapchain out of full screen before exiting
		BOOL fs = false;
		if (mySwapChain->GetFullscreenState(&fs, NULL))
			mySwapChain->SetFullscreenState(false, NULL);

		SAFE_RELEASE(myDevice);
		SAFE_RELEASE(mySwapChain);
		SAFE_RELEASE(myCommandQueue);
		SAFE_RELEASE(myRtvDescriptorHeap);
		SAFE_RELEASE(myCommandList);

		for (int i = 0; i < FRAME_BUFFER_COUNT; ++i)
		{
			SAFE_RELEASE(myRenderTargets[i]);
			SAFE_RELEASE(myCommandAllocator[i]);
			SAFE_RELEASE(myFence[i]);
		};

		SAFE_RELEASE(myPipelineStateObject);
		SAFE_RELEASE(myRootSignature);
		SAFE_RELEASE(myVertexBuffer);
	}

	bool CDirect3D12::Init(const CEngine& aEngine, Vector2ui aWindowSize, bool aEnableVSync, bool aStartInFullScreen)
	{
		myEnableVSync = aEnableVSync;
		myWindowSize = aWindowSize;
		HRESULT result = S_OK;

		INFO_PRINT("%s", "Initiating DirectX 12...");

		IDXGIFactory4* dxgiFactory = nullptr;
		result = CreateDXGIFactory1(IID_PPV_ARGS(&dxgiFactory));
		if (FAILED(result))
		{
			ERROR_PRINT("%s", "Could not create dxFactory, exiting...");
			return false;
		}

		// -- DEVICE -- //
		Vector2<int> numDenum;
		IDXGIAdapter* adapter = nullptr;
		if (CollectAdapters(myWindowSize, numDenum, adapter))
		{
			INFO_PRINT("%s%s", "VSYNC Compatible: Yes, Enabled: ", myEnableVSync ? "Yes" : "No");
		}

		result = D3D12CreateDevice(adapter, D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&myDevice));
		if (FAILED(result))
		{
			ERROR_PRINT("%s", "Could not create dxDevice, exiting...");
			return false;
		}

		// -- COMMAND QUEUE -- //
		CreateCmdQueue();

		DXGI_SAMPLE_DESC sampleDesc = {};
		ZeroMemory(&sampleDesc, sizeof(DXGI_SAMPLE_DESC));
		sampleDesc.Count = 1; // Multi-sample count.

		CreateSwapChain(dxgiFactory, aEngine, sampleDesc, aStartInFullScreen);
		CreateBackBuffers();
		CreateCmdAllocators();
		CreateCmdList();
		CreateFences();
		CreateRootSignature();

		SetViewport(0.f, 0.f, static_cast<float>(aWindowSize.x), static_cast<float>(aWindowSize.y));

		myScissorRect.left = 0;
		myScissorRect.top = 0;
		myScissorRect.right = myWindowSize.x;
		myScissorRect.bottom = myWindowSize.y;


		INFO_PRINT("%s", "DirectX 12 successfully initiated.");
		return true;
	}

	void CDirect3D12::EndRenderFrame()
	{
		HRESULT result;

		UpdatePipeline();

		ID3D12CommandList* ppCommandLists[] = { myCommandList };
		myCommandQueue->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);
		result = myCommandQueue->Signal(myFence[myFrameIndex], myFenceValue[myFrameIndex]);
		if (FAILED(result))
		{
			ERROR_PRINT("%s", "Error when signaling commandqueue.");
		}

		if (myEnableVSync)
		{
			result = mySwapChain->Present(1, 0);
		}
		else
		{
			result = mySwapChain->Present(0, 0);
		}
		if (FAILED(result))
		{
			ERROR_PRINT("%s", "Error when presenting frame.");
		}

	}

	void CDirect3D12::BeginRenderFrame()
	{
		
	}

	bool CDirect3D12::CollectAdapters(Vector2ui aWindowSize, Vector2i & aNumDenumerator, IDXGIAdapter *& outAdapter)
	{
		HRESULT result = S_OK;
		IDXGIFactory4* factory;

		DXGI_MODE_DESC* displayModeList = nullptr;
		unsigned numModes = 0;
		unsigned denominator = 0;
		unsigned numerator = 0;
		result = CreateDXGIFactory1(IID_PPV_ARGS(&factory));
		if (FAILED(result))
		{
			ERROR_PRINT("%s", "Could not create dxFactory, exiting...");
			return false;
		}

		IDXGIAdapter* usingAdapter = nullptr;
		int adapterIndex = 0;
		std::vector<DXGI_ADAPTER_DESC> myAdapterDescs;
		std::vector<IDXGIAdapter*> myAdapters;
		while (factory->EnumAdapters(adapterIndex, &usingAdapter) != DXGI_ERROR_NOT_FOUND)
		{
			DXGI_ADAPTER_DESC adapterDesc;
			usingAdapter->GetDesc(&adapterDesc);
			myAdapterDescs.push_back(adapterDesc);
			myAdapters.push_back(usingAdapter);
			++adapterIndex;
		}

		if (adapterIndex == 0)
		{
			return false;
		}

		INFO_PRINT("%s", "Video card(s) detected: ");
		for (DXGI_ADAPTER_DESC desc : myAdapterDescs)
		{
			int memory = (int)(desc.DedicatedVideoMemory / 1024 / 1024);
			INFO_PRINT("	%ls%s%i%s", desc.Description, " Mem: ", memory, "Mb");
			memory;
		}

		INFO_PRINT("%s", "Detecting best card...");

		DXGI_ADAPTER_DESC usingAdapterDesc = myAdapterDescs[0];
		usingAdapter = myAdapters[0];

		const std::wstring nvidia = L"NVIDIA";
		const std::wstring amd = L"AMD";

		int memory = (int)(usingAdapterDesc.DedicatedVideoMemory / 1024 / 1024);
		int mostMem = 0;

		for (unsigned int i = 0; i < myAdapterDescs.size(); i++)
		{
			DXGI_ADAPTER_DESC desc = myAdapterDescs[i];
			memory = (int)(desc.DedicatedVideoMemory / 1024 / 1024);
			std::wstring name = desc.Description;
			if (name.find(nvidia) != std::wstring::npos || name.find(amd) != std::wstring::npos)
			{
				if (memory > mostMem)
				{
					mostMem = memory;
					usingAdapterDesc = desc;
					usingAdapter = myAdapters[i];
				}
			}
		}
		INFO_PRINT("%s%ls%s%i", "Using graphic card: ", usingAdapterDesc.Description, " Dedicated Mem: ", mostMem);

		IDXGIOutput* pOutput = nullptr;
		if (usingAdapter->EnumOutputs(0, &pOutput) != DXGI_ERROR_NOT_FOUND)
		{
			// Get the number of modes that fit the DXGI_FORMAT_R8G8B8A8_UNORM display format for the adapter output (monitor).
			result = pOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, NULL);
			if (!FAILED(result))
			{
				// Create a list to hold all the possible display modes for this monitor/video card combination.
				displayModeList = new DXGI_MODE_DESC[numModes];
				if (displayModeList)
				{
					// Now fill the display mode list structures.
					result = pOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, displayModeList);
					if (!FAILED(result))
					{
						// Now go through all the display modes and find the one that matches the screen width and height.
						// When a match is found store the numerator and denominator of the refresh rate for that monitor.
						for (unsigned int i = 0; i < numModes; i++)
						{
							if (displayModeList[i].Width == (unsigned int)aWindowSize.x)
							{
								if (displayModeList[i].Height == (unsigned int)aWindowSize.y)
								{
									numerator = displayModeList[i].RefreshRate.Numerator;
									denominator = displayModeList[i].RefreshRate.Denominator;
								}
							}
						}
					}
				}
			}
			// Release the adapter output.
			pOutput->Release();
			pOutput = 0;
		}

		result = usingAdapter->GetDesc(&usingAdapterDesc);
		if (FAILED(result))
		{
			return false;
		}

		myVideoCardMemory = (int)(usingAdapterDesc.DedicatedVideoMemory / 1024 / 1024);

		delete[] displayModeList;
		displayModeList = 0;

		factory->Release();
		factory = 0;

		if (myEnableVSync == true)
		{
			aNumDenumerator.x = numerator;
			aNumDenumerator.y = denominator;
		}
		else
		{
			aNumDenumerator.x = 0;
			aNumDenumerator.y = 1;
		}

		outAdapter = usingAdapter;
		return true;
	}

	bool CDirect3D12::CreateCmdQueue()
	{
		D3D12_COMMAND_QUEUE_DESC cmdQDesc = {};
		HRESULT result = myDevice->CreateCommandQueue(&cmdQDesc, IID_PPV_ARGS(&myCommandQueue));
		if (FAILED(result))
		{
			ERROR_PRINT("%s", "Could not create dxCmdQueue, exiting...");
			return false;
		}
		return true;
	}

	bool CDirect3D12::CreateSwapChain(IDXGIFactory4* dxgiFactory, const CEngine& aEngine, DXGI_SAMPLE_DESC& sampleDesc, const bool aStartInFullScreen)
	{
		DXGI_MODE_DESC backBufferDesc = {};
		ZeroMemory(&backBufferDesc, sizeof(DXGI_MODE_DESC));
		backBufferDesc.Width = myWindowSize.x;
		backBufferDesc.Height = myWindowSize.y;
		backBufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;

		DXGI_SWAP_CHAIN_DESC swapChainDesc = {};
		ZeroMemory(&swapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
		swapChainDesc.BufferCount = FRAME_BUFFER_COUNT;
		swapChainDesc.BufferDesc = backBufferDesc;
		swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
		swapChainDesc.OutputWindow = *(aEngine.myHwnd);
		swapChainDesc.SampleDesc = sampleDesc;
		swapChainDesc.Windowed = !aStartInFullScreen;

		IDXGISwapChain* tempSwapChain;

		HRESULT result = dxgiFactory->CreateSwapChain(myCommandQueue, &swapChainDesc, &tempSwapChain);
		if (FAILED(result))
		{
			ERROR_PRINT("%s", "Could not create dxSwapChain, exiting...");
			return false;
		}
		mySwapChain = static_cast<IDXGISwapChain3*>(tempSwapChain);
		myFrameIndex = mySwapChain->GetCurrentBackBufferIndex();
		return true;
	}

	bool CDirect3D12::CreateBackBuffers()
	{
		D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc = {};
		rtvHeapDesc.NumDescriptors = FRAME_BUFFER_COUNT;
		rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
		rtvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;

		HRESULT result = myDevice->CreateDescriptorHeap(&rtvHeapDesc, IID_PPV_ARGS(&myRtvDescriptorHeap));
		if (FAILED(result))
		{
			ERROR_PRINT("%s", "Could not create dxDescHeap, exiting...");
			return false;
		}

		myRtvDescriptorSize = myDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
		CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle(myRtvDescriptorHeap->GetCPUDescriptorHandleForHeapStart());

		for (unsigned int i = 0; i < FRAME_BUFFER_COUNT; ++i)
		{
			result = mySwapChain->GetBuffer(i, IID_PPV_ARGS(&myRenderTargets[i]));
			if (FAILED(result))
			{
				ERROR_PRINT("%s", "Could not get swapchain-buffer, exiting...");
				return false;
			}

			myDevice->CreateRenderTargetView(myRenderTargets[i], nullptr, rtvHandle);
			rtvHandle.Offset(1, myRtvDescriptorSize);
		}
		return true;
	}

	bool CDirect3D12::CreateCmdAllocators()
	{
		for (unsigned int i = 0; i < FRAME_BUFFER_COUNT; ++i)
		{
			HRESULT result = myDevice->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&myCommandAllocator[i]));
			if (FAILED(result))
			{
				ERROR_PRINT("%s", "Could not create dxCmdAllocator, exiting...");
				return false;
			}
		}
		return true;
	}

	bool CDirect3D12::CreateCmdList()
	{
		HRESULT result = myDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, myCommandAllocator[0], nullptr, IID_PPV_ARGS(&myCommandList));
		if (FAILED(result))
		{
			ERROR_PRINT("%s", "Could not create dxCmdList, exiting...");
			return false;
		}
		myCommandList->Close();
		return true;
	}

	bool CDirect3D12::CreateFences()
	{
		for (unsigned int i = 0; i < FRAME_BUFFER_COUNT; ++i)
		{
			HRESULT result = myDevice->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&myFence[i]));
			if (FAILED(result))
			{
				ERROR_PRINT("%s", "Could not create dxFence, exiting...");
				return false;
			}
			myFenceValue[i] = 0;
		}

		myFenceEvent = CreateEvent(nullptr, FALSE, FALSE, nullptr);
		if (myFenceEvent == nullptr)
		{
			ERROR_PRINT("%s", "Could not create dxFenceEvent, exiting...");
			return false;
		}
		return true;
	}

	bool CDirect3D12::CreateRootSignature()
	{
		CD3DX12_ROOT_SIGNATURE_DESC rootSignatureDesc;
		rootSignatureDesc.Init(0, nullptr, 0, nullptr, D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT);
		ID3DBlob* signature;
		HRESULT result = D3D12SerializeRootSignature(&rootSignatureDesc, D3D_ROOT_SIGNATURE_VERSION_1, &signature, nullptr);
		if (FAILED(result))
		{
			ERROR_PRINT("%s", "Could not serialize root signature, exiting...");
			return false;
		}

		result = myDevice->CreateRootSignature(0, signature->GetBufferPointer(), signature->GetBufferSize(), IID_PPV_ARGS(&myRootSignature));
		if (FAILED(result))
		{
			ERROR_PRINT("%s", "Could not create root signature, exiting...");
			return false;
		}
		return true;
	}

	void CDirect3D12::SetViewport(float aTopLeftX, float aTopLeftY, float aWidth, float aHeight, float aMinDepth, float aMaxDepth)
	{
		myViewport.TopLeftX = aTopLeftX;
		myViewport.TopLeftY = aTopLeftY;
		myViewport.Width = aWidth;
		myViewport.Height = aHeight;
		myViewport.MinDepth = aMinDepth;
		myViewport.MaxDepth = aMaxDepth;
	}

	void CDirect3D12::WaitForPreviousFrame()
	{
		HRESULT result;

		myFrameIndex = mySwapChain->GetCurrentBackBufferIndex();
		if (myFence[myFrameIndex]->GetCompletedValue() < myFenceValue[myFrameIndex])
		{
			result = myFence[myFrameIndex]->SetEventOnCompletion(myFenceValue[myFrameIndex], myFenceEvent);
			if (FAILED(result))
			{
				ERROR_PRINT("%s", "ERROR WHILE WAITING FOR FRAME!");
			}
			WaitForSingleObject(myFenceEvent, INFINITE);
		}
		++myFenceValue[myFrameIndex];
	}

	void CDirect3D12::UpdatePipeline()
	{
		HRESULT result;

		WaitForPreviousFrame();

		result = myCommandAllocator[myFrameIndex]->Reset(); 
		if (FAILED(result))
		{
			ERROR_PRINT("%s", "ERROR WHILE RESETTING CMDALLOCATOR!");
		}

		result = myCommandList->Reset(myCommandAllocator[myFrameIndex], nullptr);
		if (FAILED(result))
		{
			ERROR_PRINT("%s", "ERROR WHILE RESETTING CMDALLOCATOR!");
		}


		myCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(myRenderTargets[myFrameIndex], D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET));

		// here we again get the handle to our current render target view so we can set it as the render target in the output merger stage of the pipeline
		CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle(myRtvDescriptorHeap->GetCPUDescriptorHandleForHeapStart(), myFrameIndex, myRtvDescriptorSize);

		// set the render target for the output merger stage (the output of the pipeline)
		myCommandList->OMSetRenderTargets(1, &rtvHandle, FALSE, nullptr);

		// Clear the render target by using the ClearRenderTargetView command
		const float clearColor[] = { 0.0f, 0.2f, 0.4f, 1.0f };
		myCommandList->ClearRenderTargetView(rtvHandle, clearColor, 0, nullptr);


		myCommandList->SetGraphicsRootSignature(myRootSignature);
		myCommandList->RSSetViewports(1, &myViewport);
		myCommandList->RSSetScissorRects(1, &myScissorRect);
		myCommandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		myCommandList->IASetVertexBuffers(0, 1, &myVertexBufferView);
		myCommandList->DrawInstanced(3, 1, 0, 0);
		myCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(myRenderTargets[myFrameIndex], D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT));

		result = myCommandList->Close();
		if (FAILED(result))
		{
			ERROR_PRINT("%s", "ERROR WHILE CLOSING CMDLIST!");
		}
	}

}