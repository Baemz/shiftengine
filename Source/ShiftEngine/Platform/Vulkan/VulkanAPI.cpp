#include "stdafx.h"
#include "VulkanAPI.h"

using namespace Shift3D;

CVulkanAPI::CVulkanAPI()
	: CGraphicsInterface()
{
}


CVulkanAPI::~CVulkanAPI()
{
}

bool Shift3D::CVulkanAPI::Init(const CEngine& aEngine, Vector2ui aWindowSize, bool aEnableVSync, bool aStartInFullScreen)
{
	myEnableVSync = aEnableVSync;
	myWindowSize = aWindowSize;
	aStartInFullScreen;

	INFO_PRINT("%s", "Initiating Vulkan...");
	VkApplicationInfo appInfo = {};
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	const std::wstring ws = aEngine.GetAppName();
	const std::string appName(ws.begin(), ws.end());
	appInfo.pApplicationName = appName.c_str();
	appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
	appInfo.pEngineName = "Shift3D Engine";
	appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
	appInfo.apiVersion = VK_API_VERSION_1_0;

	VkInstanceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	createInfo.pApplicationInfo = &appInfo;

	std::vector<const char*> enabledExtensions = { VK_KHR_SURFACE_EXTENSION_NAME };

#if defined(_WIN32)
	enabledExtensions.push_back(VK_KHR_WIN32_SURFACE_EXTENSION_NAME);
#elif defined(__ANDROID__)
	enabledExtensions.push_back(VK_KHR_ANDROID_SURFACE_EXTENSION_NAME);
#elif defined(__linux__)
	enabledExtensions.push_back(VK_KHR_XCB_SURFACE_EXTENSION_NAME);
#endif

	createInfo.enabledExtensionCount = static_cast<uint32_t>(enabledExtensions.size());
	createInfo.ppEnabledExtensionNames = enabledExtensions.data();
	createInfo.enabledLayerCount = 0;

	INFO_PRINT("%s", "	Creating instance...");
	VkResult result = vkCreateInstance(&createInfo, nullptr, myVkInstance.replace());
	if (result != VK_SUCCESS)
	{
		ERROR_PRINT("%s", "Could not create Vulkan instance, exiting...");
		return false;
	}

	INFO_PRINT("%s", "	Creating Window-surface...");
#if defined(_WIN32)
	CreateWindowsSurface(aEngine.GetHWND(), aEngine.GetHINSTANCE());
#endif


	INFO_PRINT("%s", "	Finding best GPU...");
	if (GetGPUDevice() == false)
	{
		ERROR_PRINT("&s", "Failed to find GPUs with Vulkan support!");
		return false;
	}


	INFO_PRINT("%s", "Vulkan successfully initiated.");
	return true;
}

void Shift3D::CVulkanAPI::EndRenderFrame()
{
}

void Shift3D::CVulkanAPI::BeginRenderFrame()
{
}

bool Shift3D::CVulkanAPI::GetGPUDevice()
{
	uint32_t deviceCount = 0;
	vkEnumeratePhysicalDevices(myVkInstance, &deviceCount, nullptr);
	if (deviceCount == 0) 
	{
		return false;
	}

	std::vector<VkPhysicalDevice> devices(deviceCount);
	vkEnumeratePhysicalDevices(myVkInstance, &deviceCount, devices.data());

	for (const auto& device : devices) 
	{
		if (IsDeviceSuitable(device)) 
		{
			myPhysicalDevice = device;
			break;
		}
	}
	if (myPhysicalDevice == VK_NULL_HANDLE)
	{
		return false;
	}

	INFO_PRINT("%s", "	Creating logical device...");
	return CreateLogicalDevice();
}

bool Shift3D::CVulkanAPI::IsDeviceSuitable(VkPhysicalDevice aDevice)
{
	VkPhysicalDeviceProperties deviceProperties;
	vkGetPhysicalDeviceProperties(aDevice, &deviceProperties);
	const char* name = deviceProperties.deviceName;
	INFO_PRINT("%s%s", "		- Detected GPU: ", name);

	QueueFamilyIndices indices = FindQueueFamilies(aDevice);

	if (indices.IsComplete())
	{
		INFO_PRINT("%s%s", "		- Using GPU: ", name);
		return true;
	}
	else
	{
		return false;
	}
}

QueueFamilyIndices Shift3D::CVulkanAPI::FindQueueFamilies(VkPhysicalDevice aDevice)
{
	QueueFamilyIndices indices;

	uint32_t queueFamilyCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(aDevice, &queueFamilyCount, nullptr);

	std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(aDevice, &queueFamilyCount, queueFamilies.data());

	int i = 0;
	for (const auto& queueFamily : queueFamilies) 
	{
		VkBool32 presentSupport = false;
		vkGetPhysicalDeviceSurfaceSupportKHR(aDevice, i, myWndSurface, &presentSupport);
		if (queueFamily.queueCount > 0 && presentSupport > 0)
		{
			indices.myPresentFamily = i;
		}

		if (queueFamily.queueCount > 0 && queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT)
		{
			indices.myGraphicsFamily = i;
		}

		if (indices.IsComplete()) 
		{
			break;
		}
		i++;
	}

	return indices;
}

bool Shift3D::CVulkanAPI::CreateLogicalDevice()
{
	QueueFamilyIndices indices = FindQueueFamilies(myPhysicalDevice);

	VkDeviceQueueCreateInfo queueCreateInfo = {};
	queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
	queueCreateInfo.queueFamilyIndex = indices.myGraphicsFamily;
	queueCreateInfo.queueCount = 1;
	float queuePriority = 1.0f;
	queueCreateInfo.pQueuePriorities = &queuePriority;

	VkPhysicalDeviceFeatures deviceFeatures = {};

	VkDeviceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	createInfo.pQueueCreateInfos = &queueCreateInfo;
	createInfo.queueCreateInfoCount = 1;
	createInfo.pEnabledFeatures = &deviceFeatures;

	createInfo.enabledExtensionCount = 0;

	if (false) 
	{
		createInfo.enabledLayerCount = 0;
		createInfo.ppEnabledLayerNames = nullptr;
	}
	else 
	{
		createInfo.enabledLayerCount = 0;
	}

	if (vkCreateDevice(myPhysicalDevice, &createInfo, nullptr, myDevice.replace()) != VK_SUCCESS) 
	{
		ERROR_PRINT("&s", "Failed to create logical device!");
	}

	vkGetDeviceQueue(myDevice, indices.myGraphicsFamily, 0, &myGraphicsQueue);

	return true;
}

#if defined(_WIN32)
bool Shift3D::CVulkanAPI::CreateWindowsSurface(HWND aHwnd, HINSTANCE aHInstance)
{
	VkWin32SurfaceCreateInfoKHR createInfo;
	createInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
	createInfo.hwnd = aHwnd;
	createInfo.hinstance = aHInstance;

	auto CreateWin32SurfaceKHR = (PFN_vkCreateWin32SurfaceKHR)vkGetInstanceProcAddr(myVkInstance, "vkCreateWin32SurfaceKHR");

	if (!CreateWin32SurfaceKHR || CreateWin32SurfaceKHR(myVkInstance, &createInfo,
		nullptr, myWndSurface.replace()) != VK_SUCCESS) 
	{
		ERROR_PRINT("&s", "Failed to create window-surface!");
		return false;
	}

	return true;
}
#endif
