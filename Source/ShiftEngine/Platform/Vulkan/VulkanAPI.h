#pragma once
#pragma once
#include "Rendering/GraphicsInterface.h"
#include <vulkan/vulkan.h>
#include "Platform/Vulkan/VulkanDeleter.h"

namespace Shift3D
{
	struct QueueFamilyIndices 
	{
		int myGraphicsFamily = -1;
		int myPresentFamily = -1;

		bool IsComplete() 
		{
			return myGraphicsFamily >= 0 && myPresentFamily >= 0;
		}
	};

	class CVulkanAPI : public CGraphicsInterface
	{
	public:
		CVulkanAPI();
		~CVulkanAPI();

		bool Init(const CEngine& aEngine, Vector2ui aWindowSize, bool aEnableVSync, bool aStartInFullScreen) override;
		void EndRenderFrame() override;
		void BeginRenderFrame() override;

	private:
		CVulkanDeleter<VkInstance> myVkInstance { vkDestroyInstance }; 
		VkPhysicalDevice myPhysicalDevice = VK_NULL_HANDLE;
		CVulkanDeleter<VkDevice> myDevice { vkDestroyDevice };
		CVulkanDeleter<VkSurfaceKHR> myWndSurface { myVkInstance, vkDestroySurfaceKHR }; 
		VkQueue myGraphicsQueue; 
		VkQueue myPresentQueue;

		bool GetGPUDevice();
		bool IsDeviceSuitable(VkPhysicalDevice aDevice);
		QueueFamilyIndices FindQueueFamilies(VkPhysicalDevice aDevice);
		bool CreateLogicalDevice();

#if defined(_WIN32)
		bool CreateWindowsSurface(HWND aHwnd, HINSTANCE aHInstance);
#endif

	};
}
