#pragma once
#include "DX11BaseBuffer.h"

namespace Shift3D
{
	class DX11VertexBuffer : public DX11BaseBuffer
	{
	public:
		DX11VertexBuffer(void* aData, const size_t aVertexCount, const size_t aStrideSize, bool aCPUAccess);
		~DX11VertexBuffer();

		void Bind();

		const size_t& GetStrideSize() const;
		const size_t& GetVertexCount() const;
		const size_t& GetSize() const;

	private:
		size_t myStrideSize;
		size_t myVertexCount;
		size_t mySize;
	};
}


