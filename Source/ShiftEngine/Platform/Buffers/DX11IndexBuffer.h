#pragma once
#include "DX11BaseBuffer.h"

namespace Shift3D
{
	class DX11IndexBuffer : public DX11BaseBuffer
	{
	public:
		DX11IndexBuffer(const std::vector<UINT>& aIndices);
		~DX11IndexBuffer();

		void Bind();

		const size_t& GetIndexCount() const;

	private:
		size_t myIndexCount;

	};
}

