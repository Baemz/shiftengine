#include "stdafx.h"
#include "DX11VertexBuffer.h"
#include <d3d11.h>

using namespace Shift3D;

DX11VertexBuffer::DX11VertexBuffer(void* aData, const size_t aVertexCount, const size_t aStrideSize, bool aCPUAccess)
	: DX11BaseBuffer()
{
	myStrideSize = aStrideSize;
	myVertexCount = aVertexCount;
	mySize = myVertexCount * myStrideSize;

	if (mySize == 0)
	{
		return;
	}
	if (aData == nullptr)
	{
		OutputDebugStringA("ERROR! The data was nullptr");
		return;
	}


	D3D11_BUFFER_DESC vertexBufferDescription;
	if (aCPUAccess == true)
	{
		vertexBufferDescription.Usage = D3D11_USAGE_DYNAMIC;
		vertexBufferDescription.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	}
	else
	{
		vertexBufferDescription.Usage = D3D11_USAGE_DEFAULT;
		vertexBufferDescription.CPUAccessFlags = 0;
	}

	vertexBufferDescription.ByteWidth = static_cast<UINT>(mySize);
	vertexBufferDescription.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDescription.MiscFlags = 0;
	vertexBufferDescription.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA vertexData = {};
	vertexData.pSysMem = aData;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	CreateBuffer(vertexBufferDescription, &vertexData);
}


DX11VertexBuffer::~DX11VertexBuffer()
{
}

void DX11VertexBuffer::Bind()
{
	const UINT strideSize = static_cast<UINT>(myStrideSize);
	const UINT offset = 0;
	myDeviceContext->IASetVertexBuffers(0, 1, &myBuffer, &strideSize, &offset);
}

const size_t& DX11VertexBuffer::GetStrideSize() const
{
	return myStrideSize;
}

const size_t& DX11VertexBuffer::GetVertexCount() const
{
	return myVertexCount;
}

const size_t& DX11VertexBuffer::GetSize() const
{
	return mySize;
}
