#include "stdafx.h"
#include "Platform/Buffers/DX11BaseBuffer.h"
#include <d3d11.h>
#include <Platform/DirectX/11/Direct3D11.h>

using namespace Shift3D;

DX11BaseBuffer::DX11BaseBuffer(const D3D11_BUFFER_DESC& aBufferDescription)
	:DX11BaseBuffer()
{
	CreateBuffer(aBufferDescription);
}

DX11BaseBuffer::DX11BaseBuffer()
{
	mySize = 0;
	myBuffer = nullptr;
	myDevice = CDirect3D11::GetDevice();
	myDeviceContext = CDirect3D11::GetDeviceContext();
}

DX11BaseBuffer::~DX11BaseBuffer()
{
	myBuffer->Release();
	myBuffer = nullptr;
}

void DX11BaseBuffer::UpdateData(void *aData)
{
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedObjectResource;

	if (FAILED(result = myDeviceContext->Map(myBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedObjectResource)))
	{
		// PRINT ERROR
		return;
	}

	memcpy(mappedObjectResource.pData, aData, mySize);
	myDeviceContext->Unmap(myBuffer, 0);
}

void DX11BaseBuffer::CreateBuffer(const D3D11_BUFFER_DESC& aBufferDescription, D3D11_SUBRESOURCE_DATA* aInitialData)
{
	mySize = static_cast<size_t>(aBufferDescription.ByteWidth);
	if (myBuffer != nullptr)
	{
		// PRINT ERROR
	}
	if (&aBufferDescription == nullptr)
	{
		return;
	}

	HRESULT result;
	if (FAILED(result = myDevice->CreateBuffer(&aBufferDescription, aInitialData, &myBuffer)))
	{
	}
}