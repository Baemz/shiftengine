#pragma once

struct D3D11_BUFFER_DESC;
struct D3D11_SUBRESOURCE_DATA;
struct ID3D11Buffer;
struct ID3D11Device;
struct ID3D11DeviceContext;

namespace Shift3D
{
	class DX11BaseBuffer
	{
	public:
		DX11BaseBuffer();
		DX11BaseBuffer(const D3D11_BUFFER_DESC& aBufferDescription);
		virtual ~DX11BaseBuffer();
		void UpdateData(void *aData);

	protected:
		void CreateBuffer(const D3D11_BUFFER_DESC& aBufferDescription, D3D11_SUBRESOURCE_DATA* aInitialData = nullptr);

		size_t mySize;
		ID3D11Buffer* myBuffer;
		ID3D11DeviceContext* myDeviceContext;

	private:
		ID3D11Device* myDevice;
	};
}