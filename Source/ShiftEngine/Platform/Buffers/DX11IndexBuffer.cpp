#include "stdafx.h"
#include "DX11IndexBuffer.h"
#include <d3d11.h>

using namespace Shift3D;

DX11IndexBuffer::DX11IndexBuffer(const std::vector<UINT>& aIndices)
	: DX11BaseBuffer()
{
	myIndexCount = aIndices.size();

	D3D11_BUFFER_DESC indexBufferDescription;
	indexBufferDescription.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDescription.ByteWidth = static_cast<UINT>(aIndices.size() * sizeof(UINT));
	indexBufferDescription.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDescription.CPUAccessFlags = 0;
	indexBufferDescription.MiscFlags = 0;
	indexBufferDescription.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA indexData;
	indexData.pSysMem = &aIndices[0];
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	CreateBuffer(indexBufferDescription, &indexData);
}


DX11IndexBuffer::~DX11IndexBuffer()
{
}

void Shift3D::DX11IndexBuffer::Bind()
{
	myDeviceContext->IASetIndexBuffer(myBuffer, DXGI_FORMAT_R32_UINT, 0);
}

const size_t& Shift3D::DX11IndexBuffer::GetIndexCount() const
{
	return myIndexCount;
}
