#include "stdafx.h"
#include "DX11ConstantBuffer.h"
#include <d3d11.h>

using namespace Shift3D;

DX11ConstantBuffer::DX11ConstantBuffer(unsigned char aRegisterIndex)
	: DX11BaseBuffer()
{
	myData = nullptr;
	myRegisterIndex = aRegisterIndex;
	mySize = 0;
	myCurrentIndex = 0;
}


DX11ConstantBuffer::~DX11ConstantBuffer()
{
}

void DX11ConstantBuffer::Init()
{
	D3D11_BUFFER_DESC bufferDesc;
	ZeroMemory(&bufferDesc, sizeof(bufferDesc));

	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.ByteWidth = static_cast<UINT>(mySize);
	bufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bufferDesc.MiscFlags = 0;
	bufferDesc.StructureByteStride = 0;

	CreateBuffer(bufferDesc);
}

void DX11ConstantBuffer::UpdateBuffer()
{
	UpdateData(myData);
	myCurrentIndex = 0;
}

void DX11ConstantBuffer::BindVS()
{
	myDeviceContext->VSSetConstantBuffers(myRegisterIndex, 1, &myBuffer);
}

void Shift3D::DX11ConstantBuffer::BindPS()
{
	myDeviceContext->PSSetConstantBuffers(myRegisterIndex, 1, &myBuffer);
}
