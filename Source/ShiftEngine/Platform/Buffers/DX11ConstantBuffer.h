#pragma once
#include "DX11BaseBuffer.h"

struct ID3D11Buffer;

namespace Shift3D
{
	class DX11ConstantBuffer : DX11BaseBuffer
	{
	public:
		DX11ConstantBuffer(unsigned char aRegisterIndex);
		~DX11ConstantBuffer();
		void Init();
		void UpdateBuffer();

		void BindVS();
		void BindPS();

		/*void BindAll();
		void BindCommon();

		void BindHS();
		void BindDS();
		void BindGS();
		void BindCS();*/

		template<typename T>
		void AddDataMember();
		template<typename T>
		void SetData(T aData);
	private:
		unsigned char myRegisterIndex;
		size_t myCurrentIndex;
		void* myData;
	};

	template<typename T>
	void DX11ConstantBuffer::AddDataMember()
	{
		mySize += sizeof(T);
		free(myData);
		myData = malloc(mySize);
	}

	template<typename T>
	void DX11ConstantBuffer::SetData(T aData)
	{
		memcpy(static_cast<char*>(myData) + (sizeof(char) * myCurrentIndex), &aData, sizeof(T));
		myCurrentIndex += sizeof(T);
	}
}
