#include "stdafx.h"
#include "DX11Shader.h"
#include <d3d11.h>
#include <D3Dcompiler.h>
#include <Platform/DirectX/11/Direct3D11.h>

using namespace Shift3D;
static void CreateLayoutPart(std::vector<D3D11_INPUT_ELEMENT_DESC>& aInputLayout, LPCSTR aName, int aSematicIndex, DXGI_FORMAT aFormat, int aInputSlot, D3D11_INPUT_CLASSIFICATION aClassification, int aInstanceStep, bool aShouldResetDataCount = false);

DX11Shader::DX11Shader()
	: myPixelShader(nullptr)
	, myVertexShader(nullptr)
	, myInputLayout(nullptr)
{
	myDevice = CDirect3D11::GetDevice();
	myDeviceContext = CDirect3D11::GetDeviceContext();
}

DX11Shader::~DX11Shader()
{
	SAFE_RELEASE(myInputLayout);
	SAFE_RELEASE(myVertexShader); 
	SAFE_RELEASE(myPixelShader);
}

void DX11Shader::SetVShader(const char* aVertexShaderPath, const char* aEntryPoint, const eInputLayoutPart aInputLayout)
{
	std::vector<D3D11_INPUT_ELEMENT_DESC> inputLayout;

	if ((aInputLayout & ePosition) != 0)
	{
		CreateLayoutPart(inputLayout, "SV_POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_INPUT_PER_VERTEX_DATA, 0);
	}
	if ((aInputLayout & ePosition2D) != 0)
	{
		CreateLayoutPart(inputLayout, "POSITION2D", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_INPUT_PER_VERTEX_DATA, 0);
	}
	if ((aInputLayout & eNormal) != 0)
	{
		CreateLayoutPart(inputLayout, "NORMAL", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_INPUT_PER_VERTEX_DATA, 0);
	}
	if ((aInputLayout & eTangent) != 0)
	{
		CreateLayoutPart(inputLayout, "TANGENT", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_INPUT_PER_VERTEX_DATA, 0);
	}
	if ((aInputLayout & eBinormal) != 0)
	{
		CreateLayoutPart(inputLayout, "BINORMAL", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_INPUT_PER_VERTEX_DATA, 0);
	}
	if ((aInputLayout & eUV) != 0)
	{
		CreateLayoutPart(inputLayout, "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_INPUT_PER_VERTEX_DATA, 0);
	}

	UINT elementCount = static_cast<UINT>(inputLayout.size());
	ID3D10Blob* dataBlob = nullptr;
	CompileShader(aVertexShaderPath, aEntryPoint, dataBlob, DX11Shader::ShaderType::Vertex);

	void* vertexShaderByteCode = dataBlob->GetBufferPointer();
	SIZE_T vertexBytecodeSize = dataBlob->GetBufferSize();

	HRESULT result = myDevice->CreateVertexShader(vertexShaderByteCode, vertexBytecodeSize, nullptr, &myVertexShader);
	if (FAILED(result))
	{
		ERROR_PRINT("%s", "Failed to create Vertex Shader.");
	}
	result = myDevice->CreateInputLayout(&inputLayout[0], elementCount, vertexShaderByteCode, vertexBytecodeSize, &myInputLayout);
	if (FAILED(result))
	{
		ERROR_PRINT("%s", "Failed to create Vertex InputLayer.");
	}
}

void DX11Shader::SetPShader(const char* aPixelShaderPath, const char* aEntryPoint)
{
	ID3D10Blob* dataBlob = nullptr;
	CompileShader(aPixelShaderPath, aEntryPoint, dataBlob, DX11Shader::ShaderType::Pixel);

	void* pixelShaderByteCode = dataBlob->GetBufferPointer();
	SIZE_T pixelBytecodeSize = dataBlob->GetBufferSize();

	HRESULT result = myDevice->CreatePixelShader(pixelShaderByteCode, pixelBytecodeSize, nullptr, &myPixelShader);
	if (FAILED(result))
	{
		ERROR_PRINT("%s", "Failed to create Pixel Shader.");
	}
}

void DX11Shader::BindVShader()
{
	if (myVertexShader != nullptr && myInputLayout != nullptr)
	{
		myDeviceContext->IASetInputLayout(myInputLayout);
		myDeviceContext->VSSetShader(myVertexShader, nullptr, 0);
	}
}

void DX11Shader::BindPShader()
{
	if (myPixelShader != nullptr)
	{
		myDeviceContext->PSSetShader(myPixelShader, nullptr, 0);
		myDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	}
}

void Shift3D::DX11Shader::BindGShader()
{
}

void Shift3D::DX11Shader::BindCShader()
{
}

void DX11Shader::BindCommon()
{
	if (myPixelShader != nullptr && myVertexShader != nullptr && myInputLayout != nullptr)
	{
		myDeviceContext->IASetInputLayout(myInputLayout);
		myDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		myDeviceContext->VSSetShader(myVertexShader, nullptr, 0);
		myDeviceContext->PSSetShader(myPixelShader, nullptr, 0);
	}
}

void Shift3D::DX11Shader::BindAll()
{
}

void CreateLayoutPart(std::vector<D3D11_INPUT_ELEMENT_DESC>& aInputLayout, LPCSTR aName, int aSematicIndex, DXGI_FORMAT aFormat, int aInputSlot, D3D11_INPUT_CLASSIFICATION aClassification, int aInstanceStep, bool aShouldResetDataCount)
{
	D3D11_INPUT_ELEMENT_DESC layout;
	layout.SemanticName = aName;
	layout.SemanticIndex = aSematicIndex;
	layout.Format = aFormat;
	layout.InputSlot = aInputSlot;
	if (aShouldResetDataCount == true)
	{
		layout.AlignedByteOffset = 0;
	}
	else
	{
		layout.AlignedByteOffset = (aInputLayout.size() > 0) ? D3D11_APPEND_ALIGNED_ELEMENT : 0;

	}
	layout.InputSlotClass = aClassification;
	layout.InstanceDataStepRate = aInstanceStep;
	aInputLayout.push_back(layout);
}

void DX11Shader::CompileShader(const char* aPath, const char* aEntryPoint, ID3D10Blob*& aDataBlob, const ShaderType aShaderType)
{
	const char* shaderVersion = "";
	switch (aShaderType)
	{
		case DX11Shader::ShaderType::Vertex:
			shaderVersion = "vs_5_0";
			break;
		case DX11Shader::ShaderType::Pixel:
			shaderVersion = "ps_5_0";
			break;
	}
	ID3D10Blob* pErrorMsg = nullptr;
	DWORD dwShaderFlags = D3DCOMPILE_SKIP_OPTIMIZATION | D3DCOMPILE_DEBUG;
	HRESULT result = D3DCompileFromFile(Utils::ConvertCharArrayToLPCWSTR(aPath), nullptr, nullptr, aEntryPoint, shaderVersion, dwShaderFlags, 0, &aDataBlob, &pErrorMsg);
	if (FAILED(result))
	{
		if (pErrorMsg)
		{
			_RPT1(_CRT_WARN, "compilation error : %s\n", (char*)pErrorMsg->GetBufferPointer());
			SAFE_RELEASE(pErrorMsg);
		}
		ERROR_PRINT("%s", "Failed to compile VertexShader.");
	}
}
