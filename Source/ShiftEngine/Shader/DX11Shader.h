#pragma once

struct ID3D11VertexShader;
struct ID3D11PixelShader;
struct ID3D11InputLayout;
struct ID3D10Blob;
struct ID3D11Device;
struct ID3D11DeviceContext;
namespace Shift3D
{
	enum eInputLayoutPart
	{
		ePosition = 1 << 0,
		ePosition2D = 1 << 1,
		eNormal = 1 << 2,
		eBinormal = 1 << 3,
		eTangent = 1 << 4,
		eUV = 1 << 5
	};

	class DX11Shader
	{
	public:
		DX11Shader();
		~DX11Shader();

		void SetVShader(const char* aVertexShaderPath, const char* aEntryPoint, const eInputLayoutPart aInputLayout);
		void SetPShader(const char* aPixelShaderPath, const char* aEntryPoint);

		void BindVShader();
		void BindPShader();
		void BindGShader();
		void BindCShader();
		void BindCommon();
		void BindAll();

	private:
		enum class ShaderType
		{
			Vertex,
			Pixel,
			Geometry,
			Compute,
		};

		void CompileShader(const char* aPath, const char* aEntryPoint, ID3D10Blob*& aDataBlob, const ShaderType aShaderType);

		ID3D11PixelShader* myPixelShader;

		ID3D11VertexShader* myVertexShader;
		ID3D11InputLayout* myInputLayout;

		ID3D11Device* myDevice;
		ID3D11DeviceContext* myDeviceContext;
	};
}

