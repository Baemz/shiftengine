#include "stdafx.h"
#include "Transform.h"

using namespace Shift3D;

Transform::Transform()
{
	myRotation = Quaternion();
	myScale = { 1.f, 1.f, 1.f };
}


Transform::~Transform()
{
}

void Transform::SetLocalPosition(const Vector3f &aPosition)
{
	myLocalPosition = aPosition;
}

void Transform::SetWorldPosition(const Vector3f &aPosition)
{
	if (GetParent().expired() == false)
	{
		myLocalPosition = aPosition - GetParent().lock()->GetWorldPosition();
	}
	else
	{
		myLocalPosition = aPosition;
	}
}

void Transform::SetWorldRotation(const Vector3f &aRotation)
{
	Quaternion quaternion;
	quaternion.RotateWorld(aRotation);
	myRotation = quaternion;
}

void Transform::SetWorldRotation(const Quaternion &aRotation)
{
	Matrix44f matrix = GetWorldMatrix();

	matrix *= aRotation.GenerateMatrix();

	SetTransformationMatrix(matrix);
}

void Transform::SetLocalRotation(const Vector3f &aRotation)
{
	myRotation = Quaternion();
	myRotation.RotateLocal(aRotation);
}

void Transform::SetLocalRotation(const Quaternion &aRotation)
{
	myRotation = aRotation;
}

void Transform::WorldRotate(const Vector3f &aRotation)
{
	Quaternion rotation;
	rotation.RotateWorld(aRotation);

	WorldRotate(rotation);
}

void Transform::WorldRotate(const Quaternion &aRotation)
{
	myRotation *= aRotation;
}

void Transform::LocalRotate(const Vector3f &aRotation)
{
	Quaternion rotation;
	rotation.RotateLocal(aRotation);
	LocalRotate(rotation);
}

void Transform::LocalRotate(const Quaternion &aRotation)
{
	myRotation *= aRotation;
}

void Transform::SetLocalScale(const Vector3f &aScale)
{
	myScale = aScale;
}

void Transform::SetLocalScale(const float aScale)
{
	SetLocalScale(Vector3f::One * aScale);
}

void Transform::SetWorldScale(const Vector3f &aScale)
{
	myScale = -GetWorldScale() + aScale;
}

void Transform::SetWorldScale(const float aScale)
{
	SetWorldScale(Vector3f::One * aScale);
}

const Vector3f & Transform::GetLocalPosition() const
{
	return myLocalPosition;
}

const Vector3f Transform::GetWorldPosition() const
{
	if (myParent.expired() == false)
	{
		//Vector4f position = Vector4f(myLocalPosition.x, myLocalPosition.y, myLocalPosition.z, 1.0f);
		//Vector4f newPosition = position * GetParent().lock()->GetWorldMatrix();
		return GetWorldMatrix().GetPosition();

		//return Vector3f(newPosition.x, newPosition.y, newPosition.z);
	}
	return GetLocalPosition();
}

void Transform::MoveWithMyRotation(const Vector3f &aMoveVector)
{
	Vector4f vector = Vector4f(aMoveVector.x, aMoveVector.y, aMoveVector.z, 1.0f);
	vector = vector * GetLocalRotation().GenerateMatrix();
	Vector3f finalVector = Vector3f(vector.x, vector.y, vector.z);
	SetLocalPosition(myLocalPosition + finalVector);
}

const Quaternion & Transform::GetLocalRotation() const
{
	return myRotation;
}

const Quaternion Transform::GetWorldRotation() const
{
	if (GetParent().expired() == false)
	{
		return GetParent().lock()->GetWorldRotation() * GetLocalRotation();
	}
	return GetLocalRotation();
}

const Vector3f & Transform::GetLocalScale() const
{
	return myScale;
}

Vector3f Transform::GetWorldScale() const
{
	if (GetParent().expired() == false)
	{
		return GetParent().lock()->GetWorldScale() * GetLocalScale();
	}
	return GetLocalScale();
}

const Matrix44f Transform::GetWorldMatrix() const
{
	if (GetParent().expired() == false)
	{
		return GetLocalMatrix() * GetParent().lock()->GetWorldMatrix();
	}
	return GetLocalMatrix();
}

inline const Matrix44f Transform::GetLocalMatrix() const
{
	Matrix44f matrix = Matrix44f::Identity;
	matrix *= myRotation.GenerateMatrix();
	matrix *= Matrix44f::CreateScale(myScale.x, myScale.y, myScale.z);
	matrix.SetPosition(myLocalPosition);
	return matrix;
}

std::weak_ptr<Transform> Transform::GetParent() const
{
	return myParent;
}

void Transform::AddChild(std::shared_ptr<Transform> aChild, std::shared_ptr<Transform> aThis)
{
	myChildren.push_back(std::weak_ptr<Transform>(aChild));
	aChild->SetParent(aThis);
}

void Transform::SetTransformationMatrix(const Matrix44f &aMatrix)
{
	Quaternion quaternion;
	quaternion.RotateWorld(aMatrix.GetRotationVector());
	myScale = aMatrix.GetScale();
	myLocalPosition = aMatrix.GetPosition();

}

void Transform::RemoveAllChilds()
{
	myChildren.clear();
}

void Transform::WriteDebugInfo() const
{
}

Quaternion & Transform::GetRotationQuaternion()
{
	return myRotation;
}

void Transform::SetParent(std::shared_ptr<Transform> aParent)
{
	myParent = std::weak_ptr<Transform>(aParent);
}
