#pragma once
#include "../Math/Vector3.h"
#include "../Math/Matrix.h"
#include "../Math/Quaternion.h"
#include <memory>

namespace Shift3D
{
	class Transform
	{
	public:
		Transform();
		~Transform();

		/*
		Local position.
		*/
		void SetLocalPosition(const Vector3f& aPosition);

		/*
		Set position in world.
		*/
		void SetWorldPosition(const Vector3f& aPosition);

		/*
		Will rotate around world.
		(Will change position)
		*/
		void SetWorldRotation(const Vector3f& aRotation);

		/*
		Will rotate around world.
		(Will change position)
		*/
		void SetWorldRotation(const Quaternion& aRotation);

		/*
		Will rotate around itself.
		(Will not change position)
		*/
		void SetLocalRotation(const Vector3f &aRotation);

		/*
		Will rotate around itself.
		(Will not change position)
		*/
		void SetLocalRotation(const Quaternion &aRotation);

		/*
		Will rotate around world axes.
		*/
		void WorldRotate(const Vector3f &aRotation);

		/*
		Will rotate around world axes.
		*/
		void WorldRotate(const Quaternion &aRotation);

		/*
		Will rotate around itself.
		(Will not change position)
		*/
		void LocalRotate(const Vector3f &aRotation);

		/*
		Will rotate around itself.
		(Will not change position)
		*/
		void LocalRotate(const Quaternion &aRotation);

		/*
		Set local scale.
		*/
		void SetLocalScale(const Vector3f &aScale);

		/*
		Set world scale
		*/
		void SetWorldScale(const Vector3f &aScale);

		/*
		Set local scale.
		*/
		void SetLocalScale(const float aScale);

		/*
		Set world scale
		*/
		void SetWorldScale(const float aScale);

		/*
		Will get local position.
		*/
		const Vector3f &GetLocalPosition() const;

		/*
		Will get world position.
		*/
		const Vector3f GetWorldPosition() const;

		void MoveWithMyRotation(const Vector3f &aMoveVector);

		/*
		Will get local rotation.
		*/
		const Quaternion &GetLocalRotation() const;

		/*
		Will get world rotation.
		*/
		const Quaternion GetWorldRotation() const;

		/*
		Will return local scale.
		*/
		const Vector3f &GetLocalScale() const;

		/*
		Will return scale in world.
		*/
		Vector3f GetWorldScale() const;

		/*
		Will get matrix in world.
		*/
		const Matrix44f GetWorldMatrix() const;

		/*
		Will get translation rotation and scale.
		*/
		const Matrix44f GetLocalMatrix() const;

		/*
		Will return parent transform.
		*/
		std::weak_ptr<Transform> GetParent() const;

		void AddChild(std::shared_ptr<Transform> aChild, std::shared_ptr<Transform> aThis);

		/*
		Will set all values in the transform
		*/
		void SetTransformationMatrix(const Matrix44f &aMatrix);

		void RemoveAllChilds();

		void WriteDebugInfo() const;

		Quaternion &GetRotationQuaternion();
	private:
		void SetParent(std::shared_ptr<Transform>aChild);

		Quaternion myRotation;
		Vector3f myLocalPosition;
		Vector3f myScale;

		std::weak_ptr<Transform> myParent;
		std::vector<std::weak_ptr<Transform>> myChildren;
	};
}

