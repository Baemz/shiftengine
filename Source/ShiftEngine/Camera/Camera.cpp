#include "stdafx.h"
#include "Camera.h"

using namespace Shift3D;

Matrix44f CCamera::ourWorldMatrix = Matrix44f::Identity;

CCamera::CCamera(const float aNearZ,
	const float aFarZ,
	const float aFovAngle,
	const float aProjectionWidth,
	const float aProjectionHeight,
	const Vector3f &aPosition)
{
	Init(aNearZ, aFarZ, aFovAngle, aProjectionWidth, aProjectionHeight);
	SetPosition(aPosition);
}


CCamera::~CCamera()
{
}

void Shift3D::CCamera::Init(const float aNearZ, const float aFarZ, const float aFovAngle, const float aProjectionWidth, const float aProjectionHeight)
{
	myProjection = Matrix44f::Identity;
	ourWorldMatrix = Matrix44f::Identity;
	myProjection = myProjection.CreateProjectionMatrix(aNearZ, aFarZ, aProjectionWidth, aProjectionHeight, aFovAngle);
}

const Shift3D::Matrix44f Shift3D::CCamera::GetViewInverse() const
{
	Matrix44f inverse(myTransform.GetLocalMatrix());

	Vector4f translation = inverse.GetTranslation();
	Vector4f tempVector4 = Vector4f(0, 0, 0, 1.f);
	inverse.SetTranslation(tempVector4);
	translation *= -1.f;
	translation.w = 1.f;
	inverse = Matrix44f::Transpose(inverse);
	translation = translation * inverse;

	inverse.SetTranslation(translation);
	return inverse;
}

const Matrix44f Shift3D::CCamera::GetTransformation() const
{
	return myTransform.GetLocalMatrix();
}

void Shift3D::CCamera::SetTransformationMatrix(const Matrix44f& aMatrix)
{
	myTransform.SetLocalRotation(Quaternion(aMatrix));
}

Transform& Shift3D::CCamera::GetTransform()
{
	return myTransform;
}

const Matrix44f& Shift3D::CCamera::GetProjection() const
{
	return myProjection;
}

const Matrix44f& Shift3D::CCamera::GetWorldMatrix()
{
	return ourWorldMatrix;
}

void Shift3D::CCamera::RotateCamera(const Vector3f& aRotation)
{
	myTransform.WorldRotate(aRotation);
}

const Vector4f Shift3D::CCamera::GetRight() const
{
	return Vector4f(myTransform.GetLocalRotation().GetRight());
}

const Vector4f Shift3D::CCamera::GetLeft() const
{
	return -GetRight();
}

const Vector4f Shift3D::CCamera::GetForward() const
{
	return Vector4f(myTransform.GetLocalRotation().GetForward());
}

const Vector4f Shift3D::CCamera::GetBack() const
{
	return -(GetForward());
}


const Vector4f Shift3D::CCamera::GetUp() const
{
	return Vector4f(myTransform.GetLocalRotation().GetUp());
}

const Vector4f Shift3D::CCamera::GetDown() const
{
	return -(GetUp());
}

void Shift3D::CCamera::MoveLeft(const float aScalar)
{
	myTransform.SetWorldPosition(myTransform.GetWorldPosition() + Vector3f(GetLeft()) * aScalar);
}

void Shift3D::CCamera::MoveRight(const float aScalar)
{
	myTransform.SetWorldPosition(myTransform.GetWorldPosition() + Vector3f(GetRight()) * aScalar);
}

void Shift3D::CCamera::MoveForward(const float aScalar)
{
	myTransform.SetWorldPosition(myTransform.GetWorldPosition() + Vector3f(GetForward()) * aScalar);
}

void Shift3D::CCamera::MoveBack(const float aScalar)
{
	myTransform.SetWorldPosition(myTransform.GetWorldPosition() + Vector3f(GetBack()) * aScalar);
}

void Shift3D::CCamera::MoveUp(const float aScalar)
{
	myTransform.SetWorldPosition(myTransform.GetWorldPosition() + Vector3f(GetUp() * aScalar));
}

void Shift3D::CCamera::MoveDown(const float aScalar)
{
	myTransform.SetWorldPosition(myTransform.GetWorldPosition() + Vector3f(GetDown() * aScalar));
}

void Shift3D::CCamera::SetPosition(const Vector3f & aPosition)
{
	myTransform.SetWorldPosition(aPosition);
}

const Vector3f Shift3D::CCamera::GetPosition() const
{
	return myTransform.GetWorldPosition();
}

void Shift3D::CCamera::SetRotation(const Quaternion& aRotation)
{
	myTransform.SetLocalRotation(aRotation);
}
