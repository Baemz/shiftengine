#pragma once
#include "Transform.h"

namespace Shift3D
{
	class CCamera
	{
	public:
		CCamera(const float aNearZ,
			const float aFarZ,
			const float aFovAngle,
			const float aProjectionWidth,
			const float aProjectionHeight,
			const Vector3f& aPosition);
		~CCamera();

		void Init(const float aNearZ,
			const float aFarZ,
			const float aFovAngle,
			const float aProjectionWidth,
			const float aProjectionHeight);
		
		const Matrix44f GetViewInverse() const;
		const Matrix44f GetTransformation() const;
		void SetTransformationMatrix(const Matrix44f& aMatrix);
		Transform& GetTransform();

		const Matrix44f& GetProjection() const;
		static const Matrix44f& GetWorldMatrix();

		void RotateCamera(const Vector3f& aRotation);

		const Vector4f GetRight() const;
		const Vector4f GetLeft() const;
		const Vector4f GetForward() const;
		const Vector4f GetBack() const;
		const Vector4f GetUp() const;
		const Vector4f GetDown() const;

 		void MoveLeft(const float aScalar);
		void MoveRight(const float aScalar);
		void MoveForward(const float aScalar);
		void MoveBack(const float aScalar);
		void MoveUp(const float aScalar);
		void MoveDown(const float aScalar);
		void SetPosition(const Vector3f& aPosition);
		const Vector3f GetPosition() const;
		void SetRotation(const Quaternion& aRotation);

	private:
		Transform myTransform;
		Matrix44f myProjection;
		Matrix44f myRotationMatrix;
		Vector3f myRotation;
		static Matrix44f ourWorldMatrix;
	};
}
