#pragma once

//STL
#include <windows.h>
#include <functional>

// ShiftEngine
#include "Math/Vector2.h"
#include "Utilities/Timer.h"
#include "Utilities/EngineStartParams.h"

struct ID3D11DeviceContext;
namespace Shift3D
{
	class WindowsWindow;
	class CErrorHandler;
	class CGraphicsInterface;
	namespace Physics
	{
		class CPhysXEngine;
	}
}

namespace Shift3D
{
	using callback_function = std::function<void()>;
	using callback_function_update = std::function<void(const float)>;
	using callback_function_debugRender = std::function<void()>;
	using callback_function_wndProc = std::function<LRESULT(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)>;
	using callback_function_log = std::function<void(std::string)>;
	using callback_function_error = std::function<void(std::string)>;

	struct EngineCreateParameters
	{
		EngineCreateParameters()
		{
			myHwnd = nullptr;
			myHInstance = nullptr;
			myWindowConfig.myWindowWidth = 800;
			myWindowConfig.myWindowHeight = 600;
			myWindowConfig.myEnableVSync = false;
			myWindowConfig.myRenderWidth = myWindowConfig.myWindowWidth;
			myWindowConfig.myRenderHeight = myWindowConfig.myWindowHeight;
			myErrorFunction = nullptr;
			myWindowConfig.myStartInFullScreen = false;
			myWindowConfig.myWindowSetting = EWindowSetting::EWindowSetting_Overlapped;
			myGraphicsAPI = EGraphicsAPI::eDirectX11;
			myWindowConfig.myTargetHeight = 0;
			myWindowConfig.myTargetWidth = 0;
		}

		callback_function myInitFunctionToCall;
		callback_function_update myUpdateFunctionToCall;
		callback_function_debugRender myDebugRenderFunction;
		callback_function_wndProc myWinProcCallback;
		callback_function_log myLogFunction;
		callback_function_error myErrorFunction;

		HWND *myHwnd;
		HINSTANCE myHInstance;
		EGraphicsAPI myGraphicsAPI;
		SWindowConfig myWindowConfig;
		std::wstring myApplicationName;
	};

	class CEngine
	{
		friend class CDirect3D12;
		friend class CGraphicsInterface;
		friend class CErrorHandler;

	public:
		CEngine &operator =(const CEngine &anOther) = delete;
		static void Create(const EngineCreateParameters& aCreateParameters);
		static void Destroy();
		static CEngine* GetInstance();

		bool Start();
		void Shutdown();

		void StartStep();
		void ExecuteStep();

		CErrorHandler& GetErrorHandler() const { return *myErrorHandler; }

		ID3D11DeviceContext* GetDeviceContext();

		Physics::CPhysXEngine* GetPhysXEngine() const;

		HINSTANCE GetHINSTANCE() const { return myHInstance; }
		HWND GetHWND() const;
		const std::wstring& GetAppName() const { return myCreateParameters.myApplicationName; }

	private:
		CEngine(const EngineCreateParameters& aCreateParameters);
		~CEngine();
		static CEngine* ourEngine;

		//Engine Members
		bool myRunEngine;
		float myFPSShowTimer;
		WindowsWindow* myWindow;
		CGraphicsInterface* myGraphicsInterface;
		CErrorHandler* myErrorHandler;
		Physics::CPhysXEngine* myPhysXEngine;
		Timer myTimer;

		// Window settings
		HWND* myHwnd;
		HINSTANCE myHInstance;
		Vector2ui myWindowSize;
		Vector2ui myRenderSize;
		Vector2ui myTargetSize;
		EngineCreateParameters myCreateParameters;

		// Callbacks
		callback_function_update myUpdateFunctionToCall;
		callback_function_debugRender myDebugRenderFunction;
		callback_function myInitFunctionToCall;
	};
}
