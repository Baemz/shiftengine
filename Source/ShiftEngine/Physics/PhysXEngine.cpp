#include "stdafx.h"
#include "PhysXEngine.h"

#include <ThirdParty/PhysX/pxphysicsapi.h>
#include <ThirdParty/PhysX/extensions/pxdefaultallocator.h>

#ifdef _DEBUG
#pragma comment(lib, "PhysX3DEBUG_x64.lib")
#pragma comment(lib, "PhysX3ExtensionsDEBUG.lib")
#else
#pragma comment(lib, "PhysX3_x64.lib")
#pragma comment(lib, "PhysX3Extensions.lib")
#endif



Shift3D::Physics::CPhysXEngine::CPhysXEngine()
	: myPhysX(nullptr)
	, myPxFoundation(nullptr)
	, myPvd(nullptr)
	, myCooking(nullptr)
	, myScene(nullptr)
	, myDefaultAllocatorCallback(nullptr)
{
}


Shift3D::Physics::CPhysXEngine::~CPhysXEngine()
{
	myPhysX->release();
	myPxFoundation->release();
	myPvd->release();
	myCooking->release();
	myScene->release();
	SAFE_DELETE(myDefaultAllocatorCallback);
}

bool Shift3D::Physics::CPhysXEngine::Init()
{
	myDefaultAllocatorCallback = new physx::PxDefaultAllocator();
	INFO_PRINT("%s", "Initiating PhysX...");

	// Setup Foundation
	INFO_PRINT("%s", "		Creating foundation.");
	myPxFoundation = PxCreateFoundation(PX_FOUNDATION_VERSION, *myDefaultAllocatorCallback, myDefaultErrorCallback);
	if (myPxFoundation == nullptr)
	{
		ERROR_PRINT("%s", "Couldn't create PxFoundation.");
		return false;
	}

	bool recordMemoryAllocations = true;

	// Setup Physics object
	INFO_PRINT("%s", "		Creating PhysX-device.");
	myPhysX = PxCreatePhysics(PX_PHYSICS_VERSION, *myPxFoundation, physx::PxTolerancesScale(), recordMemoryAllocations, myPvd);
	if (myPhysX == nullptr)
	{
		ERROR_PRINT("%s", "Couldn't create PxPhysics.");
		return false;
	}

	// Setup Cooking
	INFO_PRINT("%s", "		Cooking some data...");
	physx::PxTolerancesScale scale;
	scale.length = 100;
	scale.speed = 981;
	myCooking = PxCreateCooking(PX_PHYSICS_VERSION, *myPxFoundation, physx::PxCookingParams(scale));
	if (myCooking == nullptr)
	{
		ERROR_PRINT("%s", "Couldn't create PxCooking.");
		return false;
	}

	// Init the extension-library
	INFO_PRINT("%s", "		Initing the extensions.");
	if (!PxInitExtensions(*myPhysX, myPvd))
	{
		ERROR_PRINT("%s", "Couldn't initiate Extensions.");
		return false;
	}

	// Setup the scene
	INFO_PRINT("%s", "		Creating scene.");
	physx::PxSceneDesc sceneDesc = physx::PxSceneDesc((physx::PxTolerancesScale()));
	sceneDesc.gravity = physx::PxVec3(0.0f, -9.8f, 0.0f);
	if (!sceneDesc.cpuDispatcher)
	{
		physx::PxDefaultCpuDispatcher* mCpuDispatcher = physx::PxDefaultCpuDispatcherCreate(4);
		if (!mCpuDispatcher)
		{
			ERROR_PRINT("%s","		PxDefaultCpuDispatcherCreate failed!");
		}
		sceneDesc.cpuDispatcher = mCpuDispatcher;
	}
	if (!sceneDesc.filterShader)
	{
		sceneDesc.filterShader = &physx::PxDefaultSimulationFilterShader;
	}
	myScene = myPhysX->createScene(sceneDesc);

	INFO_PRINT("%s", "PhysX successfully initialized...");
	return true;
}

void Shift3D::Physics::CPhysXEngine::Simulate(const float aDeltaTime)
{
	myScene->simulate(physx::PxReal(aDeltaTime));
}

void Shift3D::Physics::CPhysXEngine::FetchResults()
{
	myScene->fetchResults(true);
}
