#pragma once

#include <ThirdParty/PhysX/extensions/pxdefaulterrorcallback.h>

namespace physx
{
	class PxPhysics;
	class PxPvd;
	class PxFoundation;
	class PxCooking;
	class PxScene;
	class PxDefaultAllocator;
}
namespace Shift3D { namespace Physics {

	class PhysXErrorCallback : public physx::PxErrorCallback
	{
		void reportError(physx::PxErrorCode::Enum code, const char* message, const char* file,
			int line) override
		{
			code; // Use this for more accurate error-message??
			ERROR_PRINT("%s - %s - %d", message, file, line);
		}
	};

	class CPhysXEngine
	{
	public:
		CPhysXEngine();
		~CPhysXEngine();

		bool Init();
		void Simulate(const float aDeltaTime);
		void FetchResults();

	private:
		physx::PxPhysics* myPhysX;
		physx::PxPvd* myPvd;
		physx::PxFoundation* myPxFoundation;
		physx::PxCooking* myCooking;
		physx::PxScene* myScene;
		PhysXErrorCallback myDefaultErrorCallback;
		physx::PxDefaultAllocator* myDefaultAllocatorCallback;
	};
}}
