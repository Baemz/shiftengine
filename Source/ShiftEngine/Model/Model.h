#pragma once
namespace FBXLoader
{
	struct SMeshData;
}

namespace Shift3D
{
	class DX11VertexBuffer;
	class DX11IndexBuffer;
	class DX11ConstantBuffer;
	class CMaterial;
	struct SMeshData;
	class CModel
	{
	public:
		CModel();
		CModel(const char* aModelPath);
		~CModel();

		void LoadModel(const char* aModelPath);
		void Render(const Matrix44f& aWorldPosition, const Matrix44f& aViewMatrix, const Matrix44f& aProjectionMatrix, const Vector3f& aCameraPosition);

		void SetLightData(const Vector3f& aDirection, const Vector4f& aColor);


		// Warning! This sets the material for ALL model-instances
		void SetMaterial(CMaterial* aMaterial);

	protected:
		DX11VertexBuffer* myVertexBuffer;
		DX11IndexBuffer* myIndexBuffer;
		DX11ConstantBuffer* myMatrixBuffer;
		DX11ConstantBuffer* myLightBuffer;
		DX11ConstantBuffer* myBoolsBuffer;
		CMaterial* mySurfaceMaterial;
		std::vector<CModel*> myChildren;

		void LoadMeshData(FBXLoader::SMeshData& aData);
		void CheckForNormalMap();
		void NullMaterial();
	};
}

