#pragma once
namespace Shift3D
{
	class DX11Shader;
	class CModel;
	class CMaterial;
	class CModelInstance
	{
	public:
		CModelInstance();
		CModelInstance(const char* aModelPath);
		~CModelInstance();

		void Init(const char* aModelPath);

		void Render(const Matrix44f& aViewMatrix, const Matrix44f& aProjectionMatrix, const Vector3f& aCameraPosition);

		void SetPosition(const Vector3f& aPosition);
		void SetScale(const Vector3f& aScale);
		const Vector3f GetScale() const;
		void SetLightData(const Vector3f& aDirection, const Vector4f& aColor);

		// Warning! This sets the material for ALL model-instances
		void SetMaterial(CMaterial* aMaterial);


	private:
		DX11Shader* myShaders;
		CModel* myModel;
		Matrix44f myWorldTransform;
		
	};
}
