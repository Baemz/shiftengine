#include "stdafx.h"
#include "Model.h"
#include <Platform/Buffers/DX11VertexBuffer.h>
#include <Platform/Buffers/DX11IndexBuffer.h>
#include <Platform/Buffers/DX11ConstantBuffer.h>
#include <Texture/TextureInstance.h>
#include <Model/Material/Material.h>
#include <Camera/Camera.h>
#include <Shader/DX11Shader.h>
#include <Engine.h>
#include <d3d11.h>

//-----------------------//
#include "FBX/FBXLoader.h"
#include "ThirdParty/FBX/FBXLoaderA.h"
//-----------------------//

using namespace Shift3D;

CModel::CModel()
	: myIndexBuffer(nullptr)
	, myVertexBuffer(nullptr)
	, myMatrixBuffer(nullptr)
	, mySurfaceMaterial(nullptr)
{
}

Shift3D::CModel::CModel(const char * aModelPath)
	: CModel()
{
	LoadModel(aModelPath);
}


CModel::~CModel()
{
	SAFE_DELETE(myVertexBuffer);
	SAFE_DELETE(myIndexBuffer);
	SAFE_DELETE(myMatrixBuffer);
	SAFE_DELETE(mySurfaceMaterial);
}

void CModel::LoadModel(const char* aModelPath)
{
	std::vector<FBXLoader::SMeshData> data;
	CRootManager::GetFBXLoaderA()->LoadModel(aModelPath, data);
	LoadMeshData(data[0]);
	
	for (size_t index = 1; index < data.size(); ++index)
	{
		CModel* child = new CModel();
		child->LoadMeshData(data[index]);
		myChildren.push_back(child);
	}
}

void Shift3D::CModel::Render(const Matrix44f& aWorldPosition, const Matrix44f& aViewMatrix, const Matrix44f& aProjectionMatrix, const Vector3f& aCameraPosition)
{
	for (size_t i = 0; i < myChildren.size(); ++i)
	{
		myChildren[i]->Render(aWorldPosition, aViewMatrix, aProjectionMatrix, aCameraPosition);
	}

	auto* deviceContext = CEngine::GetInstance()->GetDeviceContext();

	// MATRICES
	SMatrixBuffer matrices;
	matrices.modelTransform = Matrix44f::Transpose(aWorldPosition);
	matrices.viewMatrix = Matrix44f::Transpose(aViewMatrix);
	matrices.projectionMatrix = Matrix44f::Transpose(aProjectionMatrix);
	matrices.cameraPosition = aCameraPosition;
	myMatrixBuffer->SetData<SMatrixBuffer>(matrices);
	myMatrixBuffer->UpdateBuffer();
	myMatrixBuffer->BindVS();

	myLightBuffer->BindPS();
	myBoolsBuffer->BindPS();

	mySurfaceMaterial->Bind();

	myVertexBuffer->Bind();
	myIndexBuffer->Bind();

	deviceContext->DrawIndexed(static_cast<UINT>(myIndexBuffer->GetIndexCount()), 0, 0);
	mySurfaceMaterial->Unbind();
}

void Shift3D::CModel::SetLightData(const Vector3f& aDirection, const Vector4f& aColor)
{
	SDirLightBuffer light;
	light.direction = aDirection;
	light.diffuseColor = aColor;
	myLightBuffer->SetData(light);
	myLightBuffer->UpdateBuffer();

	for (size_t i = 0; i < myChildren.size(); ++i)
	{
		myChildren[i]->SetLightData(aDirection, aColor);
	}
}

void Shift3D::CModel::SetMaterial(CMaterial* aMaterial)
{
	if (mySurfaceMaterial != nullptr)
	{
		SAFE_DELETE(mySurfaceMaterial); 
		for (size_t i = 0; i < myChildren.size(); ++i)
		{
			myChildren[i]->NullMaterial();
		}
	}
	mySurfaceMaterial = aMaterial;
	CheckForNormalMap();

	for (size_t i = 0; i < myChildren.size(); ++i)
	{
		myChildren[i]->SetMaterial(aMaterial);
	}
}

void Shift3D::CModel::LoadMeshData(FBXLoader::SMeshData& aData)
{
	myVertexBuffer = new DX11VertexBuffer(aData.myVertices, aData.myVertexCount, aData.myVertexBufferSize, true);
	myIndexBuffer = new DX11IndexBuffer(aData.myIndices);
	myMatrixBuffer = new DX11ConstantBuffer(0);
	myMatrixBuffer->AddDataMember<SMatrixBuffer>();
	myMatrixBuffer->Init();

	myLightBuffer = new DX11ConstantBuffer(0);
	myLightBuffer->AddDataMember<SDirLightBuffer>();
	myLightBuffer->Init();

	myBoolsBuffer = new DX11ConstantBuffer(1);
	myBoolsBuffer->AddDataMember<SBoolsBuffer>();
	myBoolsBuffer->Init();

	mySurfaceMaterial = new CMaterial();

	//////
	mySurfaceMaterial->SetDiffuse(aData.myTextures[0].c_str());
	mySurfaceMaterial->SetRoughness(aData.myTextures[1].c_str());
	mySurfaceMaterial->SetNormalMap(aData.myTextures[5].c_str());
	mySurfaceMaterial->SetMetalness(aData.myTextures[10].c_str());
	//////
	CheckForNormalMap();
}

void Shift3D::CModel::CheckForNormalMap()
{
	if (mySurfaceMaterial != nullptr)
	{
		SBoolsBuffer bools;
		bools.useNormalMap = (mySurfaceMaterial->IsUsingNormalMap()) ? 1.f : 0.f;
		bools.padding0 = 0.f;
		myBoolsBuffer->SetData(bools);
		myBoolsBuffer->UpdateBuffer();
	}
}

void Shift3D::CModel::NullMaterial()
{
	for (size_t i = 0; i < myChildren.size(); ++i)
	{
		myChildren[i]->NullMaterial();
	}
	mySurfaceMaterial = nullptr;
}
