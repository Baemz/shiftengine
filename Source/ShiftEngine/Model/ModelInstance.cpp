#include "stdafx.h"
#include "ModelInstance.h"
#include "Model.h"
#include "ModelManager.h"
#include <Shader/DX11Shader.h>

using namespace Shift3D;

CModelInstance::CModelInstance()
	: myModel(nullptr)
	, myShaders(nullptr)
{
	myWorldTransform = Matrix44f::Identity;
}

CModelInstance::CModelInstance(const char* aModelPath)
	: CModelInstance()
{
	Init(aModelPath);
}


CModelInstance::~CModelInstance()
{
	SAFE_DELETE(myShaders);
}

void Shift3D::CModelInstance::Init(const char* aModelPath)
{
	myShaders = new DX11Shader();
	myShaders->SetVShader("Data/Shaders/CommonSurfaceVShader.hlsl", "main", eInputLayoutPart(eInputLayoutPart::ePosition | eInputLayoutPart::eUV | eInputLayoutPart::eNormal | eInputLayoutPart::eBinormal | eInputLayoutPart::eTangent));
	myShaders->SetPShader("Data/Shaders/CommonSurfacePShader.hlsl", "main");
	myModel = CRootManager::GetModelManager()->GetModel(aModelPath);
}

void Shift3D::CModelInstance::Render(const Matrix44f& aViewMatrix, const Matrix44f& aProjectionMatrix, const Vector3f& aCameraPosition)
{
	if (myModel)
	{
		myShaders->BindCommon();
		myModel->Render(myWorldTransform, aViewMatrix, aProjectionMatrix, aCameraPosition);
	}
}

void Shift3D::CModelInstance::SetPosition(const Vector3f& aPosition)
{
	myWorldTransform.SetPosition(aPosition);
}

void Shift3D::CModelInstance::SetScale(const Vector3f& aScale)
{
	myWorldTransform *= Matrix44f::CreateScale(aScale.x, aScale.y, aScale.z);
}

const Vector3f Shift3D::CModelInstance::GetScale() const
{
	return myWorldTransform.GetScale();
}

void Shift3D::CModelInstance::SetLightData(const Vector3f& aDirection, const Vector4f& aColor)
{
	if (myModel)
	{
		myModel->SetLightData(aDirection, aColor);
	}
}

void Shift3D::CModelInstance::SetMaterial(CMaterial* aMaterial)
{
	myModel->SetMaterial(aMaterial);
}
