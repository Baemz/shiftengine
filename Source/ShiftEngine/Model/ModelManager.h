#pragma once

namespace Shift3D
{
	class CModel;
	class CModelManager
	{
	public:
		CModelManager();
		~CModelManager();

		CModel* GetModel(const char* aModelPath);

	private:
		void LoadModel(const std::string& aModelPath);

		unsigned int myLoadedModelCount;
		std::map<std::string, CModel*> myLoadedModels;
	};
}
