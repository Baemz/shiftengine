#include "stdafx.h"
#include "Material.h"
#include "Texture/TextureInstance.h"

using namespace Shift3D;

Shift3D::CMaterial::CMaterial()
{
	for (size_t i = 0; i < myTextures.size(); ++i)
	{
		myTextures[i] = nullptr;
	}
	myTextures[myTextures.size() - 1] = new CTextureInstance("PreintegratedFG.dds", eTextureType::PreintegratedFG);
}


Shift3D::CMaterial::~CMaterial()
{
	for (size_t i = 0; i < myTextures.size(); ++i)
	{
		delete myTextures[i];
		myTextures[i] = nullptr;
	}
}

void Shift3D::CMaterial::Bind()
{
	for (size_t i = 0; i < myTextures.size(); ++i)
	{
		if (myTextures[i])
		{
			myTextures[i]->Bind();
		}
	}
}

void Shift3D::CMaterial::Unbind()
{
	for (size_t i = 0; i < myTextures.size(); ++i)
	{
		if (myTextures[i])
		{
			myTextures[i]->Unbind();
		}
	}
}

void Shift3D::CMaterial::SetDiffuse(const char* aTexturePath)
{
	if (myTextures[eTexturePlacement::Diffuse] != nullptr)
	{
		SAFE_DELETE(myTextures[eTexturePlacement::Diffuse]);
	}
	if (aTexturePath == nullptr)
	{
		myTextures[eTexturePlacement::Diffuse] = nullptr;
	}
	else
	{
		myTextures[eTexturePlacement::Diffuse] = new CTextureInstance(aTexturePath, eTextureType::Diffuse);
	}

}

void Shift3D::CMaterial::SetRoughness(const char* aTexturePath)
{
	if (myTextures[eTexturePlacement::Roughness] != nullptr)
	{
		SAFE_DELETE(myTextures[eTexturePlacement::Roughness]);
	}

	if (aTexturePath == nullptr)
	{
		myTextures[eTexturePlacement::Roughness] = nullptr;
	}
	else
	{
		myTextures[eTexturePlacement::Roughness] = new CTextureInstance(aTexturePath, eTextureType::Roughness);
	}
}

void Shift3D::CMaterial::SetMetalness(const char* aTexturePath)
{
	if (myTextures[eTexturePlacement::Metalness] != nullptr)
	{
		SAFE_DELETE(myTextures[eTexturePlacement::Metalness]);
	}

	if (aTexturePath == nullptr)
	{
		myTextures[eTexturePlacement::Metalness] = nullptr;
	}
	else
	{
		myTextures[eTexturePlacement::Metalness] = new CTextureInstance(aTexturePath, eTextureType::Metalness);
	}
}

void Shift3D::CMaterial::SetEmissive(const char* aTexturePath)
{
	if (myTextures[eTexturePlacement::Emissive] != nullptr)
	{
		SAFE_DELETE(myTextures[eTexturePlacement::Emissive]);
	}

	if (aTexturePath == nullptr)
	{
		myTextures[eTexturePlacement::Emissive] = nullptr;
	}
	else
	{
		myTextures[eTexturePlacement::Emissive] = new CTextureInstance(aTexturePath, eTextureType::Emissive);
	}
}

void Shift3D::CMaterial::SetNormalMap(const char * aTexturePath)
{
	if (myTextures[eTexturePlacement::NormalMap] != nullptr)
	{
		SAFE_DELETE(myTextures[eTexturePlacement::NormalMap]);
	}

	if (aTexturePath == nullptr)
	{
		myTextures[eTexturePlacement::NormalMap] = nullptr;
	}
	else
	{
		myTextures[eTexturePlacement::NormalMap] = new CTextureInstance(aTexturePath, eTextureType::NormalMap);
	}
}

void Shift3D::CMaterial::SetAmbientOcclusion(const char* aTexturePath)
{
	if (myTextures[eTexturePlacement::AmbientOcclusion] != nullptr)
	{
		SAFE_DELETE(myTextures[eTexturePlacement::AmbientOcclusion]);
	}

	if (aTexturePath == nullptr)
	{
		myTextures[eTexturePlacement::AmbientOcclusion] = nullptr;
	}
	else
	{
		myTextures[eTexturePlacement::AmbientOcclusion] = new CTextureInstance(aTexturePath, eTextureType::Ambient);
	}
}

void Shift3D::CMaterial::SetEnvironmentMap(const char * aTexturePath)
{
	if (myTextures[eTexturePlacement::EnvironmentMap] != nullptr)
	{
		SAFE_DELETE(myTextures[eTexturePlacement::EnvironmentMap]);
	}

	if (aTexturePath == nullptr)
	{
		myTextures[eTexturePlacement::EnvironmentMap] = nullptr;
	}
	else
	{
		myTextures[eTexturePlacement::EnvironmentMap] = new CTextureInstance(aTexturePath, eTextureType::EnvironmentMap);
	}
}

const bool Shift3D::CMaterial::IsUsingNormalMap() const
{
	return (myTextures[eTexturePlacement::NormalMap] != nullptr);
}
