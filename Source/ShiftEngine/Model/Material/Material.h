#pragma once

namespace Shift3D
{
	class CTextureInstance;
	class CMaterial
	{
	public:
		CMaterial();
		~CMaterial();

		void Bind();
		void Unbind();

		// Diffuse - Albedo - Base Color
		void SetDiffuse(const char* aTexturePath);

		// Roughness - Specular
		void SetRoughness(const char* aTexturePath);

		// Metallic - Glossiness
		void SetMetalness(const char* aTexturePath);


		void SetEmissive(const char* aTexturePath);
		void SetNormalMap(const char* aTexturePath);
		void SetAmbientOcclusion(const char* aTexturePath);
		void SetEnvironmentMap(const char* aTexturePath);

		const bool IsUsingNormalMap() const;

		enum eTexturePlacement
		{
			Diffuse = 0,
			Roughness = 1,
			Metalness = 2,
			Emissive = 3,
			NormalMap = 4,
			AmbientOcclusion = 5,
			EnvironmentMap = 6
		};

	private:
		std::array<CTextureInstance*, 8> myTextures;
	};
}
