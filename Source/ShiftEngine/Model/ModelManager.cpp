#include "stdafx.h"
#include "ModelManager.h"
#include "Model.h"

using namespace Shift3D;

CModelManager::CModelManager()
	: myLoadedModelCount(0)
{
}


CModelManager::~CModelManager()
{
}

CModel* CModelManager::GetModel(const char* aModelPath)
{
	std::string path(aModelPath);

	if (myLoadedModels.find(path) == myLoadedModels.end())
	{
		LoadModel(path);
	}

	return myLoadedModels[path];
}

void CModelManager::LoadModel(const std::string& aModelPath)
{
	myLoadedModels.insert(std::pair<std::string, CModel*>(aModelPath, new CModel(aModelPath.c_str())));
	++myLoadedModelCount;
}
