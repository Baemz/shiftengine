#pragma once
#include "Math/Vector.h"
struct SVertex
{
	float x = 0.f, y = 0.f, z = 0.f, w = 0.f;
	float normalX = 0.f, normalY = 0.f, normalZ = 0.f, normalW = 0.f;
	float tangentX = 0.f, tangentY = 0.f, tangentZ = 0.f;
	float binormalX = 0.f, binormalY = 0.f, binormalZ = 0.f;
	float textU = 0.f, textV = 0.f;
};

struct SSimpleVertex
{
	Shift3D::Vector4f position;
	Shift3D::Vector2f textureUV;
};

struct SMatrixBuffer
{
	Shift3D::Matrix44f modelTransform;
	Shift3D::Matrix44f viewMatrix;
	Shift3D::Matrix44f projectionMatrix;
	Shift3D::Vector3f cameraPosition;
	float padding;
};

struct SDirLightBuffer
{
	Shift3D::Vector3f direction;
	Shift3D::Vector4f diffuseColor;
	float padding0;
};

struct SBoolsBuffer
{
	float useNormalMap = 0.f;
	float padding0;
	float padding1;
	float padding2;
};
