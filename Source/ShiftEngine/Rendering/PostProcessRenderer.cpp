#include "stdafx.h"
#include "PostProcessRenderer.h"
#include <Shader/DX11Shader.h>
#include <Platform/Buffers/DX11VertexBuffer.h>

using namespace Shift3D;

CPostProcessRenderer::CPostProcessRenderer()
	: myVertexBuffer(nullptr)
	, myVertexShader(nullptr)
	, myCopyShader(nullptr)
	, myGaussianBlurHorizontalShader(nullptr)
	, myGaussianBlurVerticalShader(nullptr)
	, myAddShader(nullptr)
	, myToneMapShader(nullptr)
	, myLuminanceShader(nullptr)
{
}

CPostProcessRenderer::~CPostProcessRenderer()
{
	SAFE_DELETE(myVertexBuffer);
	SAFE_DELETE(myVertexShader);
	SAFE_DELETE(myCopyShader);
	SAFE_DELETE(myGaussianBlurHorizontalShader);
	SAFE_DELETE(myGaussianBlurVerticalShader);
	SAFE_DELETE(myAddShader);
	SAFE_DELETE(myToneMapShader);
	SAFE_DELETE(myLuminanceShader);
}

void Shift3D::CPostProcessRenderer::Init()
{
	myVertexShader = new DX11Shader();
	myCopyShader = new DX11Shader();
	myGaussianBlurHorizontalShader = new DX11Shader();
	myGaussianBlurVerticalShader = new DX11Shader();
	myAddShader = new DX11Shader();
	myToneMapShader = new DX11Shader();
	myLuminanceShader = new DX11Shader();

	myVertexShader->SetVShader("Data/Shaders/PostProcessFX.hlsl", "BasicVertexShader", eInputLayoutPart(eInputLayoutPart::ePosition | eInputLayoutPart::eUV));
	myCopyShader->SetPShader("Data/Shaders/PostProcessFX.hlsl", "PShader_Copy");
	myGaussianBlurHorizontalShader->SetPShader("Data/Shaders/PostProcessFX.hlsl", "PShader_GaussianBlurHorizontal");
	myGaussianBlurVerticalShader->SetPShader("Data/Shaders/PostProcessFX.hlsl", "PShader_GaussianBlurVertical");
	myAddShader->SetPShader("Data/Shaders/PostProcessFX.hlsl", "PShader_Add");
	myToneMapShader->SetPShader("Data/Shaders/PostProcessFX.hlsl", "PShader_ToneMap");
	myLuminanceShader->SetPShader("Data/Shaders/PostProcessFX.hlsl", "PShader_Luminance");

	CreateVertexBuffer();
}

void Shift3D::CPostProcessRenderer::DoHDR()
{
}

void Shift3D::CPostProcessRenderer::CreateVertexBuffer()
{
	SSimpleVertex vertices[6];
	vertices[0].position = Vector4f(-1.0f, -1.0f, 0.5f, 1.0f);
	vertices[1].position = Vector4f(-1.0f, 1.0f, 0.5f, 1.0f);
	vertices[2].position = Vector4f(1.0f, -1.0f, 0.5f, 1.0f);
	vertices[3].position = Vector4f(1.0f, 1.0f, 0.5f, 1.0f);
	vertices[4].position = Vector4f(1.0f, -1.0f, 0.5f, 1.0f);
	vertices[5].position = Vector4f(-1.0f, 1.0f, 0.5f, 1.0f);

	vertices[0].textureUV = Vector2f(0.0f, 1.0f);
	vertices[2].textureUV = Vector2f(1.0f, 1.0f);
	vertices[4].textureUV = Vector2f(1.0f, 1.0f);
	vertices[1].textureUV = Vector2f(0.0f, 0.0f);
	vertices[3].textureUV = Vector2f(1.0f, 0.0f);
	vertices[5].textureUV = Vector2f(0.0f, 0.0f);

	myVertexBuffer = new DX11VertexBuffer(&vertices[0], 6, sizeof(SSimpleVertex), false);
}

void Shift3D::CPostProcessRenderer::Bloom()
{
}
