#pragma once

namespace Shift3D
{
	class DX11VertexBuffer;
	class DX11Shader;
	class CPostProcessRenderer
	{
	public:
		CPostProcessRenderer();
		~CPostProcessRenderer();

		void Init();
		void DoHDR();

	private:
		DX11VertexBuffer* myVertexBuffer;
		DX11Shader* myVertexShader;
		DX11Shader* myCopyShader;
		DX11Shader* myLuminanceShader;
		DX11Shader* myGaussianBlurHorizontalShader;
		DX11Shader* myGaussianBlurVerticalShader;
		DX11Shader* myAddShader;
		DX11Shader* myToneMapShader;

		void CreateVertexBuffer();

		void Bloom();
		void CopyTextures();
		void Luminate();
		void ToneMap();
		void DoGaussianBlur();
		void AddTextures();
	};
}