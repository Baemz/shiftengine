#pragma once
namespace Shift3D
{
	class CGraphicsInterface
	{
	public:
		CGraphicsInterface();
		virtual ~CGraphicsInterface();

		virtual bool Init(const CEngine& aEngine, Vector2ui aWindowSize, bool aEnableVSync, bool aStartInFullScreen) = 0;
		virtual void EndRenderFrame() = 0;
		virtual void BeginRenderFrame() = 0;


		static EGraphicsAPI GetGraphicsAPI() { return ourGFXAPI; };
		static void SetGraphicsAPI(const EGraphicsAPI aAPI) { ourGFXAPI = aAPI; }

	protected:
		static CGraphicsInterface* ourInterface;
		static EGraphicsAPI ourGFXAPI;
		bool myEnableVSync;
		Vector2ui myWindowSize;

	};
}
