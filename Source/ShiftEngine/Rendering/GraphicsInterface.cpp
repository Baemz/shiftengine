#include "stdafx.h"
#include "GraphicsInterface.h"

using namespace Shift3D;

EGraphicsAPI CGraphicsInterface::ourGFXAPI = EGraphicsAPI::None;
CGraphicsInterface* CGraphicsInterface::ourInterface = nullptr;

CGraphicsInterface::CGraphicsInterface()
	: myEnableVSync(false)
	, myWindowSize(0, 0)
{
}


CGraphicsInterface::~CGraphicsInterface()
{
	ourInterface = nullptr;
}
