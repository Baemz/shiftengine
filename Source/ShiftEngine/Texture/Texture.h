#pragma once

struct ID3D11ShaderResourceView;

struct ID3D11Device;
struct ID3D11DeviceContext;
namespace Shift3D
{
	class CTexture
	{
	public:
		CTexture();
		CTexture(const char* aPath);
		~CTexture();

		void Init(const char* aPath);
		void Bind(const UINT aStartSlot);
		void Unbind(const UINT aStartSlot);

		ID3D11ShaderResourceView* GetTexture() { return myTexture; }
	private:
		ID3D11ShaderResourceView* myTexture;
		ID3D11Device* myDevice;
		ID3D11DeviceContext* myDeviceContext;
		Vector2f mySize;
		Vector2<unsigned int> myImageSize;
		std::string myPath;

	};
}
