#pragma once

namespace Shift3D
{
	enum eTextureType
	{
		Diffuse = 0,
		Metalness = 1,
		Roughness = 2,
		NormalMap = 3,
		Ambient = 4,
		Emissive = 5,
		EnvironmentMap = 6,
		PreintegratedFG = 7
	};

	class CTexture;
	class CTextureInstance
	{
	public:
		CTextureInstance();
		CTextureInstance(const char* aTexturePath, eTextureType aTextureType);
		~CTextureInstance();

		void Init(const std::string& aTexturePath, eTextureType aTextureType);
		void Bind();
		void Unbind();

	private:
		CTexture* myTextureResource;
		UINT myTextureType;
	};
}
