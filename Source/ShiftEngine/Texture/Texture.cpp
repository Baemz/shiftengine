#include "stdafx.h"
#include "Texture.h"
#include <d3d11.h>
#include <Platform/DirectX/11/Direct3D11.h>
#include <Platform\DirectX\DirectXTK\Inc\DDSTextureLoader.h>

using namespace Shift3D;

CTexture::CTexture()
{
	myDevice = CDirect3D11::GetDevice();
	myDeviceContext = CDirect3D11::GetDeviceContext();
}

Shift3D::CTexture::CTexture(const char* aPath)
	: CTexture()
{
	Init(aPath);
}


CTexture::~CTexture()
{
	SAFE_RELEASE(myTexture);
}

void Shift3D::CTexture::Init(const char* aPath)
{
	myPath = aPath;
	std::wstring ws(myPath.begin(), myPath.end());
	DirectX::CreateDDSTextureFromFile(myDevice, ws.c_str(), nullptr, &myTexture);
}

void Shift3D::CTexture::Bind(const UINT aStartSlot)
{
	myDeviceContext->PSSetShaderResources(aStartSlot, 1, &myTexture);
}

void Shift3D::CTexture::Unbind(const UINT aStartSlot)
{
	ID3D11ShaderResourceView* nullResource = { nullptr };
	myDeviceContext->PSSetShaderResources(aStartSlot, 1, &nullResource);
}
