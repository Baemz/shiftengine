#pragma once

namespace Shift3D
{
	class CTexture;
	class CTextureManager
	{
	public:
		CTextureManager();
		~CTextureManager();

		CTexture* GetTexture(const std::string&  aTexturePath);

	private:
		void LoadTexture(const std::string& aPath);
		unsigned int myLoadedTextureCount;
		std::map<std::string, CTexture*> myLoadedTextures;
	};
}