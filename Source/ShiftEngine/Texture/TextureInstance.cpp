#include "stdafx.h"
#include "TextureInstance.h"
#include "TextureManager.h"
#include "Texture.h"

using namespace Shift3D;

CTextureInstance::CTextureInstance()
	: myTextureResource(nullptr)
	, myTextureType(0)
{
}

CTextureInstance::CTextureInstance(const char* aTexturePath, eTextureType aTextureType)
	: CTextureInstance()
{
	Init(aTexturePath, aTextureType);
}

CTextureInstance::~CTextureInstance()
{
}

void CTextureInstance::Init(const std::string& aTexturePath, eTextureType aTextureType)
{
	myTextureType = static_cast<UINT>(aTextureType);
	std::string path = "Data\\Textures\\" + Utils::GetFilename(aTexturePath);
	myTextureResource = CRootManager::GetTextureManager()->GetTexture(path.c_str());
	if (myTextureResource == nullptr)
	{
		ERROR_PRINT("%s", "WARNING! Texture-resource is nullptr, textures may be visually corrupt.");
	}
}

void CTextureInstance::Bind()
{
	if (myTextureResource)
	{
		myTextureResource->Bind(myTextureType);
	}
}

void Shift3D::CTextureInstance::Unbind()
{
	myTextureResource->Unbind(myTextureType);
}
