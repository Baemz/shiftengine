#include "stdafx.h"
#include "TextureManager.h"
#include "Texture.h"


using namespace Shift3D;

CTextureManager::CTextureManager()
	: myLoadedTextureCount(0)
{
}


CTextureManager::~CTextureManager()
{
}

CTexture* Shift3D::CTextureManager::GetTexture(const std::string& aTexturePath)
{
	if (Utils::FileExists(aTexturePath.c_str()))
	{
		if (myLoadedTextures.find(aTexturePath) == myLoadedTextures.end())
		{
			LoadTexture(aTexturePath);
		}
		return myLoadedTextures[aTexturePath];
	}
	else
	{
		ERROR_PRINT("%s", "ERROR! Texture-file not found. Did you spell it correctly?");
		return nullptr;
	}

}

void Shift3D::CTextureManager::LoadTexture(const std::string& aPath)
{
	myLoadedTextures.insert(std::pair<std::string, CTexture*>(aPath, new CTexture(aPath.c_str())));
	++myLoadedTextureCount;
}
