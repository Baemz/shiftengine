#include "stdafx.h"
#include "Engine.h"

//Utilities
#include "Utilities/RootManager.h"
#include "Input/InputManager.h"
#include "Window/WindowsWindow.h"

// Physics
#include "Physics/PhysXEngine.h"

// Rendering
#include "Platform/DirectX/12/Direct3D12.h"
#include "Platform/DirectX/11/Direct3D11.h"
#include "Platform/Vulkan/VulkanAPI.h"

//Errors
#include "Utilities/ErrorHandler.h"
#include <cassert>

//Temp Includes
#include <thread>
#include <iostream>

using namespace Shift3D;
CEngine* CEngine::ourEngine = nullptr;


void CEngine::Create(const EngineCreateParameters& aCreateParameters)
{
	assert(ourEngine == nullptr && "Instance already created!");
	if (ourEngine != nullptr)
	{
		ERROR_PRINT("%s", "ERROR, ENGINE ALREADY CREATED.");
		return;
	}
	else
	{
		ourEngine = new CEngine(aCreateParameters);
		CRootManager::Create();
	}

}

ID3D11DeviceContext* Shift3D::CEngine::GetDeviceContext()
{
	CDirect3D11* d3d = dynamic_cast<CDirect3D11*>(myGraphicsInterface);
	if (d3d != nullptr)
	{
		return d3d->GetDeviceContext();
	}
	return nullptr;
}
Physics::CPhysXEngine * Shift3D::CEngine::GetPhysXEngine() const
{
	return myPhysXEngine;
}
HWND Shift3D::CEngine::GetHWND() const
{
	return myWindow->GetWindowHandle();
}

CEngine::CEngine(const EngineCreateParameters& aCreateParameters)
	: myWindow(nullptr)
	, myGraphicsInterface(nullptr)
	, myRunEngine(true)
	, myCreateParameters(aCreateParameters)
	, myFPSShowTimer(0.f)
{
	myWindowSize.x = myCreateParameters.myWindowConfig.myWindowWidth;
	myWindowSize.y = myCreateParameters.myWindowConfig.myWindowHeight;

	myRenderSize.x = myCreateParameters.myWindowConfig.myRenderWidth;
	myRenderSize.y = myCreateParameters.myWindowConfig.myRenderHeight;

	myTargetSize = myRenderSize;
	if (myCreateParameters.myWindowConfig.myTargetWidth > 0 && myCreateParameters.myWindowConfig.myTargetHeight > 0)
	{
		myTargetSize.x = myCreateParameters.myWindowConfig.myTargetWidth;
		myTargetSize.y = myCreateParameters.myWindowConfig.myTargetHeight;
	}

	myInitFunctionToCall = myCreateParameters.myInitFunctionToCall;
	myUpdateFunctionToCall = myCreateParameters.myUpdateFunctionToCall;
	myDebugRenderFunction = myCreateParameters.myDebugRenderFunction;
	myHwnd = myCreateParameters.myHwnd;
	myHInstance = myCreateParameters.myHInstance;
	myCreateParameters.myHInstance = myCreateParameters.myHInstance;



	myErrorHandler = new CErrorHandler();
	myErrorHandler->AddLogListener(aCreateParameters.myLogFunction, aCreateParameters.myErrorFunction);
}

void CEngine::Destroy()
{
	if (ourEngine != nullptr)
	{
		SAFE_DELETE(ourEngine);
		CRootManager::Destroy();
		return;
	}
	else
	{
		ERROR_PRINT("%s", "ERROR, NO ENGINE WAS DESTROYED.");
		return;
	}
}
CEngine::~CEngine()
{
	SAFE_DELETE(myWindow);
	SAFE_DELETE(myGraphicsInterface);
}

CEngine* CEngine::GetInstance()
{
	return ourEngine;
}

bool CEngine::Start()
{
	std::string startText("Starting Shift-Engine in ");
#ifdef _DEBUG
	startText = startText + "DEBUG-mode.";
#else
	startText = startText + "RELEASE-mode.";
#endif

	INFO_PRINT("%s", startText.c_str());
	myWindow = new WindowsWindow();
	if (!myWindow->Init(myWindowSize, myHwnd, &myCreateParameters, myHInstance, myCreateParameters.myWinProcCallback))
	{
		ERROR_PRINT("%s", "Could not create window, exiting...");
		return false;
	}

	switch (myCreateParameters.myGraphicsAPI)
	{
		case EGraphicsAPI::eDirectX11:
		{
			myGraphicsInterface = new CDirect3D11();
			if (!myGraphicsInterface->Init(*this, myWindowSize, myCreateParameters.myWindowConfig.myEnableVSync, myCreateParameters.myWindowConfig.myStartInFullScreen))
			{
				ERROR_PRINT("%s", "Could not initiate DirectX 11, exiting...");
				myWindow->Close();
				return false;
			}
			break;
		}
		case EGraphicsAPI::eDirectX12:
		{
			myGraphicsInterface = new CDirect3D12();
			if (!myGraphicsInterface->Init(*this, myWindowSize, myCreateParameters.myWindowConfig.myEnableVSync, myCreateParameters.myWindowConfig.myStartInFullScreen))
			{
				ERROR_PRINT("%s", "Could not initiate DirectX 12, exiting...");
				myWindow->Close();
				return false;
			}
			break;
		}
		case EGraphicsAPI::eOpenGL:
		{
			break;
		}
		case EGraphicsAPI::eVulkan:
		{
			myGraphicsInterface = new CVulkanAPI();
			if (!myGraphicsInterface->Init(*this, myWindowSize, myCreateParameters.myWindowConfig.myEnableVSync, myCreateParameters.myWindowConfig.myStartInFullScreen))
			{
				ERROR_PRINT("%s", "Could not initiate Vulkan, exiting...");
				myWindow->Close();
				return false;
			}
			break;
		}
	}


	myPhysXEngine = new Physics::CPhysXEngine();
	if (myPhysXEngine->Init() ==  false)
	{
		ERROR_PRINT("%s", "Could not initiate PhysX, exiting...");
		return false;
	}

	if (myInitFunctionToCall != nullptr)
	{
		myInitFunctionToCall();
	}

	if (myUpdateFunctionToCall != nullptr)
	{
		StartStep();
	}

	return true;
}

void CEngine::Shutdown()
{
	myRunEngine = false;
}

void CEngine::StartStep()
{
	INFO_PRINT("%s", "Shift-Engine started. Executing game...");
	ExecuteStep();
}

void CEngine::ExecuteStep()
{
	MSG msg = { 0 };
	while (myRunEngine)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);

			if (msg.message == WM_QUIT)
			{
				INFO_PRINT("%s", "Shutting down engine.");
				break;
			}
			CRootManager::GetInputManager()->HandleInput(msg.hwnd, msg.message, msg.wParam, msg.lParam);
		}
		else
		{
			myTimer.Update();
			if (myFPSShowTimer >= 1.f)
			{
				std::cout << "FPS: " << static_cast<int>(myTimer.GetFPS()) << std::endl;
				myFPSShowTimer = 0.f;
			}
			else
			{
				myFPSShowTimer += myTimer.GetDeltaTime();
			}
			myPhysXEngine->Simulate(myTimer.GetDeltaTime());
			myGraphicsInterface->BeginRenderFrame();
			if (myUpdateFunctionToCall != nullptr)
			{
				myUpdateFunctionToCall(myTimer.GetDeltaTime());
			}

			if (myDebugRenderFunction != nullptr)
			{
				#ifdef _DEBUG
					myDebugRenderFunction();
				#endif
			}
			myGraphicsInterface->EndRenderFrame();
			myPhysXEngine->FetchResults();
			CRootManager::GetInputManager()->Update();
		}
		std::this_thread::yield();
	}
}