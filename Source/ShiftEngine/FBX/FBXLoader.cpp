#include "stdafx.h"
#include "FBXLoader.h"
#include <cassert>

using namespace Shift3D;

CFBXLoader::CFBXLoader()
{
	myFbxManager = FbxManager::Create(); 
	FbxIOSettings* ioSettings = FbxIOSettings::Create(myFbxManager, IOSROOT);
	myFbxManager->SetIOSettings(ioSettings);
}


CFBXLoader::~CFBXLoader()
{
	myFbxManager->Destroy();
}

bool Shift3D::CFBXLoader::LoadFBXs(const char* aFilePath, std::vector<SVertex>& aVertexVectorToFill, std::vector<UINT>& aIndexVectorToFill)
{
	FbxImporter* importer = FbxImporter::Create(myFbxManager, "");
	FbxScene* fbxScene = FbxScene::Create(myFbxManager, "");

	bool result = importer->Initialize(aFilePath, -1, myFbxManager->GetIOSettings());
	if (!result)
	{
		return false;
	}

	result = importer->Import(fbxScene);
	if (!result)
	{
		return false;
	}
	importer->Destroy();
	importer = nullptr;


	FbxNode* fbxRootNode = fbxScene->GetRootNode();

	if (fbxRootNode)
	{
		for (int i = 0; i < fbxRootNode->GetChildCount(); i++)
		{
			FbxNode* fbxChildNode = fbxRootNode->GetChild(i);

			if (fbxChildNode->GetNodeAttribute() == NULL)
			{
				continue;
			}

			FbxNodeAttribute::EType attributeType = fbxChildNode->GetNodeAttribute()->GetAttributeType();

			if (attributeType != FbxNodeAttribute::eMesh)
			{
				continue;
			}

			FbxMesh* mesh = fbxChildNode->GetMesh();

			if (mesh != nullptr)
			{
				unsigned long polyCount = mesh->GetPolygonCount();
				unsigned long vertexCount = 0;
				unsigned long vertexIndex = 0;

				for (unsigned long uPoly = 0; uPoly < polyCount; ++uPoly)
				{
					vertexCount = mesh->GetPolygonSize(uPoly);
					assert((vertexCount % 3) == 0 && "Mesh not triangulated");

					for (unsigned long uVertex = 0; uVertex < vertexCount; ++uVertex)
					{
						aIndexVectorToFill.push_back(static_cast<UINT>(vertexIndex++));

						SVertex vertex;

						// POSITION
						FbxVector4 fbxVertex = mesh->GetControlPointAt(mesh->GetPolygonVertex(uPoly, uVertex));
						vertex.x = (float)fbxVertex.mData[0];
						vertex.y = (float)fbxVertex.mData[1];
						vertex.z = (float)fbxVertex.mData[2];
						vertex.w = 1.f;

						// NORMAL
						FbxVector4 fbxNormal;
						bool hasNormal = mesh->GetPolygonVertexNormal(uPoly, uVertex, fbxNormal);
						if (hasNormal)
						{
							vertex.normalX = (float)fbxNormal.mData[0];
							vertex.normalY = (float)fbxNormal.mData[1];
							vertex.normalZ = (float)fbxNormal.mData[2];
							vertex.normalW = 0.f;
						}

						// TEXTURE (UV)
						FbxLayerElementArrayTemplate<FbxVector2>* textureUVs = 0;
						mesh->GetTextureUV(&textureUVs, FbxLayerElement::eTextureDiffuse);
						if (textureUVs)
						{
							FbxVector2 uv = textureUVs->GetAt(mesh->GetTextureUVIndex(uPoly, uVertex));
							vertex.textU = static_cast<float>(uv[0]);
							vertex.textV = 1.f - static_cast<float>(uv[1]);
						}
						
						aVertexVectorToFill.push_back(vertex);
					}
				}
			}
		}

	}
	fbxScene->Destroy(true);
	fbxScene = nullptr;
	return true;
}

std::vector<SMeshData> Shift3D::CFBXLoader::LoadFBX(const char* aFilePath)
{
	std::vector<SMeshData> modelData;
	FbxImporter* importer = FbxImporter::Create(myFbxManager, "");
	FbxScene* fbxScene = FbxScene::Create(myFbxManager, "");

	bool result = importer->Initialize(aFilePath, -1, myFbxManager->GetIOSettings());
	if (!result)
	{
		return modelData;
	}

	result = importer->Import(fbxScene);
	if (!result)
	{
		return modelData;
	}
	importer->Destroy();
	importer = nullptr;

	FbxNode* fbxRootNode = fbxScene->GetRootNode();
	
	FbxAxisSystem::DirectX.ConvertScene(fbxScene);

	if (fbxRootNode)
	{
		for (int i = 0; i < fbxRootNode->GetChildCount(); i++)
		{
			FbxNode* fbxChildNode = fbxRootNode->GetChild(i);
			if (fbxChildNode->GetNodeAttribute() == NULL)
			{
				continue;
			}
			if (fbxChildNode->GetNodeAttribute()->GetAttributeType() != FbxNodeAttribute::eMesh)
			{
				continue;
			}

			FbxMesh* mesh = fbxChildNode->GetMesh();
			if (mesh != nullptr)
			{
				SMeshData meshData;
				//LoadTextures(fbxChildNode, meshData);

				unsigned long polyCount = mesh->GetPolygonCount();
				unsigned long vertexCount = 0;
				unsigned long vertexIndex = 0;

				for (unsigned long uPoly = 0; uPoly < polyCount; ++uPoly)
				{
					vertexCount = mesh->GetPolygonSize(uPoly);
					assert((vertexCount % 3) == 0 && "Mesh not triangulated");

					for (unsigned long uVertex = 0; uVertex < vertexCount; ++uVertex)
					{
						meshData.myIndices.push_back(static_cast<UINT>(vertexIndex++));

						SVertex vertex;

						// POSITION
						FbxVector4 fbxVertex = mesh->GetControlPointAt(mesh->GetPolygonVertex(uPoly, uVertex));
						vertex.x = (float)fbxVertex.mData[0];
						vertex.y = (float)fbxVertex.mData[1];
						vertex.z = (float)fbxVertex.mData[2];
						vertex.w = 1.f;

						// NORMAL
						FbxVector4 fbxNormal;
						bool hasNormal = mesh->GetPolygonVertexNormal(uPoly, uVertex, fbxNormal);
						if (hasNormal)
						{
							vertex.normalX = (float)fbxNormal.mData[0];
							vertex.normalY = (float)fbxNormal.mData[1];
							vertex.normalZ = (float)fbxNormal.mData[2];
							vertex.normalW = 0.f;
						}

						// BINORMAL
						bool hasBinormal = (mesh->GetElementBinormalCount() > 0);
						if (hasBinormal)
						{
							auto geoBinormal = mesh->GetElementBinormal(0);
							if (geoBinormal != nullptr)
							{
								FbxVector4 binormal = geoBinormal->mDirectArray->GetAt(uVertex);
								vertex.binormalX = (float)binormal.mData[0];
								vertex.binormalY = (float)binormal.mData[1];
								vertex.binormalZ = (float)binormal.mData[2];
							}
						}

						// TANGENT
						bool hasTangent = (mesh->GetElementTangentCount() > 0);
						if (hasTangent)
						{
							auto geoTangent = mesh->GetElementTangent(0);
							if (geoTangent != nullptr)
							{
								FbxVector4 tangent = geoTangent->mDirectArray->GetAt(uVertex);
								vertex.tangentX = (float)tangent.mData[0];
								vertex.tangentY = (float)tangent.mData[1];
								vertex.tangentZ = (float)tangent.mData[2];
							}
						}

						// TEXTURE (UV)
						FbxLayerElementArrayTemplate<FbxVector2>* textureUVs = 0;
						mesh->GetTextureUV(&textureUVs, FbxLayerElement::eTextureDiffuse);
						if (textureUVs)
						{
							FbxVector2 uv = textureUVs->GetAt(mesh->GetTextureUVIndex(uPoly, uVertex));
							vertex.textU = static_cast<float>(uv[0]);
							vertex.textV = 1.f - static_cast<float>(uv[1]);
						}

						meshData.myVertices.push_back(vertex);
					}
				}
				modelData.push_back(meshData);
			}
		}

	}
	fbxScene->Destroy(true);
	fbxScene = nullptr;
	return modelData;
}

void Shift3D::CFBXLoader::LoadTextures(FbxNode* aFbxChildNode, SMeshData& aMeshData)
{
	int materialCount = aFbxChildNode->GetSrcObjectCount<FbxSurfaceMaterial>();
	for (int index = 0; index < materialCount; index++)
	{
		FbxSurfaceMaterial* material = (FbxSurfaceMaterial*)aFbxChildNode->GetSrcObject<FbxSurfaceMaterial>(index);
		if (material != NULL)
		{
			FbxProperty prop = material->FindProperty(FbxSurfaceMaterial::sDiffuse);

			int layeredTextureCount = prop.GetSrcObjectCount<FbxLayeredTexture>();
			if (layeredTextureCount > 0)
			{
				for (int j = 0; j < layeredTextureCount; j++)
				{
					FbxLayeredTexture* layeredTexture = FbxCast<FbxLayeredTexture>(prop.GetSrcObject<FbxLayeredTexture>(j));
					int lcount = layeredTexture->GetSrcObjectCount<FbxTexture>();

					for (int k = 0; k < lcount; k++)
					{
						FbxTexture* texture = FbxCast<FbxTexture>(layeredTexture->GetSrcObject<FbxTexture>(k));
						FbxFileTexture* fileTexture = FbxCast<FbxFileTexture>(texture);
						aMeshData.myTexture = std::string(fileTexture->GetRelativeFileName());
					}
				}
			}
			else
			{
				int textureCount = prop.GetSrcObjectCount<FbxTexture>();
				for (int j = 0; j < textureCount; j++)
				{
					FbxTexture* texture = FbxCast<FbxTexture>(prop.GetSrcObject<FbxTexture>(j));
					FbxFileTexture* fileTexture = FbxCast<FbxFileTexture>(texture);
					aMeshData.myTexture = std::string(fileTexture->GetRelativeFileName());
				}
			}
		}
	}
}
