#pragma once
#include "include/fbxsdk.h"
#include <vector>

namespace Shift3D
{
	struct SMeshData
	{
		std::vector<UINT> myIndices;
		std::vector<SVertex> myVertices;
		std::string myTexture;
		std::vector<std::string> myTextures;
	};

	class CFBXLoader
	{
	public:
		CFBXLoader();
		~CFBXLoader();

		bool LoadFBXs(const char* aFilePath, std::vector<SVertex>& aVertexVectorToFill, std::vector<UINT>& aIndexVectorToFill);
		std::vector<SMeshData> LoadFBX(const char* aFilePath);
	private:
		FbxManager* myFbxManager;

		void LoadTextures(FbxNode* aFbxChildNode, SMeshData& aMeshData);
	};
}
