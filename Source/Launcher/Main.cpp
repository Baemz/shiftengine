#include "stdafx.h"
#include "GameManager.h"


// Comment out below define to disable command line
#define USE_CONSOLE_COMMAND

#pragma region WindowsInit
#ifdef USE_CONSOLE_COMMAND
#pragma comment(linker, "/SUBSYSTEM:console")
#else
#pragma comment(linker, "/SUBSYSTEM:windows")
#endif

#ifdef USE_CONSOLE_COMMAND
int main()
{
	CGameManager game;
	StartData startData;
	startData.appName = L"Game";
	startData.resolutionWidth = 1920;
	startData.resolutionHeight = 1080;
	startData.startInFullscreen = false;
	game.Init(startData);

	return 0;
}
#else
int WINAPI WinMain(HINSTANCE /*hInstance*/, HINSTANCE /*hPrevInstance*/, char*, int /*nShowCmd*/)
{
	CGameManager game;
	StartData startData;
	startData.appName = L"Game";
	startData.resolutionWidth = 1920;
	startData.resolutionHeight = 1080;
	startData.startInFullscreen = false;
	game.Init(startData);

	return 0;
}
#endif
#pragma endregion MainFunction
