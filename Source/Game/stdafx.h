// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"
#include <stdio.h>
#include <tchar.h>

#include "Utilities/MainManager.h"
#include "ShiftEngine/Utilities/ErrorHandler.h"
#include "ShiftEngine/Math/Vector.h"


#define SAFE_DELETE(aPtr) delete aPtr; aPtr = nullptr;
#define SAFE_RELEASE(aPtr) { if ( (aPtr) ) { (aPtr)->Release(); (aPtr) = 0; } }
#define MAX(aNum, aSecondNum) (aNum > aSecondNum) ? aNum : aSecondNum
#define MIN(aNum, aSecondNum) (aNum < aSecondNum) ? aNum : aSecondNum
#define PIf       3.14159265358979323846
#define DEGREES_TO_RADIANS static_cast<float>(PIf / 180.0f)


// TODO: reference additional headers your program requires here
