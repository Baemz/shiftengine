#include "stdafx.h"
#include "Game.h"
#include "Input/InputManager.h"
#include <iostream>

//------------------------------------------//
#include <ShiftEngine/Camera/Camera.h>
#include <ShiftEngine/FBX/FBXLoader.h>
#include <Model/ModelInstance.h>
#include <Model/Material/Material.h>
#include <Lights/DirectionalLight.h>
//------------------------------------------//


CGame::CGame()
{
}


CGame::~CGame()
{
}

void CGame::Init()
{
	myModel = new Shift3D::CModelInstance();
	myModel->Init("Data/Models/Slinger_00.fbx");
	myModel->SetPosition({ 0.0f, 0.0f, 5.0f });

	Shift3D::CMaterial* mat = new Shift3D::CMaterial();
	mat->SetDiffuse("Slinger_Base_Color.dds");
	mat->SetRoughness("Slinger_Roughness.dds");
	mat->SetMetalness("Slinger_Metallic.dds");
	mat->SetNormalMap("Slinger_Normal.dds");
	mat->SetEnvironmentMap("CubeMap13.dds");

	myModel->SetMaterial(mat);

	/*myModel2 = new Shift3D::CModelInstance();
	myModel2->Init("Data/Models/barrels.fbx", "Data/Textures/barrels.dds");
	myModel2->SetPosition({ 3.0f, 0.0f, 3.0f });*/

	/*myModel3 = new Shift3D::CModelInstance();
	myModel3->Init("Data/Models/crate.fbx", "Data/Textures/crate.dds");
	myModel3->SetPosition({ -3.0f, 0.0f, -3.0f });

	myModel4 = new Shift3D::CModelInstance();
	myModel4->Init("Data/Models/plane.fbx", "Data/Textures/plane.dds");*/


	myDirectionalLight = new Shift3D::CDirectionalLight();
	myDirectionalLight->SetColor({ 1.f, 1.f, 1.f, 1.f });
	myDirectionalLight->SetDirection({ 0.5f, -0.5f, 1.0f });
}

void CGame::Update(const float aDeltaTime)
{
	aDeltaTime;
	if (MainManager::GetInputManager()->IsKeyDown(Shift3D::Keys::D))
	{
		MainManager::GetCamera()->MoveRight(aDeltaTime * 10);
	}
	else if (MainManager::GetInputManager()->IsKeyDown(Shift3D::Keys::A))
	{
		MainManager::GetCamera()->MoveLeft(aDeltaTime * 10);
	}
	
	if (MainManager::GetInputManager()->IsKeyDown(Shift3D::Keys::W))																		 
	{
		MainManager::GetCamera()->MoveForward(aDeltaTime * 10);
	}
	else if (MainManager::GetInputManager()->IsKeyDown(Shift3D::Keys::S))
	{
		MainManager::GetCamera()->MoveBack(aDeltaTime * 10);
	}

	if (MainManager::GetInputManager()->IsKeyDown(Shift3D::Keys::Control))
	{
		MainManager::GetCamera()->MoveDown(aDeltaTime * 10);
	}
	else if (MainManager::GetInputManager()->IsKeyDown(Shift3D::Keys::Space))
	{
		MainManager::GetCamera()->MoveUp(aDeltaTime * 10);
	}

	if (MainManager::GetInputManager()->IsKeyDown(Shift3D::Keys::Q))
	{
		MainManager::GetCamera()->RotateCamera(Shift3D::Vector3f(0.f, -aDeltaTime, 0.f));
	}
	else if (MainManager::GetInputManager()->IsKeyDown(Shift3D::Keys::E))
	{
		MainManager::GetCamera()->RotateCamera(Shift3D::Vector3f(0.f, aDeltaTime, 0.f));
	}

	if (MainManager::GetInputManager()->IsKeyDown(Shift3D::Keys::Up))
	{
		myDirectionalLight->RotateAroundX(-aDeltaTime);

	}
	else if (MainManager::GetInputManager()->IsKeyDown(Shift3D::Keys::Down))
	{
		myDirectionalLight->RotateAroundX(aDeltaTime);
	}
	else if (MainManager::GetInputManager()->IsKeyDown(Shift3D::Keys::Left))
	{

		myDirectionalLight->RotateAroundY(-aDeltaTime);
	}
	else if (MainManager::GetInputManager()->IsKeyDown(Shift3D::Keys::Right))
	{

		myDirectionalLight->RotateAroundY(aDeltaTime);
	}

	if (MainManager::GetInputManager()->WasKeyJustPressed(Shift3D::Keys::N))
	{
		myDirectionalLight->SetColor({ 0.976f, 0.949f, 0.862f, 1.0f });
	}
	else if (MainManager::GetInputManager()->WasKeyJustPressed(Shift3D::Keys::M))
	{
		myDirectionalLight->SetColor({ 1.0f, 0.0f, 1.0f, 1.0f });
	}

	/*if (MainManager::GetInputManager()->GetMouseXMovementSinceLastFrame() > 0)
	{
		MainManager::GetCamera()->RotateCamera(Shift3D::Vector3f(0.f, aDeltaTime * 10, 0.f));
	}
	else if (MainManager::GetInputManager()->GetMouseXMovementSinceLastFrame() < 0)
	{
		MainManager::GetCamera()->RotateCamera(Shift3D::Vector3f(0.f, -(aDeltaTime * 10), 0.f));
	}

	if (MainManager::GetInputManager()->GetMouseYMovementSinceLastFrame() > 0)
	{
		MainManager::GetCamera()->RotateCamera(Shift3D::Vector3f(aDeltaTime * 10, 0.f, 0.f));
	}
	else if (MainManager::GetInputManager()->GetMouseYMovementSinceLastFrame() < 0)
	{
		MainManager::GetCamera()->RotateCamera(Shift3D::Vector3f(-(aDeltaTime * 10), 0.f, 0.f));
	}*/
}

void CGame::Render()
{
	myModel->Render(MainManager::GetCamera()->GetViewInverse(), MainManager::GetCamera()->GetProjection(), MainManager::GetCamera()->GetPosition());
	//myModel2->Render(MainManager::GetCamera()->GetViewInverse(), MainManager::GetCamera()->GetProjection(), MainManager::GetCamera()->GetPosition());
	/*myModel3->Render(MainManager::GetCamera()->GetViewInverse(), MainManager::GetCamera()->GetProjection(), MainManager::GetCamera()->GetPosition());
	myModel4->Render(MainManager::GetCamera()->GetViewInverse(), MainManager::GetCamera()->GetProjection(), MainManager::GetCamera()->GetPosition());*/

	myModel->SetLightData(myDirectionalLight->GetDirection(), myDirectionalLight->GetColor());
	//myModel2->SetLightData(myDirectionalLight->GetDirection(), myDirectionalLight->GetColor(), myDirectionalLight->GetAmbientColor());
	/*myModel3->SetLightData(myDirectionalLight->GetDirection(), myDirectionalLight->GetColor(), myDirectionalLight->GetAmbientColor());
	myModel4->SetLightData(myDirectionalLight->GetDirection(), myDirectionalLight->GetColor(), myDirectionalLight->GetAmbientColor());*/
}
