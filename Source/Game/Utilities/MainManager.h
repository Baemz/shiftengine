#pragma once

namespace Shift3D
{
	class InputManager;
	class CCamera;
}

class MainManager
{
public:
	static void Create();
	static void Destroy();

	static Shift3D::InputManager* GetInputManager();
	static Shift3D::CCamera* GetCamera();

private:
	MainManager();
	~MainManager();

	static MainManager* ourInstance;
	static MainManager* GetInstance();

	Shift3D::InputManager* myInputManager;
	Shift3D::CCamera* myCamera;
};

