#include "stdafx.h"
#include "MainManager.h"
#include "ShiftEngine/Engine.h"
#include "ShiftEngine/Input/InputManager.h"
#include "ShiftEngine/Utilities/RootManager.h"
#include "ShiftEngine/Camera/Camera.h"
#include <cassert>

MainManager* MainManager::ourInstance = nullptr;

void MainManager::Create()
{
	if (ourInstance == nullptr)
	{
		ourInstance = new MainManager();
	}
	else
	{
		assert(false && "Instance already created.");
	}
}

void MainManager::Destroy()
{
	SAFE_DELETE(ourInstance);
}

Shift3D::InputManager* MainManager::GetInputManager()
{
	return GetInstance()->myInputManager;
}

Shift3D::CCamera * MainManager::GetCamera()
{
	return GetInstance()->myCamera;
}

MainManager::MainManager()
{
	myInputManager = Shift3D::CRootManager::GetInputManager();
	myCamera = new Shift3D::CCamera(0.001f, 3000.f, 80 * DEGREES_TO_RADIANS, 1366.f, 768.f, Shift3D::Vector3f(0.0f, 1.0f, -10.f));
}


MainManager::~MainManager()
{
	myInputManager = nullptr;
	SAFE_DELETE(myCamera);
}

MainManager* MainManager::GetInstance()
{
	return ourInstance;
}
