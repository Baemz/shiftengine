#pragma once
#include <ShiftEngine/Engine.h>
#include "Game.h"

struct StartData
{
	unsigned short resolutionWidth;
	unsigned short resolutionHeight;
	bool startInFullscreen;
	std::wstring appName;
};

class CGameManager
{
public:
	CGameManager();
	~CGameManager();

	bool Init(const StartData& aStartData, HWND aHWND = nullptr);

private:
	LRESULT WinProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	void InitCallback();
	void UpdateCallback(const float aDeltaTime);
	// Will only be called in "Debug" build-mode.
	// Place your debug-draws here.
	void DebugRenderCallback();
	void LogCallback(std::string aText);
	CGame myGame;

};

