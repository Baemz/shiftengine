#include "stdafx.h"
#include "GameManager.h"

using namespace std::placeholders;

CGameManager::CGameManager()
{
}
CGameManager::~CGameManager()
{
}

bool CGameManager::Init(const StartData& aStartData, HWND aHWND)
{
	unsigned short windowWidth = aStartData.resolutionWidth;
	unsigned short windowHeight = aStartData.resolutionHeight;


	Shift3D::EngineCreateParameters createParameters;

	createParameters.myInitFunctionToCall = std::bind(&CGameManager::InitCallback, this);
	createParameters.myUpdateFunctionToCall = std::bind(&CGameManager::UpdateCallback, this, _1);
	createParameters.myDebugRenderFunction = std::bind(&CGameManager::DebugRenderCallback, this);
	createParameters.myWinProcCallback = std::bind(&CGameManager::WinProc, this, _1, _2, _3, _4);
	createParameters.myLogFunction = std::bind(&CGameManager::LogCallback, this, _1);
	createParameters.myWindowConfig.myWindowHeight = windowHeight;
	createParameters.myWindowConfig.myWindowWidth = windowWidth;
	createParameters.myWindowConfig.myRenderHeight = windowHeight;
	createParameters.myWindowConfig.myRenderWidth = windowWidth;
	createParameters.myWindowConfig.myTargetWidth = windowWidth;
	createParameters.myWindowConfig.myTargetHeight = windowHeight;
	createParameters.myWindowConfig.myWindowSetting = Shift3D::EWindowSetting::EWindowSetting_Overlapped;
	createParameters.myGraphicsAPI = Shift3D::EGraphicsAPI::eDirectX11;
	createParameters.myWindowConfig.myStartInFullScreen = aStartData.startInFullscreen;

#ifdef _DEBUG
	createParameters.myApplicationName = aStartData.appName + L" *DEBUG-MODE*";
#endif
#ifdef NDEBUG
	createParameters.myApplicationName = aStartData.appName;
#endif

	if (aHWND != nullptr)
	{
		createParameters.myHwnd = new HWND(aHWND);
	}

	Shift3D::CEngine::GetInstance()->Create(createParameters);
	MainManager::Create();

	if (!Shift3D::CEngine::GetInstance()->Start())
	{
		ERROR_PRINT("%s", "FATAL ERROR! SHIFT-ENGINE COULD NOT START.");
		return false;
	}

	return true;
}

LRESULT CGameManager::WinProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	lParam;
	wParam;
	hWnd;
	switch (message)
	{
		case WM_DESTROY:
		{
			PostQuitMessage(0);
			return 0;
		}
	}

	return 0;
}

void CGameManager::InitCallback()
{
	myGame.Init();
}

void CGameManager::UpdateCallback(const float aDeltaTime)
{
	myGame.Update(aDeltaTime);
	myGame.Render();
}

void CGameManager::DebugRenderCallback()
{
}

void CGameManager::LogCallback(std::string aText)
{
}
