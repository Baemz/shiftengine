#pragma once

namespace Shift3D
{
	class CModelInstance;
	class CDirectionalLight;
}

class CGame
{
public:
	CGame();
	~CGame();

	void Init();
	void Update(const float aDeltaTime);
	void Render();

private:
	Shift3D::CModelInstance* myModel;
	Shift3D::CModelInstance* myModel2;
	Shift3D::CModelInstance* myModel3;
	Shift3D::CModelInstance* myModel4;
	Shift3D::CDirectionalLight* myDirectionalLight;
};

